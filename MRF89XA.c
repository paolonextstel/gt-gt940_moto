/*********************************************************************
 *
 *                  MRF89XA Radio Driver Functions
 *
 *********************************************************************
 * FileName:        MRF89XA.c
 * Dependencies:    None
 * Processor:       PIC16FL1829
 ********************************************************************/

#include "XC.h"
#include "MRF89XA.h"
#include "GenericTypeDefs.h"
#include "GT940BB.h"
#include "mcc_generated_files/spi1.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/clock.h"


#define    FCY    _XTAL_FREQ    // Instruction cycle frequency, Hz - required for __delayXXX() to work
#include <libpic30.h>        // __delayXXX() functions macros defined here

//First time configuration settings for MRF89XA
const char InitConfigRegs[] = {
    /* 0 */ CHIPMODE_STBYMODE | FREQBAND_950 | VCO_TRIM_11, // modifica per set 868 Mhz a 50 Kbits
    /* 1 */ MODSEL_FSK | DATAMODE_PACKET | IFGAIN_0,
    /* 2 */ FREGDEV_200, // modifica per set 868 Mhz a 100 Kbits
    /* 3 */ BITRATE_100, // modifica per set 100 Kbits
    /* 4 */ OOKFLOORTHRESH_VALUE,
    /* 5 */ FIFOSIZE_64 | FIFO_THRSHOLD_1,
    /* 6 */ 0,
    /* 7 */ 0,
    /* 8 */ 0,
    /* 9 */ 0,
    /* 10 */ 0,
    /* 11 */ 0,
    /* 12 */ DEF_PARAMP | PA_RAMP_23,
    /* 13 */ IRQ0_RX_STDBY_SYNCADRS | IRQ1_RX_STDBY_CRCOK | IRQ1_TX_TXDONE,
    /* 14 */ DEF_IRQPARAM1 | IRQ0_TX_START_FIFOTHRESH | IRQ1_PLL_LOCK_PIN_ON,
    /* 15 */ RSSIIRQTHRESH_VALUE,
    /* 16 */ PASSIVEFILT_987 | RXFC_FOPLUS400, // modifica per set 868 Mhz a 100 Kbits
    /* 17 */ DEF_RXPARAM1 | FO_100,
    /* 18 */ DEF_RXPARAM2 | POLYPFILT_OFF | SYNC_SIZE_32 | SYNC_ON | SYNC_ERRORS_0,
    /* 19 */ DEF_RXPARAM3,
    /* 20 */ 0,
    /* 21 */ OOK_THRESH_DECSTEP_000 | OOK_THRESH_DECPERIOD_000 | OOK_THRESH_AVERAGING_00,
    /* 22 */ 0x0, // 1st byte of Sync word, 
    /* 23 */ 0x0, // 2nd byte of Sync word, 
    /* 24 */ 0x0, // 3rd byte of Sync word, 
    /* 25 */ 0x0, // 4th byte of Sync word, 
    /* 26 */ FC_400 | TXPOWER_13,
    /* 27 */ CLKOUT_OFF | CLKOUT_12800,
    /* 28 */ MANCHESTER_OFF | 64,
    /* 29 */ ADD_GT_940TRP,
    /* 30 */ PKT_FORMAT_VARIABLE | PREAMBLE_SIZE_4 | WHITENING_OFF | CRC_ON | ADRSFILT_ME_AND_00,
    /* 31 */ FIFO_AUTOCLR_ON | FIFO_STBY_ACCESS_WRITE
};
//declare external variables
extern BOOL IRQ1_Received;
extern BOOL IRQ0_Received;

extern BYTE RxPacketLen;
extern BOOL hasPacket;
extern BYTE RF_Mode;
extern BYTE TxPacket[PACKET_LEN];
extern BYTE RxPacket[PACKET_LEN];
extern BOOL Enable_DutyCycle;
extern WORD Duty_Cycle;
extern BYTE RSSIRegVal;

/*********************************************************************
 * void RegisterSet(BYTE address, BYTE value)
 *
 * Overview:
 *              This function access the control register of MRF89XA.
 *              The register address and the register settings are
 *              the input
 *
 * PreCondition:
 *            	 None
 *
 * Input:
 *          WORD    setting     The address of the register and its
 *                              corresponding settings
 *
 * Output:  None
 *
 * Side Effects:    Register settings have been modified
 *
 ********************************************************************/
void RegisterSet(BYTE address, BYTE value) {

    CSCON_SetLow();
    address = (address << 1);
    SPI1_Exchange8bit(address);
    SPI1_Exchange8bit(value);
    __delay_us(SPI_DELAY_US);
    CSCON_SetHigh();
    __delay_us(SPI_DELAY_US);

}

/*********************************************************************
 * WORD RegisterRead()
 *
 * Overview:
 *              This function access the control register of MRF89XA.
 *              The register address and the register settings are
 *              the input
 *
 * PreCondition:
 *              None
 *
 * Input:
 *          WORD    setting     The address of the register and its
 *                              corresponding settings
 *
 * Output:  None
 *
 * Side Effects:    Register settings have been modified
 *
 ********************************************************************/
BYTE RegisterRead(BYTE address) {
    BYTE value;
    CSCON_SetLow();
    address = ((address << 1) | 0x40);
    SPI1_Exchange8bit(address);
    value = SPI1_Exchange8bit(0x55);
    __delay_us(SPI_DELAY_US);
    CSCON_SetHigh();
    __delay_us(SPI_DELAY_US);

    return value;
}

/*********************************************************************
 * void Prepare_Send_Packet(voidl)
 *
 * Overview:
 *              This function is used to prepare the variables and the register of the MRF before sending the packet in the buffer TxPacket
 *
 * PreCondition:
 *              MRF89XA transceiver has been properly initialized
 *
 * Input:
 *              None
 *
 * Output:      None
 *
 *
 ********************************************************************/
void Prepare_Send_Packet() {
    SetRFMode(RF_STANDBY);

    RegisterSet(REG_PKTPARAM3, ((InitConfigRegs[REG_PKTPARAM3] & 0xBF) | FIFO_STBY_ACCESS_WRITE));
    RegisterSet(REG_IRQPARAM0, (InitConfigRegs[REG_IRQPARAM0] | IRQ1_FIFO_OVERRUN_CLEAR));
    RegisterSet(REG_IRQPARAM1, ((InitConfigRegs[REG_IRQPARAM1]) | 0x02));

}

/*********************************************************************
 * void Send_Packet(BYTE TxPacketLen)
 *
 * Overview:
 *              This function send the packet in the buffer TxPacket
 *
 * PreCondition:
 *              MRF89XA transceiver has been properly initialized
 *
 * Input:
 *              BYTE    TxPacketLen     The length of the packet to be
 *                                      sent.
 *
 * Output:      None
 *
 * Side Effects:
 *              The packet has been sent out
 *
 ********************************************************************/
void Send_Packet(BYTE TxPacketLen) {
    WORD i;

    WriteFIFO(TxPacketLen + 1);
    WriteFIFO(ADD_GT_TRP); // Node_adrs of destination
    for (i = 0; i < TxPacketLen; i++) {
        WriteFIFO(TxPacket[i]);
    }

    SetRFMode(RF_TRANSMITTER);
    //  while (!IRQ1_Received); //Wait until TX Done interrupt and restore the RF state to standby mode
   // while ((RegisterRead(0x0E) & 0x20) != 0x20) {
   // }

     while (!IO_RB1_GetValue()) {
         //__delay_ms(1);
     };
    SetRFMode(RF_STANDBY);
    IRQ1_Received = FALSE;

}

/*********************************************************************
 * void WriteFIFO(BYTE Data)
 *
 * Overview:
 *              This function fills the FIFO
 *
 * PreCondition:
 *              MRF89XA transceiver has been properly initialized
 *
 * Input:
 *              BYTE   Data - Data to be sent to FIFO.
 *
 * Output:      None
 *
 * Side Effects:
 *              The packet has been sent out
 *
 ********************************************************************/
void WriteFIFO(BYTE Data) {
    CSDATA_SetLow();
    SPI1_Exchange8bit(Data);
    __delay_us(SPI_DELAY_US);
    CSDATA_SetHigh();
    __delay_us(SPI_DELAY_US);

}

/*********************************************************************
 * void ReceiveFrame(void)
 *
 * Overview:
 *              This function reads the Reiceved frame from the fifo and sets hasPacket accordingly
 *
 * PreCondition:
 *              MRF89XA transceiver has been properly initialized
 *
 * Input:
 *              None
 *
 * Output:      None
 *
 * Side Effects:
 *              The packet has been sent out
 *
 ********************************************************************/
void ReceiveFrame() {
    BYTE data, node_adrs;
    BYTE i = 0;
    RxPacketLen = 0;
    //if (IRQ1_Received) {
    if (IO_RB1_GetValue()) {
        RSSIRegVal = (RegisterRead(REG_RSSIVALUE) >> 1);
        // accendo led verde
        RxPacketLen = ReadFIFO();
        IRQ0_Received = FALSE;
        if (RxPacketLen > 2) {
            node_adrs = ReadFIFO();
            RxPacketLen = (RxPacketLen - 1);
            while (RxPacketLen--) {
                IRQ0_Received = FALSE;
                data = ReadFIFO();
                RxPacket[i] = data;
                i++;
            };
            RxPacketLen = i;
        } else
            i = 1;
    }
    IRQ1_Received = FALSE;

    // verifica numero caratteri ricevuti
    if (i > 1) {
        hasPacket = TRUE;
    }

    //Reset FIFO
    i = RegisterRead(REG_IRQPARAM0);
    RegisterSet(REG_IRQPARAM0, (i | 0x01));
}

/*********************************************************************
 * BYTE ReadFIFO(void)
 *
 * Overview:
 *              This function reads the Reiceved frame from the fifo and sets hasPacket accordingly
 *
 * PreCondition:
 *              MRF89XA transceiver has been properly initialized
 *
 * Input:
 *              None
 *
 * Output:      Data from FIFO
 *
 * Side Effects:
 *              The packet has been sent out
 *
 ********************************************************************/
BYTE ReadFIFO(void) {
    BYTE value;
    CSDATA_SetLow();
    value = SPI1_Exchange8bit(0x55);
    __delay_us(SPI_DELAY_US);
    CSDATA_SetHigh();
    __delay_us(SPI_DELAY_US);


    return value;

}

/*********************************************************************
 * void SetRFMode(BYTE mode)
 *
 * Overview:
 *              This functions sets the MRF89XA transceiver operating mode to sleep, transmit, receive or standby
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 ********************************************************************/
void SetRFMode(BYTE mode) {
    BYTE mcparam0_read;
    mcparam0_read = RegisterRead(REG_MCPARAM0);
    switch (mode) {
        case RF_TRANSMITTER:
            RegisterSet(REG_MCPARAM0, (mcparam0_read & 0x1F) | RF_TRANSMITTER);
            RF_Mode = RF_TRANSMITTER; //RF in TX mode
            break;
        case RF_RECEIVER:
            RegisterSet(REG_MCPARAM0, (mcparam0_read & 0x1F) | RF_RECEIVER);
            RF_Mode = RF_RECEIVER; //RF in RX mode
            break;
        case RF_SYNTHESIZER:
            RegisterSet(REG_MCPARAM0, (mcparam0_read & 0x1F) | RF_SYNTHESIZER);
            RF_Mode = RF_SYNTHESIZER; //RF in Synthesizer mode
            break;
        case RF_STANDBY:
            RegisterSet(REG_MCPARAM0, (mcparam0_read & 0x1F) | RF_STANDBY);
            RF_Mode = RF_STANDBY; //RF in standby mode
            break;
        case RF_SLEEP:
            RegisterSet(REG_MCPARAM0, (mcparam0_read & 0x1F) | RF_SLEEP);
            RF_Mode = RF_SLEEP; //RF in sleep mode
            break;
    } /* end switch (mode) */

}
