#include "mcc_generated_files/i2c2.h"
#include <stdbool.h>
#include "eeprom.h"
#include "mcc_generated_files/clock.h"

#define    FCY    _XTAL_FREQ    // Instruction cycle frequency, Hz - required for __delayXXX() to work
#include <libpic30.h>        // __delayXXX() functions macros defined here

static bool eeprom_write_buffer(uint16_t data_add, uint8_t *data, uint8_t size) {
    I2C2_MESSAGE_STATUS retCode;
    uint8_t i, writebuf[size + 2];
    writebuf[0] = (data_add >> 8); // high address
    writebuf[1] = (uint8_t) (data_add); // low low address
    for (i = 0; i < size; i++) {
        writebuf[i + 2] = *data++;
    }
    I2C2_MasterWrite(writebuf, size + 2, SLAVE_DEVICE_ADDRESS, &retCode);
    while (retCode == I2C2_MESSAGE_PENDING);
    if (retCode == I2C2_MESSAGE_COMPLETE) {
        return true;
    }
    return false;
}

static bool eeprom_read_buffer(uint16_t data_add, uint8_t *data, uint8_t size) {
    I2C2_MESSAGE_STATUS retCode;
    uint8_t writebuf[2];
    writebuf[0] = (data_add >> 8); // high address
    writebuf[1] = (uint8_t) (data_add); // low low address
    I2C2_MasterWrite(writebuf, 2, SLAVE_DEVICE_ADDRESS, &retCode);
    while (retCode == I2C2_MESSAGE_PENDING);
    if (retCode == I2C2_MESSAGE_COMPLETE) {
        I2C2_MasterRead(data, size, SLAVE_DEVICE_ADDRESS, &retCode);
        while (retCode == I2C2_MESSAGE_PENDING);
        if (retCode == I2C2_MESSAGE_COMPLETE) {
            return true;
        }
    }
    return false;
}

uint8_t eeprom_read(uint16_t address) {
    uint8_t r_data = 0;
    __delay_us(100);
    __builtin_disi(0x3FFF); // disable interrupts
    eeprom_read_buffer(address, &r_data, 1);
    __builtin_disi(0x0000); // enable interrupts
    __delay_us(100);
    return r_data;
}

void eeprom_write(uint16_t address, uint8_t data) {
    uint8_t w_data = data;
    __delay_us(100);
    __builtin_disi(0x3FFF); // disable interrupts
    eeprom_write_buffer(address, &w_data, 1);
    __builtin_disi(0x0000); // enable interrupts
    __delay_us(100);
}
