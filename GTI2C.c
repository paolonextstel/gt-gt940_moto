/*
 * Getronic Srl
 * File:   GTI2C.c
 * Author: Stefano Isella
 *
 * date :           22/07/2015
 * author :         Stefano Isella
 *                  Versione 01 : Versione iniziale Gt940139
 ********************************************************************
 * date :           29/07/2015
 * author :         Stefano Isella
 *                  Versione 02 : aggiunta gestione stati autoapprendimento
 ********************************************************************/

/*
 *****************************************************************************
 *                                                                           *
 *                               INCLUDES Header                             *
 *                                                                           *
 *****************************************************************************
 */
#include "xc.h"

#include "GT940BB.h"
#include "GTI2C.h"
#include "GTI2C_var.h"

#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/i2c1.h"

#include <stdio.h>
#include <string.h>
#include <float.h>

/*
 *******************************************************************************
 *                                                                             *
 *                  Internal Prototype                                         *
 *                                                                             *
 *******************************************************************************
 */

DWORD numREad = 0;
bool salvataggio_soglie = false;
 static uint8_t int_count = 0;

void I2CDoRead(void);
void I2CDoWrite(void);
void TelBuffWrite(DataTel_t * wk);
void TelBuffRead(DataTel_t * wk);
int TelBuffIsEmpty(void);
int TelBuffIsBusy(void);
int TelBuffIsFull(void);
void reset_mskT(DWORD* pMsk);

extern BYTE stato_pic_low;
extern BYTE stato_pic_High;
extern BYTE cmd_telefono;
extern BYTE ctr_sirene_autoapp;
extern BYTE val_sirena_id;
extern BYTE val_sirena_sn_hh;
extern BYTE val_sirena_sn_hl;
extern BYTE val_sirena_sn_lh;
extern BYTE val_sirena_sn_ll;
extern BYTE val_sirena_stato;
extern BYTE val_sirena_rssi;
extern BYTE val_sirena_batteria;
extern BYTE ctr_next_sirena;
extern BYTE val_last_autop_sirena_id;
extern BYTE val_last_autop_sirena_sn_hh;
extern BYTE val_last_autop_sirena_sn_hl;
extern BYTE val_last_autop_sirena_sn_lh;
extern BYTE val_last_autop_sirena_sn_ll;
extern BYTE msk_bits_inser;
extern BYTE msk_bits_disinser;
extern BYTE sir_segn_tau_durata_beep;
extern BYTE sir_segn_abil_strobo;
extern BYTE sir_segn_tau_durata_allarme;
extern BYTE durata_sirena_allarme_tamper;
extern BYTE numero_riclicli_allarme_tamper;
extern BYTE val_batt_h;
extern BYTE val_batt_l;
extern BOOL CmdCancAll;
extern BYTE CmdCancId;
extern BYTE CmdAttDisId;
extern BYTE val_id_ultimo_trovato;
extern BYTE val_id_ultimo_trovato_last;
extern WORD timeout_verifica_presenza;
extern BYTE debounce_verifica_presenza;
extern WORD ctr_interrogazioni;
extern BYTE id_prossimita;
extern BYTE tau_polling_prossimita;
extern BYTE distanza_prossimita; // per ora il RSSI
extern WORD timeout_prossimita;
extern BYTE debounce_prossimita;
extern WORD GTKATimeout;
extern BOOL GTKATimeout_flag;
extern BYTE TauSleepI2C;
extern BYTE fase_centralina;
extern BYTE mskCmdRicerca;
extern DWORD msk_prossimita_out_multipla[];
extern BYTE EE_site_code[];
extern BYTE NEW_site_code[];
extern BYTE NEW_info_device[];
extern DWORD mskT[];
extern BYTE EE_soglia_batteria;
extern BYTE EE_soglia_rssi;
extern BYTE flagReceivingAnyTrasmitter;
extern BYTE valIdBroadFromI2c;
#ifdef CONFIGURA_940
  extern BYTE EE_940_config;
#endif

extern unsigned char get_nr_sirene_autoapp(void);
extern void get_next_sirena(void);
extern void get_sirena(BYTE id);
extern void delete_sirena(BYTE id); /**/
extern void att_diss_sirena(BYTE id); /**/
extern void get_mask(BYTE type, BYTE* idDriver);
#ifdef AUTOAPPRENDIMENTO_SMART
extern void exitAutoapprendimento(void);
#endif
extern BYTE suspendTxMng;
extern BYTE tmrEnableTxMng;
extern BYTE flagRxCmdBroadI2c;
extern BYTE val_id_ultimo_trovato_verifica;      //V24.33
extern BYTE ricBroaDaI2cInCorso;

/********************************************************************************
 *                                                                              *
 *                   External Prototype                                         *
 *                                                                              *
 ********************************************************************************
 */

/*
 ********************************************************************************
 *                                                                              *
 *                   Procedure                                                  *
 *                                                                              *
 ********************************************************************************
 */

static void reset_outbuf_i2c(void)
{
    BYTE i;
    
	for (i = 0; i < __I2CMAXOUT; i++)
	{
		outBuf[i] = 0;
	}
}


/*
 * =============================================================================
 * AddNotifyToTel
 * Add new notification to the buffer of notification for telephone
 * =============================================================================
 */
void AddNotifyToTel(unsigned int nt, unsigned int cd) {
    DataTel_t dt;
    dt.info = nt;
    dt.cod = cd;

    TelBuffWrite(&dt);
    WAKE_I2C_SetHigh();
    numREad++;
    TauSleepI2C = DEF_3SEC_BS_100MS;
    //if(!TmrI2CFail)
    //TmrI2CFail = 1;
}/*End of AddNotifyToTel*/

/*
 * =============================================================================
 * TelBuffWrite
 * Adds item to the buffer
 * *wk = pointer to the item to add
 * =============================================================================
 */
void TelBuffWrite(DataTel_t * wk) {
    bflock = 1;
    int end = (bfstart + bfcount) % __I2CSIZELIST;
    DataTel[end] = *wk;
    if (bfcount == __I2CSIZELIST)
        bfstart = (bfstart + 1) % __I2CSIZELIST;
    else
        ++ bfcount;
    bflock = 0;
}/* End of TelBuffWrite */

/*
 * =============================================================================
 * TelBuffRead
 * Reads and removes an element from the buffer
 * *wk = Pointer where to store the item
 * =============================================================================
 */
void TelBuffRead(DataTel_t * wk) {
    *wk = DataTel[bfstart];
    bfstart = (bfstart + 1) % __I2CSIZELIST;
    --bfcount;
}/* End of TelBuffRead*/

/*
 * =============================================================================
 * TelBuffIsEmpty
 * Checks if buffer is empty
 * return :  true or false
 * =============================================================================
 */
int TelBuffIsEmpty(void) {
    return bfcount == 0;
} /* End of TelBuffIsEmpty */

/*
 * =============================================================================
 * TelBuffIsBusy
 * Checks if buffer is Busy
 * return :  true or false
 * =============================================================================
 */
int TelBuffIsBusy(void) {
    return bflock == 1;
} /* End of TelBuffIsBusy */

/*
 * =============================================================================
 * TelBuffIsFull
 * Checks if buffer is full
 *  *buf = pointer to the buffer
 * return :  true or false
 * =============================================================================
 */
int TelBuffIsFull(void) {
    return bfcount == __I2CSIZELIST;
} /* End of TelBuffIsFull */

/*
 *==============================================================================
 * I2CRoutine
 * Interrupt routine to manage Slave operation
 *==============================================================================
 *//*
void I2CRoutine(void) {
    unsigned int loc = 0;
    unsigned int lout = 0;

    TauSleepI2C = DEF_3SEC_BS_100MS;
    //-------    int statI2C = SSP2STAT;
    //	bit 5 D/A D=1 A=0
    //	bit 4 P STop bit (1= detected) - not used
    //	bit 3 S Start bit (1 = detecetd)
    //	bit 2 R_W bit (1= Read)
    //	bit 0 Recv full (1= receive buffer full) - not used
    //	bit 0 Trasmit buffer full (1= transmit in progress)
    // ---------------------------------------------------------

    if (SSP2CON1 & 0x0040) { // Receive Overflow
        SSP2CON1bits.SSPOV = 0;
        loc = SSP2BUF;
        pntIn = 0;
        I2CFlags.Word = 0;
        I2CWriteCmd = 0;
        I2CReadCmd = 0;
        inBuf[0] = 0;
        len_msg_i2c = 0;
        return;
    }


    // Ricezione primo carattere da parte di Master i2c
    // Write
    if ((SSP2STAT & 0x002D) == 0x0009) {
        // address, start condition, write,
        pntIn = 0;
        loc = SSP2BUF;
        pntIn = 0;
        I2CFlags.Word = 0;
        I2CWriteCmd = 0;
        I2CReadCmd = 0;
        inBuf[0] = 0;
        len_msg_i2c = 0;
        return;
    }

    // ricezione caratteri successivi
    if ((SSP2STAT & 0x002D) == 0x0029) {
        // dat , start condition, write,
        loc = SSP2BUF;
        inBuf[pntIn++] = loc;
        if (pntIn == 1) {
            // lettura comando di stato
            // verifica se comando di lettura o scrittura
            if (((loc & 0x80) == 0) && (loc != CMDW_PIC_KEEP_ALIVE_I2C)) {
                // comando di lettura
                I2CFlags.Bits.Write = 0;
                I2CFlags.Bits.Read = 1;
                len_msg_i2c = 3;
            } else {
                // comando di scrittura
                I2CFlags.Bits.Write = 1;
                I2CFlags.Bits.Read = 0;
                len_msg_i2c = 16;
                //len_msg_i2c = 9;
            }
            return;
        }
        // numero massimo di caratteri ricevuti
        // set comando di lettura o scrittura
        if (pntIn >= len_msg_i2c) {
            // ricevuto ultimo carattere
            // verifico tipo di comando
            if (I2CFlags.Bits.Write == 1) {
                // comando di scrittura
                I2CWriteCmd = inBuf[0];
                I2CDoWrite();
            } else
                // comando di lettura
                I2CReadCmd = inBuf[0];
        }
        return;
    }


    // ricezione primo comando di lettura
    if ((SSP2STAT & 0x002C) == 0x000C) {
        // address, start condition, read,
        pntOut = 0;
        if (!I2CFlags.Bits.Read) {
            I2CReadCmd = CMDR_LETTURA_CODA_EVENTI;
            I2CFlags.Bits.Write = 0;
        }
        I2CDoRead();
        SSP2BUF = outBuf[pntOut++];
        SSP2CON1bits.CKP = 1; // Release SCL
        return;
    }

    // ricezione successivi comandi di lettura
    if ((SSP2STAT & 0x002C) == 0x002C) {
        // data, start condition, read,
        if (pntOut < __I2CMAXOUT)
            lout = outBuf[pntOut++];
        else
            lout = 0;
        SSP2BUF = lout;
        SSP2CON1bits.CKP = 1; // Release SCL
        return;
    }

    if ((SSP2STAT & 0x0010)) { // Detect Stop Condiction
        loc = SSP2BUF;
        if (I2CFlags.Bits.Write) {
            inBuf[pntIn++] = loc;
            if (pntIn >= len_msg_i2c) {
                // verifica se comando di write
                if (I2CFlags.Bits.Write == 1) {
                    // comando di write
                    I2CWriteCmd = inBuf[0];
                    I2CDoWrite();
                }
            }
            return;
        }
        I2CFlags.Word = 0;
        pntIn = 0;
        pntOut = 0;
        len_msg_i2c = 0;
        return;
    }

    loc = SSP2BUF;
    SSP2CON1bits.CKP = 1; // Release SCL
    I2CFlags.Word = 0;
    pntIn = 0;
    pntOut = 0;
    len_msg_i2c = 0;

}
*/
/*End of I2CRoutine*/

static I2CFlags_t I2CFlags;
static uint8_t i2c1_slaveWriteData = 0xAA;
static uint8_t dbg0 = 0, dbg1 = 0, dbg2 = 0, dbg3 = 0, dbg4 = 0;

bool I2C1_StatusCallback(I2C1_SLAVE_DRIVER_STATUS status) {
    /*     --------------------------------------     
         Data Write Instruction:
         |Start|slave Addr + w|Ack|CommandByte|Ack|dataByte 0|Ack|...|data Byte 15|Nack|Stop|
     
         Data Read Instruction:
         |Start|slave Addr + w|Ack|AddrByte1|Ack|AddrByte2|Ack|AddrByte3|Ack||Start|slave Addr + r|Ack|dataByte 0|Ack|...|dataByte 15|Nack|Stop|
     */

    switch (status) {
        case I2C1_SLAVE_TRANSMIT_REQUEST_DETECTED: //Entra qui prima di ogni byte letto da master
            dbg0++;
            // set up the slave driver buffer transmit pointer
            if (I2CFlags.Read == 0) {
                I2CFlags.Read = 1;
                // Lettura generica su segnale filo rosa-nero
                I2CReadCmd = CMDR_LETTURA_CODA_EVENTI;
                // address, start condition, read,
                I2CDoRead();
                pntOut = 0;
                len_msg_i2c = 7;
            }

            I2C1_ReadPointerSet(&outBuf[pntOut++]);
            if (pntOut >= len_msg_i2c) {
                // Ho inviato l'ultimo byte
                I2CFlags.Read = 0;
            }
            break;

        case I2C1_SLAVE_RECEIVE_REQUEST_DETECTED: //Entra prima del primo byte scritto da master
            dbg1++;
            // Ricezione primo carattere da parte di Master i2c
            // Write
            pntIn = 0;
            I2CFlags.firstByte = 1;
            I2CWriteCmd = 0;
            I2CReadCmd = 0;
            inBuf[0] = 0;
            len_msg_i2c = 0;

            // set up the slave driver buffer receive pointer
            I2C1_WritePointerSet(&i2c1_slaveWriteData);

            break;

        case I2C1_SLAVE_RECEIVED_DATA_DETECTED: //Entra dopo ogni byte scritto da master
            dbg2++;
            // ricezione caratteri successivi
            inBuf[pntIn++] = i2c1_slaveWriteData;
            if (I2CFlags.firstByte == 1) { //verifico se � un comando di lettura o scrittura
                dbg3++;
                I2CFlags.firstByte = 0;
                // get the address of the memory being written
                if (((i2c1_slaveWriteData & 0x80) == 0) && (i2c1_slaveWriteData != CMDW_PIC_KEEP_ALIVE_I2C)) {
                    // comando di lettura
                    I2CFlags.Write = 0;
                    I2CFlags.Read = 1;
                    len_msg_i2c = 3;
                } else {
                    // comando di scrittura
                    I2CFlags.Write = 1;
                    I2CFlags.Read = 0;
                    len_msg_i2c = 16;
                }
            } else {
                dbg4++;
                // numero massimo di caratteri ricevuti
                // set comando di lettura o scrittura
                if (pntIn >= len_msg_i2c) {
                    // ricevuto ultimo carattere
                    // verifico tipo di comando
                    if (I2CFlags.Write == 1) {
                        // comando di scrittura
                        I2CWriteCmd = inBuf[0];
                        asm("NOP");
                        I2CDoWrite();
                        I2CFlags.Write = 0;
                    } else if (I2CFlags.Read == 1) {
                        // } else {
                        // comando di lettura
                        I2CReadCmd = inBuf[0];
                        // address, start condition, read,
                        pntOut = 0;
                        len_msg_i2c = 16;
                        I2CDoRead();
                    }
                }
            }

            /*    if ((SSP2STAT & 0x0010)) { // Detect Stop Condiction
                    loc = SSP2BUF;
                    if (I2CFlags.Bits.Write) {
                        inBuf[pntIn++] = loc;
                        if (pntIn >= len_msg_i2c) {
                            // verifica se comando di write
                            if (I2CFlags.Bits.Write == 1) {
                                // comando di write
                                I2CWriteCmd = inBuf[0];
                                I2CDoWrite();
                            }
                        }
                        return;
                    }
                    I2CFlags.Word = 0;
                    pntIn = 0;
                    pntOut = 0;
                    len_msg_i2c = 0;
                }*/
            break;

        case I2C1_SLAVE_10BIT_RECEIVE_REQUEST_DETECTED:

            // do something here when 10-bit address is detected

            // 10-bit address is detected

            break;

        case I2C1_SLAVE_OVERFLOW_DETECTED:
            inBuf[0] = i2c1_slaveWriteData;
            inBuf[1] = i2c1_slaveWriteData;
            break;

        default:
            break;

    }
    return true;
}

/*
 *==============================================================================
 * I2CDoRead
 * Perform the read operation
 *==============================================================================
 */

void reset_mskT(DWORD* pMsk)
{
	BYTE i;

	for (i = 0; i < 4; i++)
	{
		*pMsk = 0;
		pMsk++;
	}
}

static void put_mskT_in_outBuf(void)
{
	outBuf[0] = (BYTE)(mskT[0] & 0xFF);
	outBuf[1] = (BYTE)((mskT[0] & 0xFF00) >> 8);
	outBuf[2] = (BYTE)((mskT[0] & 0xFF0000) >> 16);
	outBuf[3] = (BYTE)((mskT[0] & 0xFF000000) >> 24);

	outBuf[4] = (BYTE)(mskT[1] & 0xFF);
	outBuf[5] = (BYTE)((mskT[1] & 0xFF00) >> 8);
	outBuf[6] = (BYTE)((mskT[1] & 0xFF0000) >> 16);
	outBuf[7] = (BYTE)((mskT[1] & 0xFF000000) >> 24);

	outBuf[8] = (BYTE)(mskT[2] & 0xFF);
	outBuf[9] = (BYTE)((mskT[2] & 0xFF00) >> 8);
	outBuf[10] = (BYTE)((mskT[2] & 0xFF0000) >> 16);
	outBuf[11] = (BYTE)((mskT[2] & 0xFF000000) >> 24);

	outBuf[12] |= (BYTE)(mskT[3] & 0xFF);
}


void I2CDoRead(void) {
    DataTel_t dt;

	reset_outbuf_i2c();
    switch (I2CReadCmd) {
        case 1:
            // richiesta stato polling
            // reset causa power ON
            outBuf[0] = stato_pic_low;
            outBuf[1] = stato_pic_High;
			outBuf[5] = SUBVERS_940MT;
            outBuf[6] = VERS_940MT;
            break;

        case 2:
            //case 3:
            //case 4:
            // richiesta numero sirene autoapprese
        {
			get_mask(PRESENZA, 0);
			put_mskT_in_outBuf();
        }
            break;
        case 3:
            // richiesta numero sirene autoapprese
        {
			get_mask(TEST, 0);
			put_mskT_in_outBuf();
        }
            break;
        case 4:
            // richiesta numero sirene autoapprese
        {
			get_mask(ANOMALIA, 0);
			put_mskT_in_outBuf();
        }
            break;
        case 5:
            // Richiesta trasponder che hanno risposto alla ricerca broadcast
        {
            BYTE idDriver = 0;

			get_mask(BROADCAST, &idDriver);

			// maschere trasponders presenti
			put_mskT_in_outBuf();

            // ID trasponder attivo alla guida
            outBuf[13] = idDriver;
        }
            break;
        case 6:
            // Comando di lettura maschera evento prossimita
            // Con prossimita IN indica i trasponder presenti allo scadere del TIMEOUT (la funzione termina)
            // Con prossimita OUT indica i trasponder che sono usciti dal raggio (la funzione non termina)
            outBuf[0] = (BYTE) (msk_prossimita_out_multipla[0] & 0xFF);
            outBuf[1] = (BYTE) ((msk_prossimita_out_multipla[0] & 0xFF00) >> 8);
            outBuf[2] = (BYTE) ((msk_prossimita_out_multipla[0] & 0xFF0000) >> 16);
            outBuf[3] = (BYTE) ((msk_prossimita_out_multipla[0] & 0xFF000000) >> 24);

            outBuf[4] = (BYTE) (msk_prossimita_out_multipla[1] & 0xFF);
            outBuf[5] = (BYTE) ((msk_prossimita_out_multipla[1] & 0xFF00) >> 8);
            outBuf[6] = (BYTE) ((msk_prossimita_out_multipla[1] & 0xFF0000) >> 16);
            outBuf[7] = (BYTE) ((msk_prossimita_out_multipla[1] & 0xFF000000) >> 24);

            outBuf[8] = (BYTE) (msk_prossimita_out_multipla[2] & 0xFF);
            outBuf[9] = (BYTE) ((msk_prossimita_out_multipla[2] & 0xFF00) >> 8);
            outBuf[10] = (BYTE) ((msk_prossimita_out_multipla[2] & 0xFF0000) >> 16);
            outBuf[11] = (BYTE) ((msk_prossimita_out_multipla[2] & 0xFF000000) >> 24);

            outBuf[12] = (BYTE) (msk_prossimita_out_multipla[3] & 0xFF);
            break;
        case 7:
            outBuf[0] = EE_site_code[0];
            outBuf[1] = EE_site_code[1];
            outBuf[2] = EE_site_code[2];
            outBuf[3] = EE_site_code[3];
            break;
        case 8:
            //  outBuf[0] = eeprom_read(inBuf[1]);
            break;
        case 0x20:
            // Lettura richiesta informazione singolo dispositivo autoappreso
            // Comando
            // Byte 0 = comando 
            // Byte 1 = tipo dispositivo (1= sirena)
            // Byte 2 = ID del dispositivo
            // Risposta
            // byte 0 = indice sirena
            // byte 1 = numero serie HH
            // byte 2 = numero serie HL
            // byte 3 = numero serie LH
            // byte 4 = numero serie LL
            // byte 5 = stato trasponder
            // byte 6 = RSSI 7bit meno significativi, bit7 bit + significativo livello bat
            // byte 7 = 8bit meno significativi livello bat

            // verifica tipo di dispositivo 
            if (inBuf[1] == 1) {
                if (inBuf[2] <= MAX_EEPR_NR_TRASPONDER) {
                    get_sirena(inBuf[2]);
                    // Leggiamo i dati del dispositivo
                    if (val_sirena_id != 0) {
                        // sirena presente
                        outBuf[0] = val_sirena_id;
                        outBuf[1] = val_sirena_sn_hh;
                        outBuf[2] = val_sirena_sn_hl;
                        outBuf[3] = val_sirena_sn_lh;
                        outBuf[4] = val_sirena_sn_ll;
                        outBuf[5] = val_sirena_stato;
                        outBuf[6] = val_sirena_rssi;
                        outBuf[7] = val_sirena_batteria;

                    } else {
                        // sirene terminate
                        ctr_next_sirena = 0;
                    }
                } else {
                    ctr_next_sirena = 0;
                }
            }
            break;

        case 0x21:
            // Lettura richiesta informazione ultimo dispositivo autoappreso
            // Comando
            // Byte 0 = comando 
            // Byte 1 = 0 non usato
            // Byte 2 = 0 non usato
            // Risposta
            // byte 0 = indice sirena
            // byte 1 = numero serie HH
            // byte 2 = numero serie HL
            // byte 3 = numero serie LH
            // byte 4 = numero serie LL
            // byte 5 = stato trasponder
            // byte 6 = RSSI 7bit meno significativi, bit7 bit + significativo livello bat
            // byte 7 = 8bit meno significativi livello bat

            // verifica tipo di dispositivo 
            if (val_last_autop_sirena_id != 0) {
                // sirena presente
                get_sirena(val_last_autop_sirena_id);

                outBuf[0] = val_last_autop_sirena_id;
                outBuf[1] = val_last_autop_sirena_sn_hh;
                outBuf[2] = val_last_autop_sirena_sn_hl;
                outBuf[3] = val_last_autop_sirena_sn_lh;
                outBuf[4] = val_last_autop_sirena_sn_ll;
                outBuf[5] = val_sirena_stato;
                outBuf[6] = val_sirena_rssi;
                outBuf[7] = val_sirena_batteria;
            }
            break;
        case 0x22:
            // Comando di cancellazione singolo dispositivo
            // Viene utilizzato un comando di lettura per dare un ACK immediato sul comando
            // Comando
            // Byte 0 = comando
            // Byte 1 = tipo dispositivo (per mantenere stessa struttura ma non viene guardato)
            // Byte 2 = id dispositivo
            // Risposta
            // Byte 0 = esito ( 0 --> KO; 1 --> OK)
            // Byte 1..7 = non usati

            // verifica tipo di dispositivo 
            if (inBuf[1] == 1) {
                if (inBuf[2] == CMD_DEL_ALL) {
                    // Bisogna cancellare tutti i trasponder
                    //delete_sirena(CMD_DEL_ALL);
                    CmdCancAll = TRUE;
                    cmd_telefono = CMDW_TEL_CANC_ON;
                    outBuf[0] = 1; // Esito cancellazione OK
                } else {
                    get_sirena(inBuf[2]);
                    if (val_sirena_id != 0) {
                        CmdCancId = inBuf[2];
                        cmd_telefono = CMDW_TEL_CANC_ON;
                        outBuf[0] = 1; // Esito cancellazione OK
                    }
                }
            }

            break;
        case 0x23:
            // Comando di attivazione/disattivazione traponder
            // Viene utilizzato un comando di lettura per dare un ACK immediato sul comando
            // Comando
            // Byte 0 = comando
            // Byte 1 = tipo dispositivo (per mantenere stessa struttura ma non viene guardato)
            // Byte 2 = id dispositivo (se da attivare)/ id | 0x80 se da disattivare
            // Risposta
            // Byte 0 = esito ( 0 --> KO; 1 --> OK)
            // Byte 1..7 = non usati

            // verifica tipo di dispositivo 
            if (inBuf[1] == 1) {
                get_sirena((inBuf[2] & CONFIG_MASK_BIT_ID_TRASP));
                if (val_sirena_id != 0) {
                    outBuf[0] = 1; // Esito  OK
                    CmdAttDisId = inBuf[2];
                    cmd_telefono = CMDW_TEL_CANC_ON; // per indicare al telefono di leggere la nuova configurazione
                }
            }
            break;
        case 0x24:
            // Lettura richiesta informazione trasponder presente
            // Comando
            // Byte 0 = comando 
            // Byte 1 = 0 non usato
            // Byte 2 = 0 non usato
            // Risposta richiesta info
            // byte 0 = indice sirena
            // byte 1 = msk stato trasponder
            // byte 2 = livello batteria
            // byte 3 = RSSI

            if (val_id_ultimo_trovato_last) {
                get_sirena(val_id_ultimo_trovato_last);
                if (val_sirena_id != 0) {
                    // Il dispositivo esiste nella configurazione

                    // inseriamo i valori
                    outBuf[0] = val_sirena_id;
                    outBuf[1] = val_sirena_stato;
                    outBuf[2] = val_sirena_batteria;
                    outBuf[3] = val_sirena_rssi;
                }
            }

            break;
        case CMDR_LETTURA_CODA_EVENTI:
            // Lettura richiesta coda eventi
            // Comando di lettura I2C diretta (senza byte di comando in scrittura)
            // Risposta diretta
            // byte 0 = Non valorizzato (per corrispondenza con 937)
            // byte 1 = Non valorizzato (per corrispondenza con 937)
            // byte 2 = Codice Evento MSB
            // byte 3 = Codice Evento LSB
            // byte 4 = Dato Evento MSB
            // byte 5 = Dato Evento LSB
            // byte 6 = Valore di verifica
            // comando di lettura diretta --> quindi senza un reale comando inviato dal master
            //    richiesta diretta di lettura senza scrittura antecedente
            if (TelBuffIsEmpty() == 0) {
                if (TelBuffIsBusy()) {
                    // Buffer occupato
                    outBuf[6] = TAIL_BUSY;
                } else {
                    // Inseriamo i dati del buffer
                    WAKE_I2C_SetLow();
                    TelBuffRead(&dt);

                    outBuf[0] = (numREad & 0xFF00) << 8;
                    outBuf[1] = (numREad & 0xFF);
                    outBuf[2] = (dt.info & 0xFF00) >> 8;
                    outBuf[3] = (dt.info & 0xFF);
                    outBuf[4] = (dt.cod & 0xFF00) >> 8;
                    outBuf[5] = (dt.cod & 0xFF);

                    outBuf[6] = DATA_OK;
                }
            }
            break;
        default:
            break;
    }

    I2CReadCmd = 0;
}/*End of I2CDoRead*/

void I2CDoWrite(void) {

	//if (flagReceivingAnyTrasmitter)
	//	flagReceivingAnyTrasmitter = 0;

    switch (I2CWriteCmd) {
        case CMDW_PIC_AUTOAPP_ONOFF:
            if (inBuf[1] == 0)
            {
              #ifdef AUTOAPPRENDIMENTO_SMART
				exitAutoapprendimento();
              #else
                // Autoapprendimento OFF
                cmd_telefono = CMDW_TEL_AUTOPP_OFF;    
              #endif 
            } else {
                //  Autoapprendimento ON
                cmd_telefono = CMDW_TEL_AUTOPP_ON;
            }
            break;
        case CMDW_PIC_TEST_ONOFF:
            if (inBuf[1] == 0) {
                // Test OFF
                cmd_telefono = CMDW_TEL_TEST_OFF;
            } else {
                //  Test ON
                cmd_telefono = CMDW_TEL_TEST_ON;
               
                //porto da cmd ricerca broadcast
                flagRxCmdBroadI2c = 2;
                suspendTxMng = 1;
                tmrEnableTxMng = 10;
            }
            break;

        case CMDW_PIC_CANC_ONOFF:
            if (inBuf[1] == 0) {
                // Cancellazione OFF
                cmd_telefono = CMDW_TEL_CANC_OFF;
            } else {
                //  Cancellazione ON
                cmd_telefono = CMDW_TEL_CANC_ON;
            }
            break;

        case CMDW_PIC_RICERCA_TRAS:
            //  Supervisione  ON
            if (inBuf[1]) {
                cmd_telefono = CMDW_TEL_SUPERV_ON;
                val_id_ultimo_trovato = inBuf[1];
            }
            break;
        case CMDW_PIC_SLEEP:
            cmd_telefono = CMDW_TEL_SLEEP_PIC;
            // da fare parte in cui va in sleep
            break;
        case CMDW_PIC_FERMA_VERIFICA:
            // Ferma verifica presenza trasponder
            fase_centralina = FASE_940_RUN;
            cmd_telefono = CMDW_TEL_VERIFICA_OFF;
            break;
        case CMDW_PIC_ATTIVA_VERIFICA:
            // Attiva verifica presenza trasponder
            // Verifico se riassegnare l'ID di verifica
            if (inBuf[1] != 0xF2)
                val_id_ultimo_trovato_verifica = inBuf[1];
            else 
                val_id_ultimo_trovato_verifica = 1;
				//val_id_ultimo_trovato = inBuf[1];
            timeout_verifica_presenza = (inBuf[2] << 8) | inBuf[3];
            mskCmdRicerca = (inBuf[4] & CMDW_PIC_BIT_RICERCA_C) ? CMDW_PIC_BIT_RICERCA_C : 0;
            debounce_verifica_presenza = inBuf[5];
            cmd_telefono = CMDW_TEL_ATT_VERIFICA;
            break;
        case CMDW_PIC_PROSSIMITA_IN:
            //  Prossimita IN ON
            id_prossimita = inBuf[1]; // Se == a zero indica prossimita broadcast
            timeout_prossimita = (inBuf[2] << 8) | inBuf[3];
            tau_polling_prossimita = inBuf[4];
            distanza_prossimita = inBuf[5]; // per ora in RSSI
            debounce_prossimita = inBuf[6];
            cmd_telefono = CMDW_TEL_PROSS_IN_START;
            break;
        case CMDW_PIC_FERMA_PROSS_IN:
            // Prossimita IN OFF
            cmd_telefono = CMDW_TEL_PROSS_IN_STOP;
            break;
        case CMDW_PIC_PROSSIMITA_OUT:
            //  Prossimita OUT ON
            id_prossimita = inBuf[1];
            timeout_prossimita = (id_prossimita) ? (inBuf[2] << 8) | inBuf[3] : 0;
            tau_polling_prossimita = inBuf[4];
            distanza_prossimita = inBuf[5]; // per ora il RSSI
            debounce_prossimita = (id_prossimita) ? 0 : inBuf[6];
            cmd_telefono = CMDW_TEL_PROSS_OUT_START;
            break;
        case CMDW_PIC_FERMA_PROSS_OUT:
            // Prossimita OUT OFF
            cmd_telefono = CMDW_TEL_PROSS_OUT_STOP;
            break;
        case CMDW_PIC_KEEP_ALIVE_I2C:
        {
            WORD tau = ((inBuf[1] << 8) + inBuf[2]);
            GTKATimeout = (WORD) (tau / 2); // base tempi 2ms
            cmd_telefono = CMDW_TEL_GST_KEEP_ALIVE;
        }
            break;
        case CMDW_PIC_FERMA_RICERCA:
            cmd_telefono = CMDW_TEL_RICERCA_OFF;
            break;
        case CMDW_PIC_RICERCA_BROADCAST:
            if(fase_centralina == FASE_940_RICERCA_BROADCAST && ricBroaDaI2cInCorso)
                break;
            if (inBuf[1] != 0xF2)
				val_id_ultimo_trovato = valIdBroadFromI2c = inBuf[1];

            ricBroaDaI2cInCorso = 1;
            mskCmdRicerca = (inBuf[2] & CMDW_PIC_BIT_RICERCA_C) ? CMDW_PIC_BIT_RICERCA_C : 0;
            int_count++;
            cmd_telefono = CMDW_TEL_RICERCA_BROADCAST;
            //flagReceivingAnyTrasmitter = 0;
            flagRxCmdBroadI2c = 1;
            suspendTxMng = 1;
            tmrEnableTxMng = 10;
            break;
        case CMDW_PIC_WRITE_SYNC_WORD:
            NEW_site_code[0] = inBuf[1];
            NEW_site_code[1] = inBuf[2];
            NEW_site_code[2] = inBuf[3];
            NEW_site_code[3] = inBuf[4];
            cmd_telefono = CMDW_TEL_WRITE_SYNC_CODE;
            break;
        case CMDW_PIC_ADD_DEVICE:
            NEW_info_device[0] = inBuf[1]; //	ID trasponder
            NEW_info_device[1] = inBuf[2]; //	SN HH
            NEW_info_device[2] = inBuf[3]; //	SN HL
            NEW_info_device[3] = inBuf[4]; //	SN LH
            NEW_info_device[4] = inBuf[5]; //	SN LL
            cmd_telefono = CMDW_TEL_ADD_DEVICE;
            break;
        case CMDW_PIC_SOGLIE:
            EE_soglia_batteria = inBuf[1];
            EE_soglia_rssi = inBuf[2];
            salvataggio_soglie = true;
            break;
      #ifdef CONFIGURA_940
        case CMDW_PIC_940_CONFIG:
            EE_940_config = inBuf[1];
			eeprom_write(EE_940_CONFIG, EE_940_config);
            if(inBuf[1] & MASK_IN_940CFG_USETX)
                stato_pic_High |= STATO_PIC_USE_TX;
            else
                stato_pic_High &= ~STATO_PIC_USE_TX;
            break;    
      #endif
        default:
            break;

    }

    I2CWriteCmd = 0;
}/*End of I2CDoRead*/

/*
 EOF
 */
