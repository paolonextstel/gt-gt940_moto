/* 
 * Getronic srl
 * File:   GTI2C_var.h
 * Author: Stefano Isella
 *
 * Created on 8 gennaio 2015
 */
#include <stdbool.h>
#include "GTI2C.h"
#include "GenericTypeDefs.h"

#ifndef GTI2C_VAR_H
#define	GTI2C_VAR_H

BYTE inBuf[__I2CMAXIN];
BYTE outBuf[__I2CMAXOUT];
BYTE pntOut;
BYTE pntIn;
BYTE I2CReadCmd;
BYTE I2CWriteCmd;
BYTE len_msg_i2c;
BYTE bfstart;
BYTE bfcount;
BYTE bflock;

typedef struct {
    bool Read;
    bool Write;
    bool firstByte;
} I2CFlags_t;

typedef struct DataTel_t {
    unsigned int info;
    unsigned int cod;
} DataTel_t;

DataTel_t DataTel[__I2CSIZELIST];


#endif	/* GTI2C_VAR_H */

