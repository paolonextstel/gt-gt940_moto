/*********************************************************************
 *
 *                  MRF89XA GT940TRP MAIN Function
 *
 *********************************************************************
 * FileName:        GT940TRP.c
 * Dependencies:    None
 * Processor:       PIC16FL1829
 * date :           09/12/2015
 * author :         Stefano Isella
                    Questa versione deriva dal GT940139 versione 09
 ********************************************************************/

#include "xc.h"
//config bits that are part-specific for the PIC16F1829
//#pragma config FOSC=INTOSC, WDTE=SWDTEN, PWRTE=ON, MCLRE=ON, CP=OFF, CPD=OFF, BOREN=OFF, CLKOUTEN=OFF, IESO=OFF, FCMEN=OFF
//#pragma config WRT=OFF, PLLEN=OFF, STVREN=ON, BORV=HI, LVP=OFF

#include "GT940BB.h"
#include "GTi2c.h"
#include "MRF89XA.h"
#include "eeprom.h"
#include <stdio.h>
#include <string.h>
#include <float.h>
//#include "mcc_generated_files/ext_int.h"
#include "mcc_generated_files/tmr1.h"
#include "mcc_generated_files/i2c1.h"
#include "mcc_generated_files/clock.h"
#include "mcc_generated_files/pin_manager.h"


#define    FCY    _XTAL_FREQ    // Instruction cycle frequency, Hz - required for __delayXXX() to work
#include <libpic30.h>        // __delayXXX() functions macros defined here

// ATTENZIONE ABILITARE IL BOR con MODALITA NON SLEEP
/*
// valore di default della eeprom in fase di programmazione microcontrollore
// numero di serie trasponder ID 1..4
__EEPROM_DATA(0x01, VAL_STATO_NULL, 0x02, VAL_STATO_NULL, 0x03, VAL_STATO_NULL, 0x04, VAL_STATO_NULL);
// numero di serie trasponder ID 5..8
__EEPROM_DATA(0x05, VAL_STATO_NULL, 0x06, VAL_STATO_NULL, 0x07, VAL_STATO_NULL, 0x08, VAL_STATO_NULL);
// numero di serie trasponder ID 9..12
__EEPROM_DATA(0x09, VAL_STATO_NULL, 0x0A, VAL_STATO_NULL, 0x0B, VAL_STATO_NULL, 0x0C, VAL_STATO_NULL);
// numero di serie trasponder ID 13..16
__EEPROM_DATA(0x0D, VAL_STATO_NULL, 0x0E, VAL_STATO_NULL, 0x0F, VAL_STATO_NULL, 0x10, VAL_STATO_NULL);
// numero di serie trasponder ID 17..20
__EEPROM_DATA(0x11, VAL_STATO_NULL, 0x12, VAL_STATO_NULL, 0x13, VAL_STATO_NULL, 0x14, VAL_STATO_NULL);
// numero di serie trasponder ID 21..24
__EEPROM_DATA(0x15, VAL_STATO_NULL, 0x16, VAL_STATO_NULL, 0x17, VAL_STATO_NULL, 0x18, VAL_STATO_NULL);
// numero di serie trasponder ID 25..28
__EEPROM_DATA(0x19, VAL_STATO_NULL, 0x1A, VAL_STATO_NULL, 0x1B, VAL_STATO_NULL, 0x1C, VAL_STATO_NULL);
// numero di serie trasponder ID 27..32
__EEPROM_DATA(0x1D, VAL_STATO_NULL, 0x1E, VAL_STATO_NULL, 0x1F, VAL_STATO_NULL, 0x20, VAL_STATO_NULL);
// numero di serie trasponder ID 33..36
__EEPROM_DATA(0x21, VAL_STATO_NULL, 0x22, VAL_STATO_NULL, 0x23, VAL_STATO_NULL, 0x24, VAL_STATO_NULL);
// numero di serie trasponder ID 37..40
__EEPROM_DATA(0x25, VAL_STATO_NULL, 0x26, VAL_STATO_NULL, 0x27, VAL_STATO_NULL, 0x28, VAL_STATO_NULL);
// numero di serie trasponder ID 41..44
__EEPROM_DATA(0x29, VAL_STATO_NULL, 0x2A, VAL_STATO_NULL, 0x2B, VAL_STATO_NULL, 0x2C, VAL_STATO_NULL);
// numero di serie trasponder ID 45..48
__EEPROM_DATA(0x2D, VAL_STATO_NULL, 0x2E, VAL_STATO_NULL, 0x2F, VAL_STATO_NULL, 0x30, VAL_STATO_NULL);
// numero di serie trasponder ID 49..52
__EEPROM_DATA(0x31, VAL_STATO_NULL, 0x32, VAL_STATO_NULL, 0x33, VAL_STATO_NULL, 0x34, VAL_STATO_NULL);
// numero di serie trasponder ID 53..56
__EEPROM_DATA(0x35, VAL_STATO_NULL, 0x36, VAL_STATO_NULL, 0x37, VAL_STATO_NULL, 0x38, VAL_STATO_NULL);
// numero di serie trasponder ID 57..60
__EEPROM_DATA(0x39, VAL_STATO_NULL, 0x3A, VAL_STATO_NULL, 0x3B, VAL_STATO_NULL, 0x3C, VAL_STATO_NULL);
// numero di serie trasponder ID 61..64
__EEPROM_DATA(0x3D, VAL_STATO_NULL, 0x3E, VAL_STATO_NULL, 0x3F, VAL_STATO_NULL, 0x40, VAL_STATO_NULL);
// numero di serie trasponder ID 65..68
__EEPROM_DATA(0x41, VAL_STATO_NULL, 0x42, VAL_STATO_NULL, 0x43, VAL_STATO_NULL, 0x44, VAL_STATO_NULL);
// numero di serie trasponder ID 69..72
__EEPROM_DATA(0x45, VAL_STATO_NULL, 0x46, VAL_STATO_NULL, 0x47, VAL_STATO_NULL, 0x48, VAL_STATO_NULL);
// numero di serie trasponder ID 73..76
__EEPROM_DATA(0x49, VAL_STATO_NULL, 0x4A, VAL_STATO_NULL, 0x4B, VAL_STATO_NULL, 0x4C, VAL_STATO_NULL);
// numero di serie trasponder ID 77..80
__EEPROM_DATA(0x4D, VAL_STATO_NULL, 0x4E, VAL_STATO_NULL, 0x4F, VAL_STATO_NULL, 0x50, VAL_STATO_NULL);
// numero di serie trasponder ID 81..84
__EEPROM_DATA(0x51, VAL_STATO_NULL, 0x52, VAL_STATO_NULL, 0x53, VAL_STATO_NULL, 0x54, VAL_STATO_NULL);
// numero di serie trasponder ID 85..88
__EEPROM_DATA(0x55, VAL_STATO_NULL, 0x56, VAL_STATO_NULL, 0x57, VAL_STATO_NULL, 0x58, VAL_STATO_NULL);
// numero di serie trasponder ID 89..92
__EEPROM_DATA(0x59, VAL_STATO_NULL, 0x5A, VAL_STATO_NULL, 0x5B, VAL_STATO_NULL, 0x5C, VAL_STATO_NULL);
// numero di serie trasponder ID 93..96
__EEPROM_DATA(0x5D, VAL_STATO_NULL, 0x5E, VAL_STATO_NULL, 0x5F, VAL_STATO_NULL, 0x60, VAL_STATO_NULL);
// numero di serie trasponder ID 97..100
__EEPROM_DATA(0x61, VAL_STATO_NULL, 0x62, VAL_STATO_NULL, 0x63, VAL_STATO_NULL, 0x64, VAL_STATO_NULL);
// numero di serie trasponder ID 101..104
__EEPROM_DATA(0x65, VAL_STATO_NULL, 0x66, VAL_STATO_NULL, 0x67, VAL_STATO_NULL, 0x68, VAL_STATO_NULL);

// dati di SITE CODE
//__EEPROM_DATA(0x33, 0x44, 0x55, 0x66+VERS_940TRP, 0x00, 0x00, 0x00, 0x00);
__EEPROM_DATA(0x33, 0x44, 0x55, 0x77, 0x00, 0x00, 0x00, 0x00);


// Byte dei dispositivi appresi --> sostituisce il byte di presenza delle vecchie versioni VAL_STATO_NULL8
__EEPROM_DATA(MSK_PRES_NULL, MSK_PRES_NULL, MSK_PRES_NULL, MSK_PRES_NULL, MSK_PRES_NULL, MSK_PRES_NULL, MSK_PRES_NULL, MSK_PRES_NULL);
__EEPROM_DATA(MSK_PRES_NULL, MSK_PRES_NULL, MSK_PRES_NULL, MSK_PRES_NULL, MSK_PRES_NULL, 0x00, 0x00, 0x00);
__EEPROM_DATA(BAT_SOGLIA_940_DEF, RSSI_SOGLIA_940_DEF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);

 */

// dichiarazione Variabili 

// stato modalit� RF
BYTE RF_Mode = RF_STANDBY;

// composizione messaggio GT
// 2 byte contatore messaggio
// 2 byte numero totale messaggio
// 8 byte di dato
//BYTE GTTxRxBuffer[MAX_TXRX_BUFF];
BYTE EE_trasponder[MAX_EEPR_TRASPONDER];
BYTE EE_msk_presenza_trasp[EE_NUM_MSK_PRES];
BYTE EE_site_code[EE_SITE_CODE_SIZE];
BYTE NEW_site_code[EE_SITE_CODE_SIZE];
BYTE batteria_trasponder;
BYTE RSSI_trasponder;
BYTE counter_prossimita[MAX_STATO_TRASPONDER];
BYTE NEW_info_device[MAX_EEPR_SN_TRASP + 1]; // Serial Number + ID

// Valori di soglia in EEPROM
BYTE EE_soglia_batteria;
BYTE EE_soglia_rssi;
#ifdef CONFIGURA_940
// Valore configurazione in eeprom
BYTE EE_940_config=0;
#endif

BYTE pointer_trasponder;
BYTE ctr_sirene_autoapp;
BOOL flg_trasponder_found;
WORD ctr_trasponder_found;
BYTE id_trasponder;

BYTE val_sirena_id;
BYTE val_sirena_sn_hh;
BYTE val_sirena_sn_hl;
BYTE val_sirena_sn_lh;
BYTE val_sirena_sn_ll;
BYTE val_sirena_stato;
BYTE val_sirena_rssi;
BYTE val_sirena_batteria;

BYTE val_last_autop_sirena_id;
BYTE val_last_autop_sirena_sn_hh;
BYTE val_last_autop_sirena_sn_hl;
BYTE val_last_autop_sirena_sn_lh;
BYTE val_last_autop_sirena_sn_ll;

BYTE Status_Rx_cmd_0;
BYTE Status_Rx_cmd_1;
BYTE Status_Rx_cmd_2;
BYTE Status_Rx_cmd_3;

BYTE Payload_Rx_0;
BYTE Payload_Rx_1;
BYTE Payload_Rx_2;
BYTE Payload_Rx_3;

BYTE Status_Tx_cmd_0;
BYTE Status_Tx_cmd_1;
BYTE Status_Tx_cmd_2;
BYTE Status_Tx_cmd_3;

BYTE stato_pic_low;
BYTE stato_pic_High;

BYTE fase_centralina;
BYTE operation_post_init;
BOOL flg_buffer_full; // segnalazione buffer autoapprendimento pieno

BYTE ctr_tx_autoapprendimento;
BYTE ctr_id_autoapp;
BYTE val_id_autoapp;
BYTE ctr_id_ricerca;
BYTE val_id_ricerca;
BYTE ctr_next_sirena;
BYTE val_id_ultimo_trovato = 0; // id dell'ultimo dispositivo trovato con la ricerca --> dove iniziare
BYTE val_id_ultimo_trovato_last = 0;
BYTE val_id_ultimo_trovato_verifica = 0;      //V24.33
BYTE valIdBroadFromI2c = 0;     //V24.32

// Gestione verifica presenza
WORD ctr_interrogazioni = 0; // contatore delle interrogazioni a buon fine consecutive
WORD timeout_verifica_presenza = 0; // timoeut in secondi per eseguire la verifica della presenza
BYTE debounce_verifica_presenza = 0; // debounce per gestione antirapina con verifica presenza
BYTE cnt_deb_verifica_presenza = 0; // contatore per gestire funzione antirapina
BOOL antirapinaNotify = FALSE;
BOOL send100_2 = FALSE;
// Gestione prossimita
BYTE id_prossimita = 0;
BYTE tau_polling_prossimita = 0;
BYTE distanza_prossimita = 0; // per ora il RSSI
WORD timeout_prossimita = 0;
WORD cnt_timeout_prossimita = 0;
BYTE debounce_prossimita = 0;
BYTE cnt_debounce_prossimita = 0;

BYTE ctr_timer_ext;
WORD timer_sistema;
WORD ctr_timer_1s;

BOOL GTRXTimeExt_expired;
BOOL GTRXTimeoutExt_flag;
WORD GTRXTimeoutExt;

BOOL GTRXInibitTime_expired;
BOOL GTRXInibitTimeout_flag;
WORD GTRXInibitTimeout;

BOOL GTRXTime_expired;
BOOL GTRXTimeout_flag;
WORD GTRXTimeout;

BOOL GTTime2_expired;
BOOL GTTimeout2_flag;
WORD GTTimeout2;

// Gestice i tempi di risposta del KeepAlive I2C
BOOL GTKATimeout_flag;
WORD GTKATimeout;
BOOL GTKASendNotify;

BYTE TauSleepI2C; // timeout di sleep dopo ultima operazione I2C

// Attiva la funzione verifica presenza
BOOL flg_reqVerPresenza;
WORD cnt_pollingVerPres;
WORD tau_pollingVerPres;
// Attiva la funzione Prossimit� IN 
BOOL flg_reqProssIn;
BYTE cnt_pollingProssIn;
BYTE tau_pollingProssIn;
// Attiva la funzione Prossimit� OUT
BOOL flg_reqProssOut;
BYTE cnt_pollingProssOut;
BYTE tau_pollingProssOut;
// gestione interrupt WMP100 da sveglio
BYTE ctr_deb_wmp100;

//receive flags
BOOL hasPacket = FALSE; //indicates that data packet is available in RX buffer
BYTE RSSIRegVal = 0; //For reading RSSI

//uninitialized variable - MRF89XA variables
WORD Rate_C; //Data Rate
BYTE FREQ_BW; //Frequenncy bandwidth - Receiver
BYTE FREQ_BAND; //Frequency band - (902-915 / 915-928 / 950-960 or 863-870)
BYTE RVALUE; //Register R value
BYTE PVALUE; //Register P value
BYTE SVALUE; //Register S value
BYTE TX_FSK; //Frequency deviation register setting
BYTE PFILTER_SETTING; //SIDEBAND FILTER SETTING


//external variables
extern char InitConfigRegs[]; //Initial Configuration setting values

//used as extern in other files
BOOL IRQ1_Received = FALSE; //indicates whether IRQ0 has been received or not
BOOL IRQ0_Received = FALSE; //indicates whether IRQ0 has been received or not
BYTE TxPacket[PACKET_LEN]; //TX buffer
BYTE RxPacket[PACKET_LEN]; //RX buffer
BYTE RxPacketLen; //Packet length for Received packet

WORD GTMaxCount;
WORD GTTXcounter;
WORD GTRXcounter;
WORD GTMaxRXcounter;

BOOL flg_trasp_reg_batt_bassa; // flag stato batteria trasponder registrato
WORD val_batt;
BOOL CmdCancAll = FALSE;
BYTE CmdCancId = 0;
BYTE CmdAttDisId = 0;
BOOL flgNotAnomalia = FALSE;
BOOL InTest = FALSE;

BYTE cmd_telefono;
BYTE mskCmdRicerca;
DWORD mskPresProssimita[4];
DWORD msk_prossimita_out_multipla[4];
DWORD indice = 0;
DWORD mskT[4];
DWORD mskPres[4];
DWORD mskIn[4];
DWORD mskOut[4];
DWORD mskRSSIPros[4];

BYTE ctr_tx_autoapprendimento = 0;
BYTE flagReceivingAnyTrasmitter = 0;
BYTE flagTxByUserPress = 0;
#ifdef AUTOAPPRENDIMENTO_SMART
BOOL faseAutoApp1_HappyHour = FALSE;
WORD ctr_timer_10s = 0;	  // timer conteggio cicli autoapprendiomento
#endif
BYTE suspendTxMng = 0;
BYTE tmrEnableTxMng = 0;
BYTE flagRxCmdBroadI2c=0;
BYTE esistevaUnTrasponder=1;
BYTE ricBroaDaI2cInCorso=0;


static BYTE loopIdx = 0;
static BYTE loopMsk = 0;

//Function prototypes
void MRF89XAInit(void);
void Setup(void);
void get_nr_sirene_autoapp(void);
void SetLoopMsk(void);

extern void reset_mskT (DWORD* pMsk);

void SetLoopMsk(void) {
    loopMsk = loopIdx / 32;
}

static void dWordMemCpy(DWORD* w, const DWORD* cW, BYTE size)
{
	BYTE i;
	for (i = 0; i < size; i++)
	{
		*w = *cW;
		w++;
		cW++;
	}
}


/*********************************************************************
 * void Read_EE_param(void)
 *
 * Overview:        
 *              This functions read eeprom parameter
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 ********************************************************************/
void Read_EE_param(BOOL reinit) {
    WORD i = 0, j = 0;
    uint8_t eeprom_read_data = 0;

    /* Controllo se � stata fatta inizializzazione celle eeprom */
    eeprom_read_data = eeprom_read(EE_EEPROM_INIT_VALID_ADD);
    if ((reinit) || (eeprom_read_data != EE_EEPROM_INIT_VALID)) {
#if(DBG_JUMPER_EEPROM)

        JUMPER_SetHigh();
#endif 
        for (i = 0, j = 1; j < MAX_EEPR_NR_TRASPONDER; j++) {
            eeprom_write(EE_ADD_TRASPONDER + i, j);
            i++;
            eeprom_write(EE_ADD_TRASPONDER + i, VAL_STATO_NULL);
            i++;
        }
        for (i = 0; i < EE_NUM_MSK_PRES; i++) {
            eeprom_write(EE_ADD_MSK_STATO + i, MSK_PRES_NULL);
        }
        eeprom_write(EE_ADD_SOGLIA_BATT, BATT_SOGLIA_940_DEF);
        eeprom_write(EE_ADD_SOGLIA_RSSI, RSSI_SOGLIA_940_DEF);
        eeprom_write(EE_EEPROM_INIT_VALID_ADD, EE_EEPROM_INIT_VALID);
#if(DBG_JUMPER_EEPROM)
        JUMPER_SetLow();
#endif
    }

    /* Controllo se in eeprom � presente un site code valido */
    eeprom_read_data = eeprom_read(EE_SITE_CODE_VALID_ADD);
    if ((reinit) || (eeprom_read_data != EE_SITE_CODE_VALID)) {
#if(DBG_JUMPER_EEPROM)
        JUMPER_SetHigh();
#endif
        uint16_t corcon_reg = CORCON;
        uint16_t psvpag_reg = PSVPAG;
        if (!CORCONbits.PSV) {
            CORCONbits.PSV = 1;
        }
        corcon_reg = CORCON;
        if (psvpag_reg) {
            PSVPAG = 0;
        }
        uint16_t flash_read_data = 0, *data_ptr = 0;
        data_ptr = (uint16_t*) (0x8000 + PM_SITE_CODE_ADD);
        flash_read_data = *data_ptr;

        eeprom_write(EE_SITE_CODE_ADD + EE_SITE_CODE_LL, (uint8_t) (flash_read_data));
        eeprom_write(EE_SITE_CODE_ADD + EE_SITE_CODE_LH, (uint8_t) (flash_read_data >> 8));
        eeprom_write(EE_SITE_CODE_ADD + EE_SITE_CODE_HL, 0x00);
        eeprom_write(EE_SITE_CODE_ADD + EE_SITE_CODE_HH, 0x00);
        eeprom_write(EE_SITE_CODE_VALID_ADD, EE_SITE_CODE_VALID);
#if(DBG_JUMPER_EEPROM)

        JUMPER_SetLow();
#endif
    }

    /* Leggo valori da eeprom e inizializzo variabili */
    for (i = 0; i < MAX_EEPR_TRASPONDER; i++) {
        EE_trasponder[i] = eeprom_read(EE_ADD_TRASPONDER + i); //load trasponder parameters from EEPROM
    }
    for (i = 0; i < EE_NUM_MSK_PRES; i++) {
        EE_msk_presenza_trasp[i] = eeprom_read(EE_ADD_MSK_STATO + i);
    }
    for (i = 0; i < MAX_STATO_TRASPONDER; i++) {
        counter_prossimita[i] = 0;
    }
    EE_soglia_batteria = eeprom_read(EE_ADD_SOGLIA_BATT);
    EE_soglia_rssi = eeprom_read(EE_ADD_SOGLIA_RSSI);
  #ifdef CONFIGURA_940
	EE_940_config = eeprom_read(EE_940_CONFIG);
  #endif


    EE_site_code[EE_SITE_CODE_LL] = eeprom_read(EE_SITE_CODE_ADD + EE_SITE_CODE_LL);
    EE_site_code[EE_SITE_CODE_LH] = eeprom_read(EE_SITE_CODE_ADD + EE_SITE_CODE_LH);
    EE_site_code[EE_SITE_CODE_HL] = eeprom_read(EE_SITE_CODE_ADD + EE_SITE_CODE_HL);
    EE_site_code[EE_SITE_CODE_HH] = eeprom_read(EE_SITE_CODE_ADD + EE_SITE_CODE_HH);
    for (i = 0; i < EE_SITE_CODE_ADD; i++) {
        NEW_site_code[i] = 0x00;
    }
}

/*********************************************************************
 * void MRF89XAInit(void)
 *
 * Overview:        
 *              This functions initialize the MRF89XA transceiver
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 ********************************************************************/
void setRegMcParam0(BYTE x)
{
	RegisterSet(REG_MCPARAM0, x);
}


void MRF89XAInit(void) {
    BYTE input, i = 0;
    while (MRF_RESET_GetValue()) {
    }
    __delay_ms(10); // Delay dopo lettura filo reset MRF
    //    EX_INT1_InterruptDisable();
    //    EX_INT2_InterruptDisable();
    // configuring the MRF89XA radio
    for (i = 0; i <= 31; i++) {
        RegisterRead(i);
    }
    //Configure MRF89XA Initialization parameters

    // inizializzazione registri 
    for (i = 0; i <= 31; i++) {
        RegisterSet(i, InitConfigRegs[i]);
    }

    RF_Mode = RF_STANDBY;
    input = RegisterRead(REG_MCPARAM0);
	setRegMcParam0((input & 0x1F) | RF_SYNTHESIZER);
    RF_Mode = RF_SYNTHESIZER;

    /* clear PLL_LOCK flag so we can see it restore on the new frequency */\
	input = RegisterRead(REG_IRQPARAM1);
    RegisterSet(REG_IRQPARAM1, (input | 0x02));

    input = RegisterRead(REG_MCPARAM0);
	setRegMcParam0(((input & 0xE7) | FREQBAND_950));

    //Program R, P,S registers
    RegisterSet(REG_R1, 125); // modifica per set 868 Mhz
    RegisterSet(REG_P1, 100); // modifica per set 868 Mhz
    RegisterSet(REG_S1, 20); // modifica per set 868 Mhz

    input = RegisterRead(REG_MCPARAM0);
	setRegMcParam0(((input & 0x1F) | RF_SYNTHESIZER));

	RF_Mode = RF_SYNTHESIZER;

    /* clear PLL_LOCK flag so we can see it restore on the new frequency */
    input = RegisterRead(REG_IRQPARAM1);
    RegisterSet(REG_IRQPARAM1, (input | 0x02));

    SetRFMode(RF_STANDBY);

    //    EX_INT1_InterruptEnable();
    //    EX_INT2_InterruptEnable();
}

/*********************************************************************
 * void Setup(void)
 *
 * Overview:        
 *              This functions initialize the MRF89XA transceiver
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 ********************************************************************/

void setNewValForSyncCode(void)
{
	RegisterSet(REG_SYNCBYTE0, EE_site_code[EE_SITE_CODE_HH]);
	RegisterSet(REG_SYNCBYTE1, EE_site_code[EE_SITE_CODE_HL]);
	RegisterSet(REG_SYNCBYTE2, EE_site_code[EE_SITE_CODE_LH]);
	RegisterSet(REG_SYNCBYTE3, EE_site_code[EE_SITE_CODE_LL]);
}


void Setup(void) {

    BYTE input;
    SetRFMode(RF_STANDBY);

    //Program FSK Modulation parameters
    input = RegisterRead(REG_MCPARAM1);
    input = ((input & 0x3F) | MODSEL_FSK);
    RegisterSet(REG_MCPARAM1, input);

#ifdef	ENABLE_SERIALE
    c(" 4. FrqBand=863-870 MHz, Center Frq = 868 MHz\r\n");
#endif
    FREQ_BAND = FREQBAND_950;
    RVALUE = 125;
    PVALUE = 100;
    SVALUE = 20;

	setRegMcParam0(((InitConfigRegs[REG_MCPARAM0] & 0xE7) | FREQ_BAND));

    if (FREQ_BAND == FREQBAND_950) {
        BYTE readback = RegisterRead(REG_MCPARAM0);
        readback = (readback & 0xF8);
		setRegMcParam0(readback);
    }
    RegisterSet(REG_R1, RVALUE);
    RegisterSet(REG_P1, PVALUE);
    RegisterSet(REG_S1, SVALUE);

#ifdef	ENABLE_SERIALE
    c(" 6. DataRate = 50 kbps, BW = 175 KHz, Frequency Deviation = 100 KHz\r\n");
#endif

#ifdef RATE50
    // set parametri per funzionamento a 50Kbits
    Rate_C = BITRATE_50;
    FREQ_BW = RXFC_FOPLUS175;
    TX_FSK = FREGDEV_100;
    PFILTER_SETTING = PASSIVEFILT_514;
#endif

#ifdef RATE100
    // set parametri per funzionamento a 100Kbits
    Rate_C = BITRATE_100;
    FREQ_BW = RXFC_FOPLUS400;
    TX_FSK = FREGDEV_200;
    PFILTER_SETTING = PASSIVEFILT_987;
#endif

#ifdef RATE200
    // set parametri per funzionamento a 100Kbits
    Rate_C = BITRATE_200;
    FREQ_BW = RXFC_FOPLUS400;
    TX_FSK = FREGDEV_200;
    PFILTER_SETTING = PASSIVEFILT_987;
#endif

    RegisterSet(REG_BITRATE, Rate_C);
    RegisterSet(REG_RXPARAM0, (FREQ_BW | PFILTER_SETTING));
    RegisterSet(REG_FREGDEV, TX_FSK);

    //Perform frequency synthesization
    input = RegisterRead(REG_MCPARAM0);
	setRegMcParam0((input & 0x1F) | RF_SYNTHESIZER);
    RF_Mode = RF_SYNTHESIZER;

    //clear PLL_LOCK flag so we can see it restore on the new frequency 
    input = RegisterRead(REG_IRQPARAM1);
    RegisterSet(REG_IRQPARAM1, (input | 0x02));

    SetRFMode(RF_RECEIVER);
    //transceiver will be set to FSK or OOK mode
    //Include delay and put the chip back to standby mode
    __delay_ms(100);
    SetRFMode(RF_STANDBY);

    // set nuovo valore per sync word
	setNewValForSyncCode();
}

/*void setTrmKeepAlive(void)
{
    // Imposto il timer per gestire il ritardo della segnalazione
    // keep alive
    GTKATimeout = DEF_100MS_BS_2MS;
    GTKATimeout_flag = TRUE;
    GTKASendNotify = FALSE;
}*/

/***************************************************************
 *	bool verifica_ee_presenza_trasponder(byte id)
 *		Verifica se � presente il trasponder nella configurazione
 ****************************************************************/
BOOL verifica_ee_presenza_trasponder(BYTE id) {
    BOOL ret = FALSE;
    BYTE idMsk = 0, idTr = 0;
    DWORD index = 0;

    // Otteniamo l'id della maschera
    idMsk = ((id - 1) / EE_BIT_SIZE_MSK_PRES);
    // Otteniamo l'indice del trasponder nella maschera
    idTr = ((id - 1) % EE_BIT_SIZE_MSK_PRES);
    idTr++;

    if (idTr == 1)
        index = 0x0001;
    else if (idTr == 2)
        index = 0x0002;
    else {
        BYTE i;

        index = 0x0002;
        for (i = 1; i < (idTr - 1); i++)
            index *= 2;
    }
    ret = (EE_msk_presenza_trasp[idMsk] & index) ? TRUE : FALSE;

    return ret;
}

/***************************************************************
 *	void set_ee_presenza_trasponder(BYTE id, BOOL attiva)
 *		Attiva o disattiva uno ID per un trasponder attivo. E' per
 *	sostituire il byte di presenza in EEPROM per ogni trasponder
 ****************************************************************/
void set_ee_presenza_trasponder(BYTE id, BOOL attiva) {
    BYTE idMsk = 0, idTr = 0;
    DWORD index = 0;
    // Otteniamo l'id della maschera
    idMsk = ((id - 1) / EE_BIT_SIZE_MSK_PRES);
    // Otteniamo l'indice del trasponder nella maschera
    idTr = ((id - 1) % EE_BIT_SIZE_MSK_PRES);
    idTr++;

    if (idTr == 1)
        index = 0x0001;
    else if (idTr == 2)
        index = 0x0002;
    else {
        BYTE i;

        index = 0x0002;
        for (i = 1; i < (idTr - 1); i++)
            index *= 2;
    }

    if (attiva)
        EE_msk_presenza_trasp[idMsk] |= index;
    else
        EE_msk_presenza_trasp[idMsk] &= ~index;

    eeprom_write(EE_ADD_MSK_STATO + idMsk, EE_msk_presenza_trasp[idMsk]);
}

void get_next_sirena(void) {
    unsigned char i;

    val_sirena_id = 0;
    for (i = ctr_next_sirena; i < MAX_EEPR_NR_TRASPONDER; i++) {
        pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);
        //if (EE_trasponder[pointer_trasponder + EE_PRESENZA_ID] != TRASP_PRESENTE)
        if (!verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID])) {
            // trasponder non presente
            // continuo con il ciclo
            continue;
        }
        // trasponder presente
        val_sirena_id = EE_trasponder[pointer_trasponder + EE_ID];
        val_sirena_stato = EE_trasponder[pointer_trasponder + EE_STATO];
        ctr_next_sirena = i;
        break;
    }
}

void get_sirena(BYTE id) {
    static unsigned char idx=0;
    idx = id - 1; // rispetto a quello inviato su i2c decrementarlo
    val_sirena_id = 0;

    pointer_trasponder = (idx * MAX_EEPR_DATA_TRASPONDER);
    if ((EE_trasponder[pointer_trasponder + EE_ID] == id) &&
            verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID])) {
        // trasponder presente
        val_sirena_id = EE_trasponder[pointer_trasponder + EE_ID];
        val_sirena_stato = EE_trasponder[pointer_trasponder + EE_STATO];
        val_sirena_rssi = RSSI_trasponder;
        val_sirena_batteria = batteria_trasponder;
    }
}

/***************************************************************
*	Maschere presenza dei trasponder appresi
****************************************************************/

void get_mask(BYTE type, BYTE* idDriver)
{
	//BYTE dimMsk = 32;		// bit di ogni maschera, sono 13byte, quindi 4 maschere
	BYTE i = 0;
    esistevaUnTrasponder = 0;

	indice = 0x0001;

	reset_mskT(mskT);

	for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++)
	{
		pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);

		if (verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID]) &&
			((type == PRESENZA) ||
			(type == TEST && (EE_trasponder[pointer_trasponder + EE_STATO] & TRASP_TESTATO)) ||
				(type == ANOMALIA && (EE_trasponder[pointer_trasponder + EE_STATO] & (TRASP_ANOM_BAT | TRASP_ANOM_RSSI | TRASP_ANOM_BATT_940))) ||
				(type == BROADCAST && (EE_trasponder[pointer_trasponder + EE_STATO] & TRASP_VERIFICATO))))
		{
			mskT[i / 32] |= indice;

            esistevaUnTrasponder = 1;

			if (type == BROADCAST && (EE_trasponder[pointer_trasponder + EE_STATO] & TRASP_ATTIVO_GUIDA))
				* idDriver = EE_trasponder[pointer_trasponder + EE_ID];
		}

		if (indice & 0x80000000)
			indice = 0x0001;  // si resetta l'indice per il cambio di maschera
		else
			indice *= 2;
	}
}


void delete_sirena(BYTE id) {
    unsigned char idx = id - 1; // decrementiamo rispetto all'indice i2c
    // cancelliamo dalla EEPROM
    pointer_trasponder = idx * MAX_EEPR_DATA_TRASPONDER;
    if (verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID])) {
        eeprom_write(pointer_trasponder + EE_ID, id);
        eeprom_write(pointer_trasponder + EE_STATO, VAL_STATO_NULL);
        set_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID], FALSE);
        // cancelliamo dalla RAM
        EE_trasponder[pointer_trasponder + EE_ID] = id;
        EE_trasponder[pointer_trasponder + EE_STATO] = VAL_STATO_NULL;
    }
    // lettura  numero di sirene autoapprese
    get_nr_sirene_autoapp();
}

void att_diss_sirena(BYTE id) {
    unsigned char idx = (id & CONFIG_MASK_BIT_ID_TRASP) - 1; //7 bit per ID trasponder
    BYTE ee_val = 0;

    pointer_trasponder = idx * MAX_EEPR_DATA_TRASPONDER;

    if (id & 0x80) {
        // E' da disattivare
        EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRAS_ATTIVO;
    } else {
        // E' da attivare
        EE_trasponder[pointer_trasponder + EE_STATO] |= TRAS_ATTIVO;
    }

    ee_val = EE_trasponder[pointer_trasponder + EE_STATO] & (TRAS_ATTIVO | TRASP_ANOM_BAT);
    eeprom_write(pointer_trasponder + EE_STATO, ee_val);
}

void get_next_id_ricerca(BOOL checkAtt) {
    unsigned char i;

    val_id_ricerca = 0;
    for (i = ctr_id_ricerca; i < MAX_EEPR_NR_TRASPONDER; i++) {
        pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);
        if (!verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID])
                || (checkAtt && !(EE_trasponder[pointer_trasponder + EE_STATO] & TRAS_ATTIVO))) {
            // trasponder non presente
            // continuo con il ciclo
            continue;
        }
        // trasponder presente
        val_id_ricerca = EE_trasponder[pointer_trasponder + EE_ID];
        ctr_id_ricerca = i;
        break;
    }
}

void get_nr_sirene_autoapp(void) {
    unsigned char ctr_tmp_sirene, i;

    ctr_tmp_sirene = 0;

    for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++) {
        pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);
        if (!verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID])) {
            // trasponder non presente
            // continuo con il ciclo
            continue;
        }
        // incrementa il contatore numero sirene autoapprese
        ctr_tmp_sirene++;
    }
    ctr_sirene_autoapp = ctr_tmp_sirene;
}

// ritorna l'ID massimo del trasponder appreso e il massimo id di quello attivo

void get_max_id_trasponder_trasponder(BYTE * appreso) {
    BYTE i;

    for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++) {
        pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);

        // trasponder non presente
        // continuo con il ciclo
        if (verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID]))
            *appreso = EE_trasponder[pointer_trasponder + EE_ID];
    }
}

void get_next_id_autoap(void) {
    unsigned char i;

    val_id_autoapp = 0;
    for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++) {
        pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);
        if (verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID])) {
            // trasponder presente
            // continuo con il ciclo
            continue;
        }
        // trasponder non presente
        val_id_autoapp = EE_trasponder[pointer_trasponder + EE_ID];
        ctr_id_autoapp = i;
        break;
    }
}

void ferma_verifica_presenza(void) {
    flg_reqVerPresenza = FALSE;
    cnt_pollingVerPres = 0;
    tau_pollingVerPres = 0;
    stato_pic_low &= ~STATO_PIC_IN_VERIFICA;
}

void setTrasponderValBat(BYTE batt, BYTE id, BYTE stato) {
    BYTE ee_val = 0;
    BOOL updateStat = FALSE;

	//if (Status_Rx_cmd_1 == VALUE_TX_BY_USER_PRESS)
	//	return;

    val_batt = batt;
    batteria_trasponder = val_batt;

    pointer_trasponder = ((id - 1) * MAX_EEPR_DATA_TRASPONDER);
    // Controlliamo se il 985 ha segnalato anomalia batteria
    if (stato & MASK_BIT_BATT_STATUS) {
        if (!(EE_trasponder[pointer_trasponder + EE_STATO] & TRASP_ANOM_BAT)) {
            EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_ANOM_BAT;
            ee_val |= TRASP_ANOM_BAT;
        }
    } else if (EE_trasponder[pointer_trasponder + EE_STATO] & TRASP_ANOM_BAT) {
        //Indica che il trasponder non ha pi� l'anomalia batteria
        EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_ANOM_BAT;
        updateStat = TRUE;
    }

    // Verifichiamo che il valore non sia sotto la soglia batteria del 940
    if (val_batt < EE_soglia_batteria) {
        if (!(EE_trasponder[pointer_trasponder + EE_STATO] & TRASP_ANOM_BATT_940)) {
            EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_ANOM_BATT_940;
            ee_val |= TRASP_ANOM_BATT_940;
        }
    } else if (EE_trasponder[pointer_trasponder + EE_STATO] & TRASP_ANOM_BATT_940) {
        EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_ANOM_BATT_940;
        updateStat = TRUE;
    }

    // Se in test si verifica anche il valore RSSI
    if (InTest && (RSSI_trasponder < EE_soglia_rssi)) {
        if (!(EE_trasponder[pointer_trasponder + EE_STATO] & TRASP_ANOM_RSSI)) {
            EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_ANOM_RSSI;
            ee_val |= TRASP_ANOM_RSSI;
        }
    }

    if (ee_val | updateStat) {
        //AddNotifyToTel(I2CEVT_ANOMALIA_TRASP, I2CEVT_ANOMALIA_TRASP_BATT);
        ee_val &= ~TRASP_ANOM_RSSI;
        if (EE_trasponder[pointer_trasponder + EE_STATO] & TRAS_ATTIVO) ee_val |= TRAS_ATTIVO;
        eeprom_write(pointer_trasponder + EE_STATO, ee_val);
        flgNotAnomalia = TRUE;
    }
}

// Routine di lettura dati ricevuti da RF

void Read_data_Rx_Rf(BOOL stop) {

    // estrazione contatore pacchetto RX							
    GTRXcounter = (WORD) RxPacket[COUNTER_HIGH];
    GTRXcounter = ((GTRXcounter << 8) & 0xFF00);
    GTRXcounter = GTRXcounter + (WORD) RxPacket[COUNTER_LOW];

    // estrazione contatore numero pacchetti RX							
    GTMaxRXcounter = (WORD) RxPacket[MAX_COUNTER_HIGH];
    GTMaxRXcounter = ((GTMaxRXcounter << 8) & 0xFF00);
    GTMaxRXcounter = GTMaxRXcounter + (WORD) RxPacket[MAX_COUNTER_LOW];

    // estrazione stato comani
    Status_Rx_cmd_0 = RxPacket[STATUS_CMD_0];
    Status_Rx_cmd_1 = RxPacket[STATUS_CMD_1];
    Status_Rx_cmd_2 = RxPacket[STATUS_CMD_2];
    Status_Rx_cmd_3 = RxPacket[STATUS_CMD_3];

    // estrazione rolling code GT
    Payload_Rx_0 = RxPacket[ROLL_CODE_0];
    Payload_Rx_1 = RxPacket[ROLL_CODE_1];
    Payload_Rx_2 = RxPacket[ROLL_CODE_2];
    Payload_Rx_3 = RxPacket[ROLL_CODE_3];

    // stop Timeout
    if (stop) {
        GTRXTimeout_flag = FALSE;
        GTRXTimeout = 0;
        GTRXTime_expired = TRUE;
    }
}

void Prepare_TxRx_Packet_step1(void) {
    TxPacket[STATUS_CMD_0] = Status_Tx_cmd_0;
    TxPacket[STATUS_CMD_1] = Status_Tx_cmd_1;
    TxPacket[STATUS_CMD_2] = Status_Tx_cmd_2;
    TxPacket[STATUS_CMD_3] = Status_Tx_cmd_3;

    // set payload
    // VERIFICARE : implementare GT Rolling code
	TxPacket[ROLL_CODE_0] = EE_site_code[EE_SITE_CODE_HH];
	TxPacket[ROLL_CODE_1] = EE_site_code[EE_SITE_CODE_HL];
	TxPacket[ROLL_CODE_2] = EE_site_code[EE_SITE_CODE_LH];
	TxPacket[ROLL_CODE_3] = EE_site_code[EE_SITE_CODE_LL];


    // set contatore trasmissione 
    TxPacket[TX_COUNTER_HIGH] = 0;
    TxPacket[TX_COUNTER_LOW] = 0;

    // Preparazione flag per invio 
    Prepare_Send_Packet();

}

void Prepare_TxRx_Packet_step2(void) {
    // set flag pacchetto riconosciuto
    hasPacket = FALSE;
    // after transmission it put on receive mode
    SetRFMode(RF_RECEIVER);

    GTRXcounter = 0;
    GTMaxRXcounter = 0;

    // set attesa 100 ms
    GTRXTimeout = DEF_100MS_BS_2MS;
    GTRXTime_expired = FALSE;
    GTRXTimeout_flag = TRUE;

    while (GTRXTime_expired == FALSE) {
        //if (IRQ1_Received) {
        if (IO_RB1_GetValue()) {
            //RSSIRegVal = (RegisterRead(REG_RSSIVALUE) >> 1);
            IRQ1_Received = TRUE;
            ReceiveFrame();
            SetRFMode(RF_STANDBY);

            if (hasPacket) {
                // pacchetto ricevuto
                hasPacket = FALSE;
                // lettura dati da rf
                Read_data_Rx_Rf(TRUE);
            }
        }
    }
    // Set TF into stand by mode
    SetRFMode(RF_STANDBY);
}

/***************************************************************************/
/* void Prepare_TxRx_Packet_step2_broadcast(BYTE id)
 ****************************************************************************
 *	Funzione per gestire ricezione messaggi broadcast
 * Se id > 0 indica che alla prima ricezione di pacchetto
 *	deve essere interrrotta la procedura
 * Se id == 0 indica che bisogna aspettare tutte le possibili
 *	risposte dei trasponder attivi											*/
/***************************************************************************/
BYTE numAppresi = 0;

void Prepare_TxRx_Packet_step2_broadcast(BYTE id, BYTE mskCmd) {
    //BYTE numAppresi = 0;
    static BYTE idMsk, i, j, batt;

	reset_mskT(mskPres);
    hasPacket = FALSE; // set flag pacchetto riconosciuto
    SetRFMode(RF_RECEIVER); // after transmission it put on receive mode
#if(DBG_JUMPER_BROADCAST)
    JUMPER_SetHigh();
#endif

    GTRXcounter = 0;
    GTMaxRXcounter = 0;

    // set attesa in base al numero di trasponder attivi
    //get_max_id_trasponder_trasponder(&numAppresi);
    GTRXTimeout = (numAppresi * DEF_100MS_BS_2MS) + DEF_100MS_BS_2MS;
    GTRXTime_expired = FALSE;
    GTRXTimeout_flag = TRUE;

	if (flagReceivingAnyTrasmitter)
        GTRXTimeout = DEF_262MS_BS_2MS;

    while (GTRXTime_expired == FALSE) {
        //if (IRQ1_Received) {
        if (IO_RB1_GetValue()) {
            // RSSIRegVal = (RegisterRead(REG_RSSIVALUE) >> 1);
            ReceiveFrame();
            SetRFMode(RF_STANDBY);
#if(DBG_JUMPER_BROADCAST)
            JUMPER_SetLow();
#endif
#ifdef OUT_DEBUG_RX_ACTIVE		
            OUT_DEBUG_RX = 1;
#endif
            if (hasPacket) {
                BYTE idxRx;

                hasPacket = FALSE; // pacchetto ricevuto
                Read_data_Rx_Rf(FALSE); // lettura dati da rf
                idxRx = (Status_Rx_cmd_2 & CONFIG_MASK_BIT_ID_TRASP);
                pointer_trasponder = ((idxRx - 1) * MAX_EEPR_DATA_TRASPONDER);

                // Verifichiamo che abbia risposto ad un ricerca broadcast
                if ((Status_Rx_cmd_0 & MASK_BIT_RICERCA_BROADCAST) && idxRx)
                {
                    EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_VERIFICATO;
                    RSSI_trasponder = RSSIRegVal;

                    batt = Status_Rx_cmd_3;
                    if(Status_Rx_cmd_1 == VALUE_TX_BY_USER_PRESS)
                    {
                        flagTxByUserPress = 1;
                        batt = Payload_Rx_3;
                    }
                    setTrasponderValBat(batt, idxRx, Status_Rx_cmd_0); // controllo batteria

                    // bisogna interrompere la ricerca
                    // Controllo RICERCA A --> interrompersi sul primo trasponder attivo che risponde
                    if (id && !(mskCmd & CMDW_PIC_BIT_RICERCA_C)
                            && (EE_trasponder[pointer_trasponder + EE_STATO] & TRAS_ATTIVO)) {
                        val_id_ultimo_trovato = val_id_ultimo_trovato_last = idxRx;
                        if (Status_Rx_cmd_2 & GT985_DRIVER_ABIL)
                            EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_ATTIVO_GUIDA;
                        break;
                    } else if (id && (mskCmd & CMDW_PIC_BIT_RICERCA_C)
                            && (EE_trasponder[pointer_trasponder + EE_STATO] & TRAS_ATTIVO)
                            && (Status_Rx_cmd_2 & GT985_DRIVER_ABIL)) {
                        // RICERCA TIPO C --> interromp�ersi sul primo trasponder attivo con stato DRIVER ON
                        val_id_ultimo_trovato = val_id_ultimo_trovato_last = idxRx;
                        EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_ATTIVO_GUIDA;
                        break;
                    } else {
                        if (!id)
                        {
                            // E' una ricerca di tipo B
                            // alziamo il bit nella maschera per indicare la risposta del trasponder
                            //BYTE idMsk, i, j;

                            idMsk = (idxRx - 1) / 32;
                            j = idxRx % 32;

                            indice = 0x0001;
                            for (i = 0; i < (j - 1); i++)
                                indice *= 2;

                            mskPres[idMsk] |= indice;

                            // Se � attivo il test mettiamo lo stato di trasponder testato
                            if (InTest)
                                EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_TESTATO;

                            // verifico se � attiva la funzione di prossimita OUT/IN
                            // con la gestione 99 trasponders bisogna eseguire il controllo
                            // ad ogni risposta, anzich� farlo a fine processo di ricerca
                            if ((fase_centralina == FASE_940_PROSSIMITA_OUT) || (fase_centralina == FASE_940_PROSSIMITA_IN)) {
                                if (RSSI_trasponder >= distanza_prossimita)
                                    mskRSSIPros[idMsk] |= indice;
                            }

                            // vediamo se attivare il driver 
                            if(EE_trasponder[pointer_trasponder + EE_STATO] & TRAS_ATTIVO)
                            {
                                if(Status_Rx_cmd_2 & GT985_DRIVER_ABIL)
                                {
                                    val_id_ultimo_trovato = val_id_ultimo_trovato_last = idxRx;
                                    EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_ATTIVO_GUIDA;
                                }
                            }
                        }

                        //Carichiamo il tempo di attesa
                        GTRXInibitTime_expired = FALSE;
                        GTRXInibitTimeout_flag = TRUE;
                        GTRXInibitTimeout = ((GT_MAX_PACKET_RX * GT_MS_TX_PACKET) / 2) + DEF_20MS_BS_2MS; // 50 ms

                        while (GTRXInibitTime_expired == FALSE) {
                            __delay_us(10);
                        }
                    }
                }
            }
            // riattiviamo la radio in ricezione
            SetRFMode(RF_RECEIVER);
#if(DBG_JUMPER_BROADCAST)
            JUMPER_SetHigh();
#endif
#ifdef OUT_DEBUG_RX_ACTIVE		
            OUT_DEBUG_RX = 0;
#endif
        }
    }
    // Set TF into stand by mode
    GTRXTimeout = 0;
    GTRXTime_expired = TRUE;
    GTRXTimeout_flag = FALSE;
    SetRFMode(RF_STANDBY);
#if(DBG_JUMPER_BROADCAST)
    JUMPER_SetLow();
#endif

	if (flagReceivingAnyTrasmitter)
		return;

    // Si calcola il tempo di inibit della radio
    if (id) {
        // Calcolo il tempo in base all'ID del trasponder che ha risposto e il max ID appreso
        // Se non ha risposto nessun trasponder dobbiamo non eseguire nessun pausa
        BYTE attesa;
        attesa = (val_id_ultimo_trovato) ? val_id_ultimo_trovato : numAppresi;
        GTRXInibitTimeout = ((numAppresi - attesa) * DEF_100MS_BS_2MS) + DEF_200MS_BS_2MS;
    } else {
        // ha aspettato che tutti i trasponder appresi possano rispondere
        GTRXInibitTimeout = DEF_100MS_BS_2MS;
    }
    GTRXInibitTimeout_flag = TRUE;
    GTRXInibitTime_expired = 0;
}

// Routine di invio comando di ricerca trasponder 

void Prepare_TxRx_Packet(void) {
#ifdef AUTOAPPRENDIMENTO_SMART
	if (!faseAutoApp1_HappyHour)
#endif
	{

		Prepare_TxRx_Packet_step1();

		while (GTTXcounter <= GTMaxCount) {
			TxPacket[COUNTER_HIGH] = (BYTE)((GTTXcounter >> 8) & 0x00FF);
			TxPacket[COUNTER_LOW] = (BYTE)(GTTXcounter & 0x00FF);
			GTTXcounter++;

			Send_Packet(MAX_TXRX_BUFF);

		}
	}
    Prepare_TxRx_Packet_step2();

}

// Routine di invio comando di ricerca broadcast trasponders
// Se ID == 0 indica di eseguire una ricerca broadcast di tipo B

void Prepare_TxRx_Packet_broadcast(BYTE id, BYTE mskCmd) {
	if (!flagReceivingAnyTrasmitter)
	{

		// l'invio rimane uguale alla solita ricerca
		Prepare_TxRx_Packet_step1();

		get_max_id_trasponder_trasponder(&numAppresi);
#ifdef OUT_DEBUG_RX_ACTIVE		
		OUT_DEBUG_RX = 1;
#endif
		while (GTTXcounter <= GTMaxCount) {
			TxPacket[COUNTER_HIGH] = (BYTE)((GTTXcounter >> 8) & 0x00FF);
			TxPacket[COUNTER_LOW] = (BYTE)(GTTXcounter & 0x00FF);
			GTTXcounter++;
			Send_Packet(MAX_TXRX_BUFF);
		}
#ifdef OUT_DEBUG_RX_ACTIVE		
		OUT_DEBUG_RX = 0;
#endif
	}
    Prepare_TxRx_Packet_step2_broadcast(id, mskCmd);

    if (flgNotAnomalia) {
        AddNotifyToTel(I2CEVT_ANOMALIA_TRASP, I2CEVT_ANOMALIA_TRASP_BATT);
        flgNotAnomalia = FALSE;
        ;
    }
}

static BOOL matchMsk(void)
{
	BYTE i;

	for (i = 0; i < 4; i++)
		if (mskT[i] & mskPres[i])
			return TRUE;

	return FALSE;
}

// Main Program

void main_init(BOOL reinit) {


    //call all the initialization routines
    //BoardInit();			//initialize the board

    Read_EE_param(reinit); // lettura parametri da eeprom

    // lettura  numero di sirene autoapprese
    get_nr_sirene_autoapp();

    // set contatore per sblocco di emergenza
    MRF89XAInit(); // initialize MRF89XA
    Setup(); // Configure the basic settings

    GTRXTime_expired = FALSE;
    GTRXTimeout_flag = FALSE;
    GTRXTimeout = 0;

    GTRXTimeExt_expired = FALSE;
    GTRXTimeoutExt_flag = FALSE;
    GTRXTimeoutExt = 0;

    GTRXInibitTime_expired = FALSE;
    GTRXInibitTimeout_flag = FALSE;
    GTRXInibitTimeout = 0;

    GTTime2_expired = FALSE;
    GTTimeout2_flag = FALSE;
    GTTimeout2 = 0;

    GTKATimeout_flag = FALSE;
    GTKATimeout = 0;
    GTKASendNotify = FALSE;

    TauSleepI2C = 0;

    flg_reqVerPresenza = FALSE;
    cnt_pollingVerPres = 0;
    tau_pollingVerPres = 0;

    flg_reqProssIn = FALSE;
    cnt_pollingProssIn = 0;
    tau_pollingProssIn = 0;

    flg_reqProssOut = FALSE;
    cnt_pollingProssOut = 0;
    tau_pollingProssOut = 0;

    ctr_deb_wmp100 = 0;

    ctr_timer_ext = 0;
    timer_sistema = 0;
    ctr_timer_1s = 0;

    // start Timer con step di 2 ms
    TMR1_Start(); // corrisponde a circa 2 ms

    stato_pic_low = 0;
    stato_pic_High = 0;
    cmd_telefono = 0;
    //cmd_telefono = CMDW_TEL_AUTOPP_ON;

    Status_Tx_cmd_0 = 0;
    Status_Tx_cmd_1 = 0;
    Status_Tx_cmd_2 = 0;
    Status_Tx_cmd_3 = 0;

    operation_post_init = 0;

    id_trasponder = 0;
    flg_trasp_reg_batt_bassa = 0;
    mskCmdRicerca = 0;

    // inizializzazione I2C
    //I2CSlaveConf();

    fase_centralina = FASE_940_RUN;
}


static void sendChangeAlarmNotify(BYTE byte1, BYTE notify)
{
	unsigned int alarmStateCount = 0;

	alarmStateCount = byte1;
	alarmStateCount <<= 8;
	alarmStateCount &= 0xFF00;
	alarmStateCount |= notify;

	AddNotifyToTel(I2CEVT_RICERCA, alarmStateCount);
}

static BOOL orMskPres (void)
{
    if(mskPres[0] || mskPres[1] || mskPres[2] || mskPres[3])
            return TRUE;
    return FALSE;
}

#ifdef AUTOAPPRENDIMENTO_SMART
void exitAutoapprendimento(void)
{
    setNewValForSyncCode();
	// comando di fine test
	fase_centralina = FASE_940_RUN;
	// reset stato autoapprendimento attivo
	stato_pic_low = stato_pic_low & STATO_PIC_AUTOAPP_NOT;
	//cmd_telefono = 0;
}
#endif


static BOOL txByUserPress (void)
{
    if(flagTxByUserPress)
    {
        flagTxByUserPress =0;
        
        fase_centralina = FASE_940_RUN;
        cmd_telefono = CMDW_TEL_ATT_VER_SLEEP;
        
        return TRUE;
    }
    return FALSE;
}


int main_loop(void) {
    static int i;
    static BOOL firstTime = TRUE;
	//static BYTE ctrForProximitySleep = 0;


	static BYTE testStaticCtr = 0;
	BYTE debug = 0;





    // loop continuo
    if (salvataggio_soglie) {
        salvataggio_soglie = false;
        eeprom_write(EE_ADD_SOGLIA_BATT, EE_soglia_batteria);
        eeprom_write(EE_ADD_SOGLIA_RSSI, EE_soglia_rssi);
    }
    // verifica se timeout giro chiave in corso
    // attesa comandi da parte del telefono

    switch (fase_centralina) {
        case FASE_940_POWER_UP:
            //            EX_INT1_InterruptFlagClear();
            //            EX_INT2_InterruptFlagClear();
            //            EX_INT1_InterruptDisable();
            //            EX_INT2_InterruptDisable();

         #ifdef SLEEP_LIKE_940I

			// enable I2C interrupt
			IFS1bits.SI2C1IF = 0;
			IEC1bits.SI2C1IE = 1;
			
			// enable +15/54 interrupt (IRQ2 su 940I) 
			//IN_15_54_GetValue();
			
			// IRQ! IRQ0 Radio non definiti come interrupt nella versione 940MT (lo erano nel 940I: extInt0, extInt1)

			

         #endif









            // Metto in SLEEP la radio
            SetRFMode(RF_SLEEP);
            __delay_us(100);

            operation_post_init = 0;
            TMR1_Stop();
            i2c_int_flag = false;



#if(DBG_JUMPER_SLEEP) 
            JUMPER_SetLow(); // Filo di DEBUG per monitorare stato sleep e reset
#endif

          #ifndef SLEEP_LIKE_940I
            if (stato_pic_low == STATO_PIC_SLEEP) {
                Sleep();
                asm("RESET");
            }
			else
         #endif
			{
                //   if (flg_reqVerPresenza || flg_reqProssIn || flg_reqProssOut)   
					// avremmo watchdog con durata 1 secondo
				    // ma WDT timer � fisso, settato inizialmente a 256ms , timer 1 secondo gestito con   ctrForProximitySleep
				
				
					RCONbits.SWDTEN = 1;

                  #ifdef SLEEP_LIKE_940I
					Sleep();
                  #else
					Idle();					//TBD   non sleep??
                  #endif
            }




			testStaticCtr++;
			debug = 0;
			debug = 0;
			debug = 0;


            //... in Sleep...

            // NOP per sicurezza dopo risevglio
            //__delay_ms(10);
            RCONbits.SWDTEN = 0;  //stop WatchDog
            RCONbits.SLEEP = 0; //Azzero flag sleep

            if (RCONbits.WDTO)
			{ //Se sono uscito per timeout WDT
                RCONbits.WDTO = 0; //Azzero flag WDT timeout
            }
            ClrWdt();

			flagReceivingAnyTrasmitter = 0;

            //CLKDIVbits.DOZE = 0;
            //__delay_ms(10);

#if(DBG_JUMPER_SLEEP) 
            JUMPER_SetHigh(); // Filo di DEBUG per monitorare stato sleep e reset
#endif 
            stato_pic_low = stato_pic_low & STATO_PIC_SLEEP_NOT;
            //			mettere interrupt da I2C
            if (i2c_int_flag) // I2C interrupt
                //if(TRUE)
            {
                i2c_int_flag = false;
                TauSleepI2C = DEF_3SEC_BS_100MS;
                //I2CRoutine();  --> essendoci sempre due tentativi se si viene svegliati il primo non lo si gestisce
                fase_centralina = FASE_940_INIT;
                operation_post_init = 0;
            }
			else
			{
                BYTE TxMng = 1;

				if (flg_reqVerPresenza)
				{
					if (++cnt_pollingVerPres >= tau_pollingVerPres*2)
					{
                        TxMng = 0;
    					// attiviamo verifica presenza
						cnt_pollingVerPres = 0;
						operation_post_init = 1;
						//GTKASendNotify = TRUE; // TODO Togliere
						fase_centralina = FASE_940_INIT;
					}
				}
				else if (flg_reqProssIn)
				{
					if (++cnt_pollingProssIn >= tau_pollingProssIn*2)
					{
                        TxMng = 0;
        				// attiviamo prossimaita IN
						cnt_pollingProssIn = 0;
						operation_post_init = 2;
						fase_centralina = FASE_940_INIT;
					}
				}
				else if (flg_reqProssOut)
				{
					if (++cnt_pollingProssOut >= tau_pollingProssOut*2)
					{
                        TxMng = 0;
						// attiviamo prossimaita OUT
						cnt_pollingProssOut = 0;
						operation_post_init = 3;
						fase_centralina = FASE_940_INIT;
					}
				}

                if( TxMng )			
				{
                  #ifdef CONFIGURA_940
                    if( (EE_940_config & MASK_IN_940CFG_USETX) && !suspendTxMng )
                  #else
                    if( !suspendTxMng )
                  #endif
                    {     
                        //stato_pic_High &= STATO_PIC_USE_TX_NOT;
                       /// ???  SetRFMode(RF_RECEIVER);  //s210919
                        //risvegliato da WDT e nessuna richiesta di verifica presenza / prossimit� in corso
                        flagReceivingAnyTrasmitter = 1;
                        ricBroaDaI2cInCorso = 0;
                        fase_centralina = FASE_940_INIT;  // oppure nuova fase di solo ascolto radio
                        break;
                    }
				}

                // Questo timeout � gestito sia col WDT che col timer quando � sveglio
                // Gestisce la timeout generale della funzione
                if (cnt_timeout_prossimita)
                    cnt_timeout_prossimita--;
            }

            break;

        case FASE_940_INIT:

            // Attivo interrupt parte radio
            TMR1_Start(); // corrisponde a circa 2 ms

            //            EX_INT1_InterruptEnable();
            //            EX_INT2_InterruptEnable();

			if (!flagReceivingAnyTrasmitter)
			{
				// Metto in STANDBY la radio
				SetRFMode(RF_STANDBY);
				__delay_us(10);
			}
			else
			{
				//SetRFMode(RF_RECEIVER);  210919
				fase_centralina = FASE_940_RICERCA_BROADCAST;
				break;
			}

            

            // Controlliamo se c'� qualche funzione attiva

            switch (operation_post_init) {
                case 1:
                    cmd_telefono = CMDW_TEL_VERIFICA_ON;
                    break;
                case 2:
                    cmd_telefono = CMDW_TEL_PROSS_IN_ON;
                    break;
                case 3:
                    cmd_telefono = CMDW_TEL_PROSS_OUT_ON;
                    break;
                default:
                    //cmd_telefono = 0; //Commentata perch� riesco a gestire il primo comando I2C
                    break;
            }
            /* Questo delay non serve perch� gestisco il primo comando I2C ricevuto
                        GTTimeout2 = DEF_50MS_BS_2MS;
                        GTTime2_expired = FALSE;
                        GTTimeout2_flag = TRUE;

                        while (GTTime2_expired == FALSE) {
                            __delay_us(10);
                        }
             */
            __delay_ms(200);
            fase_centralina = FASE_940_RUN; // mettiamo in ricezione comandi

            break;

        case FASE_940_RUN:
            if (GTKASendNotify) {
                AddNotifyToTel(I2CEVT_NOTIFY, I2CEVT_NOT_DTO_OK);
                GTKATimeout_flag = FALSE;
                GTKASendNotify = FALSE;
            }

            if (firstTime) {
                // Indica primo avvio da power ON
                AddNotifyToTel(I2CEVT_NOTIFY, I2CEVT_NOT_DTO_ON);
                //TauSleepI2C = ( DEF_3SEC_BS_100MS * 4);
                firstTime = FALSE;
                __delay_us(10);
            }

            // attesa comandi da parte del telefono
            switch (cmd_telefono) {
                case CMDW_TEL_AUTOPP_ON:

                    // start procedura di autoapprendimento
                    fase_centralina = FASE_940_AUTOAPP_1;
#ifdef AUTOAPPRENDIMENTO_SMART
					faseAutoApp1_HappyHour = TRUE;
					SetRFMode(RF_RECEIVER);

					ctr_timer_10s = 0;
#endif

                    ctr_tx_autoapprendimento = 0;
                    flg_buffer_full = 0;
                    //flg_trasponder_found = 0;   //24.15
                    ctr_trasponder_found = 0;
                    val_last_autop_sirena_id = 0;
                    val_last_autop_sirena_sn_hh = 0;
                    val_last_autop_sirena_sn_hl = 0;
                    val_last_autop_sirena_sn_lh = 0;
                    val_last_autop_sirena_sn_ll = 0;

                    // set stato bit autoapprendimento attivo
                    stato_pic_low = stato_pic_low | STATO_PIC_AUTOAPP;

                    // set sync word passpartou
                    RegisterSet(REG_SYNCBYTE0, SYNC_WORD_PASS_0);
                    RegisterSet(REG_SYNCBYTE1, SYNC_WORD_PASS_1);
                    RegisterSet(REG_SYNCBYTE2, SYNC_WORD_PASS_2);
                    RegisterSet(REG_SYNCBYTE3, SYNC_WORD_PASS_3);
                    cmd_telefono = 0;
                    break;

                case CMDW_TEL_TEST_ON:
                    // set fase di test con la ricerca broadcast di tipo B
                    fase_centralina = FASE_940_RICERCA_BROADCAST;
                    // reset contatore per ricerca trasponder
                    val_id_ultimo_trovato = 0;
                    cmd_telefono = 0;
                    InTest = TRUE;
                {
                    // All'entrata in test tolgo le flag a tutti i dispositivi
                    unsigned char i;
                    for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++) {
                        pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);
                        EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_TESTATO;
                        EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_ANOM_RSSI;
                    }
                }
                    // set stato bit  test attivo
                    stato_pic_low = stato_pic_low | STATO_PIC_TEST;
                    break;

                case CMDW_TEL_SUPERV_ON:
                    // set fase di test 
                    fase_centralina = FASE_940_TEST;
                    cmd_telefono = 0;
                    stato_pic_low = stato_pic_low | STATO_PIC_SUPERV;
                    break;
                case CMDW_TEL_CANC_ON:
                    // comando di fine test
                    fase_centralina = FASE_940_CANCELLAZIONE;

                    stato_pic_low = stato_pic_low | STATO_PIC_CANC;
                    cmd_telefono = 0;
                    break;
                case CMDW_TEL_WRITE_SYNC_CODE:
                    // comando per reimpostare un nuovo SYNC CODE
                    fase_centralina = FASE_940_WRITE_SYNC_CODE;
                    stato_pic_High = stato_pic_High | STATO_PIC_NEW_SITE_CODE;
                    cmd_telefono = 0;
                    break;
                case CMDW_TEL_ADD_DEVICE:
                    // comando per reimpostare un nuovo SYNC CODE
                    fase_centralina = FASE_940_ADD_DEVICE;
                    stato_pic_High = stato_pic_High | STATO_PIC_ADD_DEVICE;
                    cmd_telefono = 0;
                    break;
                case CMDW_TEL_RICERCA_OFF:
                    // attualmente mai inviato dal 999
                    stato_pic_low = stato_pic_low & STATO_PIC_SUPERV_NOT;
                    val_id_ultimo_trovato = val_id_ultimo_trovato_last;
                    AddNotifyToTel(I2CEVT_RICERCA, I2CEVT_RICERCA_KO);
                    cmd_telefono = 0;
                    break;
                case CMDW_TEL_RICERCA_BROADCAST:
                    fase_centralina = FASE_940_RICERCA_BROADCAST;
                    stato_pic_low = stato_pic_low | STATO_PIC_SUPERV;
                    cmd_telefono = 0;
                    break;
                case CMDW_TEL_ATT_VERIFICA:
                case CMDW_TEL_ATT_VER_SLEEP:
                    // Devo attivare la timoeut per gestire la procedura di 
                    // Verifica presenza
                    stato_pic_low = STATO_PIC_IN_VERIFICA;
                    flg_reqVerPresenza = TRUE;
                    cnt_pollingVerPres = 0;
                    tau_pollingVerPres = timeout_verifica_presenza;
                    if (cmd_telefono == CMDW_TEL_ATT_VERIFICA) {
                        cnt_deb_verifica_presenza = 0;
                        antirapinaNotify = FALSE;
                    }
                    //if(cmd_telefono == CMDW_TEL_ATT_VER_SLEEP) 
                    //fase_centralina = FASE_940_POWER_UP;
                    cmd_telefono = 0;
                    break;

                case CMDW_TEL_VERIFICA_ON:
                    // devo attivare la procedura di verifica presenza
                    fase_centralina = FASE_940_VERIFICA_PRES;
                    cmd_telefono = 0;
                    // reset contatore per ricerca trasponder
                    ctr_id_ricerca = 0;
                    break;

                case CMDW_TEL_VERIFICA_OFF:
                    // devo interrompere la procedura di verifica presenza
                    ferma_verifica_presenza();
                    cmd_telefono = 0;
                    //suspendTxMng = 0;
                    //tmrEnableTxMng = 0;
                    send100_2 = FALSE;
                    break;
                case CMDW_TEL_PROSS_IN_START:
                    // Si programma l'attivazionde della procedura di prossimit� IN
                    ferma_verifica_presenza();
                    flg_reqProssIn = TRUE;
                    cnt_pollingProssIn = 0;
                    tau_pollingProssIn = tau_polling_prossimita;
                    cmd_telefono = 0;
                    cnt_debounce_prossimita = 0;
                    cnt_timeout_prossimita = timeout_prossimita;
                    stato_pic_low |= STATO_PIC_IN_PROSS_IN;
                    if (!id_prossimita) {
                        BYTE i;
                        // E' da attivare una prossimita broadcast
                        val_id_ultimo_trovato = 0;
                        fase_centralina = FASE_940_RICERCA_BROADCAST;
						reset_mskT(msk_prossimita_out_multipla);

                        for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++)
                            counter_prossimita[i] = 0;
                    }
                    break;
                case CMDW_TEL_PROSS_IN_ON:
                    // Prossimit� IN attiva
                    fase_centralina = FASE_940_PROSSIMITA_IN;
                    cmd_telefono = 0;
                    ctr_id_ricerca = 0;
                    break;
                case CMDW_TEL_PROSS_IN_STOP:
                case CMDW_TEL_PIN_STOP_SLEEP:
                    // devo interrompere la procedura
                    flg_reqProssIn = FALSE;
                    cnt_pollingProssIn = 0;
                    tau_pollingProssIn = 0;
                    cnt_timeout_prossimita = 0;
                    id_prossimita = 0;
                    timeout_prossimita = 0;
                    tau_polling_prossimita = 0;
                    distanza_prossimita = 0;
                    debounce_prossimita = 0;
                    if (cmd_telefono == CMDW_TEL_PIN_STOP_SLEEP)
                        fase_centralina = FASE_940_POWER_UP;
                    cmd_telefono = 0;
                    stato_pic_low &= STATO_PIC_IN_PROSS_OUT_NOT;
                    break;
                case CMDW_TEL_PROSS_OUT_START:
                    // Si programma l'attivazionde della procedura di prossimit� OUT
                    ferma_verifica_presenza();
                    flg_reqProssOut = TRUE;
                    cnt_pollingProssOut = 0;
                    tau_pollingProssOut = tau_polling_prossimita;
                    cnt_timeout_prossimita = timeout_prossimita;
                    cmd_telefono = 0;
                    if (!id_prossimita) {
                        // gestione prossimita multipla
                        BYTE i;
                        cnt_debounce_prossimita = 0;
						reset_mskT(msk_prossimita_out_multipla);

                        for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++)
                            counter_prossimita[i] = 0;
                        val_id_ultimo_trovato = 0;
                        fase_centralina = FASE_940_RICERCA_BROADCAST;
                    }
                    stato_pic_low |= STATO_PIC_IN_PROSS_OUT;
                    break;
                case CMDW_TEL_PROSS_OUT_ON:
                    // Prossimit� OUT attiva
                    fase_centralina = FASE_940_PROSSIMITA_OUT;
                    cmd_telefono = 0;
                    ctr_id_ricerca = 0;
                    break;
                case CMDW_TEL_PROSS_OUT_STOP:
                    // devo interrompere la procedura 
                    flg_reqProssOut = FALSE;
                    cnt_pollingProssOut = 0;
                    tau_pollingProssOut = 0;
                    cnt_timeout_prossimita = 0;
                    id_prossimita = 0;
                    timeout_prossimita = 0;
                    tau_polling_prossimita = 0;
                    distanza_prossimita = 0;
                    debounce_prossimita = 0;
                    cmd_telefono = 0;
                    stato_pic_low &= STATO_PIC_IN_PROSS_OUT_NOT;
                    break;
                case CMDW_TEL_SLEEP_PIC:
                    stato_pic_low = STATO_PIC_SLEEP;
                    //fase_centralina = FASE_940_POWER_UP;
                    // Faccio partire timeout per lasciare il tempo
                    // di comunicare il fbcmd al wmp100
                    GTRXTimeoutExt = DEF_2SEC_BS_100MS;
                    GTRXTimeExt_expired = FALSE;
                    GTRXTimeoutExt_flag = TRUE;
                    cmd_telefono = 0;
                    break;
                case CMDW_TEL_GST_KEEP_ALIVE:
                    GTKATimeout_flag = TRUE;
                    cmd_telefono = 0;
                    break;
                default:
                    break;
            }

            if ((fase_centralina == FASE_940_RUN) && !TauSleepI2C && TelBuffIsEmpty() && !GTRXInibitTimeout_flag) {
                if (!(stato_pic_low & STATO_PIC_SUPERV)) {
                    // viene processato lo sleep del pezzo
                    cmd_telefono = 0;
                    //flg_reqVerPresenza = TRUE;	TODO messe per test
                    //tau_pollingVerPres = 20;		TODO messe per test
                    fase_centralina = FASE_940_POWER_UP;
                } else {
                    AddNotifyToTel(I2CEVT_RICERCA, I2CEVT_RICERCA_KO);
                    stato_pic_low = stato_pic_low & STATO_PIC_SUPERV_NOT;
                }
            }

            break;
        case FASE_940_AUTOAPP_1:
        case FASE_940_AUTOAPP_2:

            // invio comando ricerca trasponder per autopprendimento
#ifndef AUTOAPPRENDIMENTO_SMART
            // incrementa contatore cicli autoapprendimento
            ctr_tx_autoapprendimento++;
#endif

            // verifica se clicli trascorsi
            if (ctr_tx_autoapprendimento > MAX_CICLI_TX_AUTOAPP) {
                // cicli di trasmissione raggiunti senza nessuna risposta
                // ripristino valore SYNC con SITE code
                // set sync word da site code
				//setNewValForSyncCode();


                // uscita da fase di autoapprendimento
                // vai in fase di attesa comando di fine autoapprendimento
                fase_centralina = FASE_940_FINE_AUTOAPP;
                // segnalazione autoapprendimento terminato KO
                AddNotifyToTel(I2CEVT_AUTOAPP, I2CEVT_AUTOAPP_KO);
                // set stato numero centraline autoapprese
                get_nr_sirene_autoapp();
                break;
            }

#ifdef AUTOAPPRENDIMENTO_SMART
			if (!faseAutoApp1_HappyHour)
#endif
			{

				// TX - trasmissione messaggio di ricerca
				GTMaxCount = 110;
				GTTXcounter = 1;

				TxPacket[COUNTER_HIGH] = 0;
				TxPacket[COUNTER_LOW] = 1;
				TxPacket[MAX_COUNTER_HIGH] = 0;
				TxPacket[MAX_COUNTER_LOW] = GTMaxCount;
			}

            if (fase_centralina == FASE_940_AUTOAPP_1) {
#ifdef AUTOAPPRENDIMENTO_SMART
				if (!faseAutoApp1_HappyHour)
#endif
				{
					// prima parte di autoapprendimento
					Status_Tx_cmd_0 = Status_Tx_cmd_0 & MASK_RESET_BIT_STOP_AUTOAPP;
					Status_Tx_cmd_0 = Status_Tx_cmd_0 | MASK_BIT_START_AUTOAPP;

					// set numero ID trasponder = 0x0 nessun ID selezionato
					Status_Tx_cmd_2 = TYP_GT940TRP | 0x0;
				}
            } else {
                if (flg_buffer_full == 0) {
                    // buffer non pieno
                    // prosegui con la seconda parte di autoapprendimento
                    // potrebbe essere  gi� autoappreso in precedenza
                    Status_Tx_cmd_0 = Status_Tx_cmd_0 & MASK_RESET_BIT_START_AUTOAPP;
                    Status_Tx_cmd_0 = Status_Tx_cmd_0 | MASK_BIT_STOP_AUTOAPP;
                    // set numero ID trasponder  
                    Status_Tx_cmd_2 = (TYP_GT940TRP) | (ctr_trasponder_found & CONFIG_MASK_BIT_ID_TRASP);
                } else {
                    // buffer pieno
                    // segnala al trasponder lo stao di buffer pieno
                    Status_Tx_cmd_0 = Status_Tx_cmd_0 & MASK_RESET_BIT_START_AUTOAPP;
                    Status_Tx_cmd_0 = Status_Tx_cmd_0 | MASK_BIT_STOP_AUTOAPP;
                    Status_Tx_cmd_0 = Status_Tx_cmd_0 | MASK_BIT_BUFFER_FULL;
                    // set numero ID trasponder = 0xF - buffer pieno
                    Status_Tx_cmd_2 = TYP_GT940TRP | (ctr_trasponder_found & CONFIG_MASK_BIT_ID_TRASP);
                }
            }

            // Preparazione Messaggio di trasmissione e ricezione  

            Prepare_TxRx_Packet();


            // controllo se ricevuto un messaggio		
            if (GTRXcounter != 0) {

#ifdef AUTOAPPRENDIMENTO_SMART
				if (faseAutoApp1_HappyHour)
				{
					//check if msgRx is passpartout
					if (Status_Rx_cmd_0 == 0x00 && Status_Rx_cmd_1 == 0x33 && Status_Rx_cmd_2 == 0x55 && Status_Rx_cmd_3 == 0xAA)
					{
						//Payload_Rx_0 == 0x81 && Payload_Rx_1 == 0x42 && Payload_Rx_2 == 0x24 && Payload_Rx_3 == 0x18) // valuto solo Status Cmd per risparmiare codice
						faseAutoApp1_HappyHour = FALSE;
						ctr_tx_autoapprendimento = 0;  //resetto ctr timeout max
					}
					break;
				}
#endif	

                // verifica se bit start autoapprendimento settato dalla centralina
                if ((Status_Rx_cmd_0 & MASK_BIT_START_AUTOAPP) != 0) {
                    // autoapprendimento attivo
                    // verifica se trasponder gi� presente
                    /*flg_trasponder_found = 1;
                    for (i =0 ; i < MAX_EEPR_NR_TRASPONDER; i ++)
                    {	
                        pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER) ;
                        if (!verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID]))
                        {
                            // trasponder non presente 
                            // almeno un posto libero interrompo la verifica
                            flg_trasponder_found = 0;
                            break;
                        }
                    }
                     */
                    //flg_trasponder_found = 0;  //24.15
                    //if (flg_trasponder_found == 0)  24.15
                    //{
                        // trasponder non trovato
                        // ricerca primo posto libero 
                        get_next_id_autoap();

                        // verifica se almeno un posto libero
                        if (val_id_autoapp == 0) {
                            // numero massimo trasponder autoappresi
                            // non autoapprende il trasponder
                            // set buffer pieno
                            flg_buffer_full = 1;
                            // set segnalazione buffer pieno
                            ctr_trasponder_found = CONFIG_MASK_BIT_ID_TRASP;
                        } else {
                            // almeno un posto libero - autoapprendimento va avanti
                            // trasponder non presente
                            // lo inserisco in memoria
                            // trasponder trovato
                            pointer_trasponder = (ctr_id_autoapp * MAX_EEPR_DATA_TRASPONDER);
                            set_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID], TRUE);

                            val_last_autop_sirena_id = EE_trasponder[pointer_trasponder + EE_ID];
                            val_last_autop_sirena_sn_hh = Payload_Rx_0;
                            val_last_autop_sirena_sn_hl = Payload_Rx_1;
                            val_last_autop_sirena_sn_lh = Payload_Rx_2;
                            val_last_autop_sirena_sn_ll = Payload_Rx_3;


                            // VERIFICA 
                            // aggiungere gestione memorizzazione contatore TX
                            EE_trasponder[pointer_trasponder + EE_STATO] = TRAS_ATTIVO;
                            eeprom_write(pointer_trasponder + EE_STATO, TRAS_ATTIVO);

                            ctr_trasponder_found = val_id_autoapp;
                        }
                    //}
                    //24.15
                        
                    /*
                    else {
                        // trasponder trovato
                        // set Id trasponder gi� presente
                        ctr_trasponder_found = (i + 1);
                    }
                    */

                    // set attesa 3 secondi
					GTRXTimeoutExt = DEF_3SEC_BS_100MS;
                    GTRXTimeExt_expired = FALSE;
                    GTRXTimeoutExt_flag = TRUE;

                    while (GTRXTimeExt_expired == FALSE) {
                        __delay_us(10);
                    }

                    // set fase successiva per attesa feedback fine autoapprendimento
                    fase_centralina = FASE_940_AUTOAPP_2;
                    // reset contatore cicli di ricezione
                    ctr_tx_autoapprendimento = 0;
                    break;
                }

                // verifica se bit stop autoapprendimento settato dalla centralina
                if ((Status_Rx_cmd_0 & MASK_BIT_STOP_AUTOAPP) != 0) {
                    // autoapprendimento trasponder terminato
                    // reset contatore cicli di ricezione
                    ctr_tx_autoapprendimento = 0;
                    // ripristino valore SYNC con SITE code
                    // set sync word da site code
					//setNewValForSyncCode();

                    // uscita da fase di autoapprendimento
                    // vai in fase di attesa comando di fine autoapprendimento
                    fase_centralina = FASE_940_FINE_AUTOAPP;

                    // segnalazione autoapprendimento terminato OK
                    if (!flg_buffer_full)
                        AddNotifyToTel(I2CEVT_AUTOAPP, I2CEVT_AUTOAPP_OK);
                    else
                        AddNotifyToTel(I2CEVT_AUTOAPP, I2CEVT_AUTOAPP_BUFF_KO);
                    break;

                }
            }
            break;
            // Fase di attesa comando di fine autoapprendimento
        case FASE_940_FINE_AUTOAPP:
			// attesa comandi da parte del telefono
#ifndef AUTOAPPRENDIMENTO_SMART
			if (cmd_telefono == CMDW_TEL_AUTOPP_OFF)

			{
				// comando di fine test
				fase_centralina = FASE_940_RUN;
				// reset stato autoapprendimento attivo
				stato_pic_low = stato_pic_low & STATO_PIC_AUTOAPP_NOT;
				cmd_telefono = 0;
				break;
			}
#else 
			exitAutoapprendimento();
#endif

            break;

            // Fase di Test comunicazione
        case FASE_940_TEST:
            // Questa fase diventa la ricerca singola di un singolo
            // trasponder in modo tale da poter inviare anche il valore di RSSI e batteria

            // TX - trasmissione messaggio di ricerca
            GTMaxCount = MAX_TX_PACKET_200K_2S;
            GTTXcounter = 1;

            TxPacket[COUNTER_HIGH] = 0;
            TxPacket[COUNTER_LOW] = 1;
            TxPacket[MAX_COUNTER_HIGH] = MAX_TX_PACKET_200K_H_2S;
            TxPacket[MAX_COUNTER_LOW] = MAX_TX_PACKET_200K_L_2S;

            // set status command
            Status_Tx_cmd_0 = MASK_BIT_START_TEST; // comando di test
            Status_Tx_cmd_1 = 0x0; // non usato
            Status_Tx_cmd_2 = TYP_GT940TRP | (val_id_ultimo_trovato & CONFIG_MASK_BIT_ID_TRASP); // invio tipo di centralina e indirizzo trasponder
            // comando non usato
            Status_Tx_cmd_3 = 0; // comando vuoto

            // Preparazione Messaggio di trasmissione e ricezione  
            Prepare_TxRx_Packet();

            // controllo se ricevuto un messaggio		
            if (GTRXcounter != 0) {
                pointer_trasponder = ((ctr_id_ricerca - 1) * MAX_EEPR_DATA_TRASPONDER);
                RSSI_trasponder = RSSIRegVal;
                // ricalcolo batteria 
                setTrasponderValBat(Status_Rx_cmd_3, val_id_ultimo_trovato, Status_Rx_cmd_0);
                val_id_ultimo_trovato_last = val_id_ultimo_trovato;

                if (flgNotAnomalia) {
                    AddNotifyToTel(I2CEVT_ANOMALIA_TRASP, I2CEVT_ANOMALIA_TRASP_BATT);
                    flgNotAnomalia = FALSE;
                    ;
                }
                //pointer_trasponder = ( (val_id_ultimo_trovato - 1) * MAX_EEPR_DATA_TRASPONDER);
                //EE_trasponder[pointer_trasponder + EE_STATO] &= ~ TRASP_ANOM_RSSI;					

                // set attesa 120 ms
                GTRXTimeout = DEF_120MS_BS_2MS;
                GTRXTime_expired = FALSE;
                GTRXTimeout_flag = TRUE;

                while (GTRXTime_expired == FALSE) {
                    __delay_us(10);
                }

                AddNotifyToTel(I2CEVT_RICERCA, I2CEVT_RICERCA_OK);
            } else
                AddNotifyToTel(I2CEVT_RICERCA, I2CEVT_RICERCA_KO);

            stato_pic_low = stato_pic_low & STATO_PIC_SUPERV_NOT;

            // set attesa 200  ms
            GTRXTimeout = DEF_200MS_BS_2MS;
            GTRXTime_expired = FALSE;
            GTRXTimeout_flag = TRUE;

            while (GTRXTime_expired == FALSE) {
                __delay_us(10);
            }

            fase_centralina = FASE_940_RUN;
            break;

            // Fase di Cancellazione
        case FASE_940_CANCELLAZIONE:
            // set attesa 1 secondi
            GTRXTimeoutExt = DEF_1SEC_BS_100MS;
            GTRXTimeExt_expired = FALSE;
            GTRXTimeoutExt_flag = TRUE;

            while (GTRXTimeExt_expired == FALSE) {
                __delay_us(10);
            }

            if (CmdCancAll) {
                for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++) {
                    pointer_trasponder = i * MAX_EEPR_DATA_TRASPONDER;
                    eeprom_write(pointer_trasponder + EE_ID, (i + 1));
                    eeprom_write(pointer_trasponder + EE_STATO, VAL_STATO_NULL);
                    set_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID], FALSE);
                }
                CmdCancAll = FALSE;
            } else if (CmdCancId) {
                delete_sirena(CmdCancId); //cancellazione in EEPROM
                CmdCancId = 0;
            } else if (CmdAttDisId) {
                att_diss_sirena(CmdAttDisId);
                CmdAttDisId = 0;
            }
            Read_EE_param(FALSE); // lettura parametri da eeprom
            // lettura  numero di sirene autoapprese
            get_nr_sirene_autoapp();

            // comando di fine test
            fase_centralina = FASE_940_RUN;
            // reset stato test  attivo
            stato_pic_low = stato_pic_low & STATO_PIC_CANC_NOT;
            AddNotifyToTel(I2CEVT_NUOVA_CONF, I2CEVT_NUOVA_CONF_OK);
            break;

            // Fase di Supervisione
        case FASE_940_WRITE_SYNC_CODE:
            // set attesa 1 secondi
            GTRXTimeoutExt = DEF_1SEC_BS_100MS;
            GTRXTimeExt_expired = FALSE;
            GTRXTimeoutExt_flag = TRUE;

            while (GTRXTimeExt_expired == FALSE) {
                __delay_us(10);
            }

            //if(NEW_site_code[0] && NEW_site_code[1] && NEW_site_code[2] && NEW_site_code[3])
        {
            BYTE i;
            eeprom_write(EE_SITE_CODE_VALID_ADD, 0xFF);
            for (i = 0; i < EE_SITE_CODE_SIZE; i++) {
                EE_site_code[i] = NEW_site_code[i];
                NEW_site_code[i] = 0x00;
                eeprom_write(EE_SITE_CODE_ADD + i, EE_site_code[i]);
            }
            eeprom_write(EE_SITE_CODE_VALID_ADD, EE_SITE_CODE_VALID);

            // set nuovo valore per sync word
			setNewValForSyncCode();

            AddNotifyToTel(I2CEVT_WRITE_SYNC_WORD, I2CEVT_WRITE_SYNC_WORD_OK);
        }
            //else
            //AddNotifyToTel(I2CEVT_WRITE_SYNC_WORD, I2CEVT_WRITE_SYNC_WORD_KO);	

            stato_pic_High = stato_pic_High | STATO_PIC_NEW_SITE_CODE_NOT;
            fase_centralina = FASE_940_RUN;
            break;
        case FASE_940_ADD_DEVICE:
            // set attesa 1 secondi
            GTRXTimeoutExt = DEF_1SEC_BS_100MS;
            GTRXTimeExt_expired = FALSE;
            GTRXTimeoutExt_flag = TRUE;

            while (GTRXTimeExt_expired == FALSE) {
                __delay_us(10);
            }

            if ((NEW_info_device[0] > 0) && (NEW_info_device[0] <= MAX_EEPR_NR_TRASPONDER)) {
                // ID compreso nel range richiesto
                unsigned char idx = NEW_info_device[0] - 1; // rispetto a quello inviato su i2c decrementarlo
                pointer_trasponder = (idx * MAX_EEPR_DATA_TRASPONDER);
                if (!verifica_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID])) {
                    // Spazio libero, possiamo aggiungerlo
                    val_last_autop_sirena_id = EE_trasponder[pointer_trasponder + EE_ID];
                    val_last_autop_sirena_sn_hh = NEW_info_device[1];
                    val_last_autop_sirena_sn_hl = NEW_info_device[2];
                    val_last_autop_sirena_sn_lh = NEW_info_device[3];
                    val_last_autop_sirena_sn_ll = NEW_info_device[4];

                    set_ee_presenza_trasponder(EE_trasponder[pointer_trasponder + EE_ID], TRUE);
                    // VERIFICA 
                    // aggiungere gestione memorizzazione contatore TX
                    EE_trasponder[pointer_trasponder + EE_STATO] = TRAS_ATTIVO;
                    eeprom_write(pointer_trasponder + EE_STATO, TRAS_ATTIVO);

                    ctr_trasponder_found = idx + 1;
                    AddNotifyToTel(I2CEVT_ADD_DEVICE, I2CEVT_ADD_DEVICE_OK);
                } else
                    AddNotifyToTel(I2CEVT_ADD_DEVICE, I2CEVT_ADD_DEVICE_TR_KO);
            } else
                AddNotifyToTel(I2CEVT_ADD_DEVICE, I2CEVT_ADD_DEVICE_ID_KO);

            stato_pic_High = stato_pic_High | STATO_PIC_ADD_DEVICE_NOT;
            fase_centralina = FASE_940_RUN;
            break;
        case FASE_940_RICERCA:
            //case FASE_940_VERIFICA_PRES : 	
            // invio comando ricerca trasponder
            // Verifico se cercare subito l'ultimo trasponder trovato o iniziare dal primo
            /* TODO TOGLIERE ANCHE I COMMENTI !!!!!!!!*/
            break;
        case FASE_940_RICERCA_BROADCAST:
        case FASE_940_VERIFICA_PRES:
            // Attivazione ricerca broadcast. La facciamo tutta in un solo ciclo 
            // senza uscire e rientrare
			if (!flagReceivingAnyTrasmitter)
			{
                if(flagRxCmdBroadI2c)
                {
                    val_id_ultimo_trovato = valIdBroadFromI2c;
                    if(flagRxCmdBroadI2c == 2)
                        val_id_ultimo_trovato = 0;
                    
                    flagRxCmdBroadI2c = 0;
                    ricBroaDaI2cInCorso = 1;
                }
                
                if(fase_centralina == FASE_940_VERIFICA_PRES)
                   val_id_ultimo_trovato = val_id_ultimo_trovato_verifica;

				// 1 - Verifichiamo che ci siano trasponder attivi in configurazione
				// e che la radio non sia inibita 
				get_next_id_ricerca(TRUE);
				if ((val_id_ricerca == 0) || GTRXInibitTimeout_flag) {
					stato_pic_low = stato_pic_low & STATO_PIC_SUPERV_NOT;
					AddNotifyToTel(I2CEVT_RICERCA, I2CEVT_RICERCA_KO);
					cmd_telefono = 0;
					fase_centralina = FASE_940_RUN;
					break;
				}
			}

            // 2 - Prepariamo il messaggio in uscita
            // TX - trasmissione messaggio di ricerca
            GTMaxCount = MAX_TX_PACKET_200K_2S;
            GTTXcounter = 1;

            TxPacket[COUNTER_HIGH] = 0;
            TxPacket[COUNTER_LOW] = 1;
            TxPacket[MAX_COUNTER_HIGH] = MAX_TX_PACKET_200K_H_2S;
            TxPacket[MAX_COUNTER_LOW] = MAX_TX_PACKET_200K_L_2S;

            // set status command

            BYTE idFirst, idTrasp, idVer, i, idTrasOn, tmpID;
            
            if( fase_centralina == FASE_940_VERIFICA_PRES)
                tmpID = val_id_ultimo_trovato_verifica;
            else 
                tmpID = val_id_ultimo_trovato;

            idFirst = idTrasp = tmpID;
            idTrasOn = val_id_ultimo_trovato_last;

            // Se il trasponder da cercare non � pi� attivo non viene messo l'ID 
            // nel comando di ricerca broadcast
            pointer_trasponder = ((val_id_ultimo_trovato - 1) * MAX_EEPR_DATA_TRASPONDER);
            if (val_id_ultimo_trovato && !(EE_trasponder[pointer_trasponder + EE_STATO] & TRAS_ATTIVO))
                idFirst = 0;

            if (fase_centralina == FASE_940_VERIFICA_PRES) {
                if (val_id_ultimo_trovato_verifica) // E' attiva verifica A
                {
                    if (val_id_ultimo_trovato_verifica <= MAX_EEPR_NR_TRASPONDER)
                        idVer = val_id_ultimo_trovato_verifica;
                    else {
                        //idFirst = 0;
                        idVer = idFirst = val_id_ultimo_trovato_verifica - MAX_EEPR_NR_TRASPONDER - 1;
                    }
                } else // E' attiva verifica B
                {
                    BYTE idT = 0;
					get_mask(BROADCAST, &idT);
                }
            }

            Status_Tx_cmd_0 = MASK_BIT_RICERCA_BROADCAST; // comando di ricerca trasponders broadcast
            Status_Tx_cmd_1 = 0x0; // non usato
            Status_Tx_cmd_2 = TYP_GT940TRP | (idFirst & CONFIG_MASK_BIT_ID_TRASP); // invio tipo di centralina e indirizzo trasponder
            Status_Tx_cmd_3 = 0; // non usato

            // 3 - Ora inizia la fase di spedizione e ricezione, viene passato l'ID se ricerca A

            // Togliamo lo stato di trasponder verificato a tutti
            for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++)
            {
                pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);
                if( !flagReceivingAnyTrasmitter)
                {
                    EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_VERIFICATO;
                    EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_ATTIVO_GUIDA;
                }
            }

            val_id_ultimo_trovato = 0;
            // RICERCA RADIO
            Prepare_TxRx_Packet_broadcast(idTrasp, mskCmdRicerca);

            
            
            //mskCmdRicerca = 0;
            // 4 - Gestione risultato in base al tipo di ricerca
            if (idTrasp)
            {
                BYTE esito, skip=0;

                // Gestione RICERCA o VERIFICA
                if (fase_centralina == FASE_940_RICERCA_BROADCAST)
                {
                    // E' stata eseguita una ricerca per ID, quindi dobbiamo verificare
                    // se un trasponder ha risposto
                    if(!flagReceivingAnyTrasmitter)
                    {
                        stato_pic_low = stato_pic_low & STATO_PIC_SUPERV_NOT;
                        esito = (val_id_ultimo_trovato) ? I2CEVT_RICERCA_OK : I2CEVT_RICERCA_KO;
                    }
                    else
                    {
                        if(!flagTxByUserPress)
                            skip = 1;
                        else
                            esito = I2CEVT_RICERCA_OK_BY_TX;
                    }
                    flagTxByUserPress = 0;

                    if(!skip)
                        AddNotifyToTel(I2CEVT_RICERCA, esito);
                }
                else if (fase_centralina == FASE_940_VERIFICA_PRES)
                {
                    if(txByUserPress())
                        break;
                   
                    val_id_ultimo_trovato_verifica = val_id_ultimo_trovato;

                    if (!(mskCmdRicerca & CMDW_PIC_BIT_RICERCA_C)) {
                        if (idVer != val_id_ultimo_trovato) {
                            // C'� stata una variazione da segnalare
                            // e non � attiva ricerca tipo C
                            if (val_id_ultimo_trovato) {
                                //esito = I2CEVT_VER_PRES_CAMBIO;
                                if (debounce_verifica_presenza) {
                                    cnt_deb_verifica_presenza = 0;
                                    antirapinaNotify = FALSE;
                                }
                                AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_CAMBIO);
                            } else {
                                val_id_ultimo_trovato = MAX_EEPR_NR_TRASPONDER + 1;
                                if (!debounce_verifica_presenza)
                                    AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_NO_TRASP);
                                else
                                    cnt_deb_verifica_presenza++;
                            }
                            //AddNotifyToTel(I2CEVT_VERIFICA_PRES, esito);
                        }

                        if (debounce_verifica_presenza && !val_id_ultimo_trovato) {
                            cnt_deb_verifica_presenza++;
                            if ((cnt_deb_verifica_presenza >= debounce_verifica_presenza) && !antirapinaNotify) {
                                AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_NO_TRASP_ANTIRAPINA);
                                antirapinaNotify = TRUE;
                            }
                            val_id_ultimo_trovato = MAX_EEPR_NR_TRASPONDER + 1;
                        } else
                            cnt_deb_verifica_presenza = 0;

                        // Essendo abilitata la funzione verifica presenza singola non � mai possibile
                        // chiudere la funzione con val_id_ultimo_trovato == 0	 						
                        if (!val_id_ultimo_trovato) val_id_ultimo_trovato = MAX_EEPR_NR_TRASPONDER + 1;
                    } else //if(mskCmdRicerca & CMDW_PIC_BIT_RICERCA_C)
                    {
                        // e' attiva ricerca tipo C
                        // controllo se ID DRIVER ON � cambiato
                        // o se il trasponder � presente ugualmente
                        if (val_id_ultimo_trovato)
                        {
                            if (idVer != val_id_ultimo_trovato)
                            {
                                send100_2 = FALSE;
                                AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_CAMBIO);
                            }
                            if (debounce_verifica_presenza)
                            {
                                cnt_deb_verifica_presenza = 0;
                                antirapinaNotify = FALSE;
                            }
                        }
                        else
                        {    
                            BYTE idDriver = 0;
                            get_mask(BROADCAST, &idDriver);

                            if(idVer)
                            {
                                pointer_trasponder = ( (idVer - 1) * MAX_EEPR_DATA_TRASPONDER);
                                if(!(EE_trasponder[pointer_trasponder + EE_STATO] & TRASP_VERIFICATO) )                                
                                    idVer = 0;
                            }

                            if(!idVer && !send100_2)
                            {
                                AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_NO_TRASP);
                                send100_2 = TRUE;
                            }

                            //ANTIRAPINA
                            if (debounce_verifica_presenza && !idVer && !antirapinaNotify)
                            {
                                if(!esistevaUnTrasponder)
                                    cnt_deb_verifica_presenza++;
                                else
                                    cnt_deb_verifica_presenza=0;

                                if(cnt_deb_verifica_presenza >= debounce_verifica_presenza)
                                {
                                    AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_NO_TRASP_ANTIRAPINA); //antirapina
                                    if( !esistevaUnTrasponder )
                                        antirapinaNotify = TRUE;
                                }
                            }
                            else
                                cnt_deb_verifica_presenza = 0;

                            val_id_ultimo_trovato = MAX_EEPR_NR_TRASPONDER + 1 + idVer;
                        }
                    }
                    val_id_ultimo_trovato_verifica = val_id_ultimo_trovato;

                    cmd_telefono = CMDW_TEL_ATT_VER_SLEEP;
                }
            } else {
                BYTE esito;
                // Gestione RICERCA o VERIFICA
                if (fase_centralina == FASE_940_RICERCA_BROADCAST) {
                    // E' stata eseguita una ricerca B, quindi dobbiamo verificare se 
                    // almeno un trasponder ha risposto
                    // controlliamo anche se � stata avviato una prossimit�
                    if (flg_reqProssIn || flg_reqProssOut) {
                        if(orMskPres())
                        {
                            // almeno un trasponder � presente
							dWordMemCpy(mskPresProssimita, (const DWORD*)mskPres, 4);

                            if (flg_reqProssIn)
                                AddNotifyToTel(I2CEVT_PROSS_IN, I2CEVT_PROSS_IN_PRES);
                            else
                                AddNotifyToTel(I2CEVT_PROSS_OUT, I2CEVT_PROSS_OUT_PRES);
                        } else {
                            // Nessun trasponder � presente, notificare e terminare la funzione
                            if (flg_reqProssIn) {
                                cmd_telefono = CMDW_TEL_PROSS_IN_STOP;
                                fase_centralina = FASE_940_RUN;
                                AddNotifyToTel(I2CEVT_PROSS_IN, I2CEVT_PROSS_IN_DISABLE);
                            } else {
                                cmd_telefono = CMDW_TEL_PROSS_OUT_STOP;
                                fase_centralina = FASE_940_RUN;
                                AddNotifyToTel(I2CEVT_PROSS_OUT, I2CEVT_PROSS_OUT_DISABLE);
                            }
                        }
                    } else {
                        esito = (orMskPres()) ? I2CEVT_RICERCA_OK : I2CEVT_RICERCA_KO;

						if ((flagReceivingAnyTrasmitter && esito == I2CEVT_RICERCA_OK) || !flagReceivingAnyTrasmitter)
						{
							if (flagReceivingAnyTrasmitter)
							{
								get_mask(PRESENZA, 0);

								if (matchMsk())
								{
									if (Status_Rx_cmd_1 == VALUE_TX_BY_USER_PRESS)
									{
										sendChangeAlarmNotify(Status_Rx_cmd_2& CONFIG_MASK_BIT_ID_TRASP, I2CEVT_CHANGE_ALARM_STATE_ID_TRASPONDER);
										sendChangeAlarmNotify(Status_Rx_cmd_3, I2CEVT_CHANGE_ALARM_STATE_TX_COUNTER);
									}
								}
							}
							else

								AddNotifyToTel(I2CEVT_RICERCA_BROADCAST, esito);
							stato_pic_low = stato_pic_low & STATO_PIC_SUPERV_NOT;
						}
                        InTest = FALSE;
                    }
                }
                else if (fase_centralina == FASE_940_VERIFICA_PRES) 
                {
                    BYTE i, idMsk, idxInMsk, notifyIsAdded = 0;
					static DWORD localMsk[4];
                    BYTE finded=0, cnt=0, trCambiato = 0;
                    DWORD j, lIndice;

                    if(txByUserPress())
                        break;

                    //Verifico ultimo trovato in memoria
                    idMsk = (idTrasOn - 1) / 32;
                    idxInMsk = idTrasOn % 32; // troviamo indice relativo alla maschera
                    lIndice = (idxInMsk) ? 1 : 0x80000000;

                    if (lIndice != 0x80000000)
                    {
                        lIndice = 1;
                        for (i = 0; i < (idxInMsk - 1); i++)
                            lIndice *= 2;
                    }
                    
                    //verifico presenza ultimo trasp attivo
                    if(!(mskPres[idMsk] & lIndice))
                    {
                        trCambiato = 1;
                        if(mskCmdRicerca & CMDW_PIC_BIT_RICERCA_C && idTrasOn == val_id_ultimo_trovato_last)
                            val_id_ultimo_trovato_last = 0;
					}

                    for(i=0; i<4; i++)
					{
						if(mskPres[i] != localMsk[i])
						{
            				notifyIsAdded = 1;
							localMsk[i] = mskPres[i];
						}

                        j=0x80000000;
                        cnt = 32;

                        if( !finded && (!esistevaUnTrasponder || trCambiato))
                        {
                            while(j>0)
                            {
                                if( mskPres[3-i] & j)
                                {
                                    finded = 32*(4-i) - (32-cnt);
                                    break;
                                }
                                j/=2;
                                cnt --;
                            }
                        }
					}
					if(notifyIsAdded)
                    {
                        if( (!esistevaUnTrasponder || trCambiato) && !(mskCmdRicerca & CMDW_PIC_BIT_RICERCA_C) )
                            //attivo diventa l'ultimo trovato
                            val_id_ultimo_trovato_last = val_id_ultimo_trovato = finded;
                                    
                        AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_RICERCA_B);
                    }

                    if(!notifyIsAdded)
            		{
                        BYTE idDriver = 0;
                        get_mask(BROADCAST, &idDriver);

                        if(idDriver)
                            AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_RICERCA_B);
                    }

                    // gestione funzione antirapina verifica presenza multipla
                    if (debounce_verifica_presenza)
                    {
                        if (antirapinaNotify)
                        {
                            // V 24.43 no differenza tra antirapina ricerca C e B
    						//if(!(mskCmdRicerca & CMDW_PIC_BIT_RICERCA_C))

                            //if (!(mskCmdRicerca & CMDW_PIC_BIT_RICERCA_C))
                            {
                                if (orMskPres())
                                    antirapinaNotify = FALSE;
                            }
                            //else
                            //{
                            //    if (idTrasOn != val_id_ultimo_trovato_last) antirapinaNotify = FALSE;
                            //}
                        }

                        // V 24.43 no differenza tra antirapina ricerca C e B
                        //if (!(mskCmdRicerca & CMDW_PIC_BIT_RICERCA_C))
                        {
                            // Gestione con ricerca A, quindi situazione cercata
                            // maschera presenze uguale a zero
                            if (!mskPres[0] && !mskPres[1] && !mskPres[2] && !mskPres[3]) {
                                if ((++cnt_deb_verifica_presenza >= debounce_verifica_presenza)
                                        && !antirapinaNotify) {
                                    AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_NO_TRASP_B);
                                    antirapinaNotify = TRUE;
                                }
                            } else
                                cnt_deb_verifica_presenza = 0;
                        }
                        /*
                        else
                        {
                            // Gestione con ricerca C, quindi situazione cercata
                            // trasponder presente non pi� attivo
                            if (lIndice && !(mskPres[idMsk] & lIndice)) {
                                if ((++cnt_deb_verifica_presenza >= debounce_verifica_presenza)
                                        && !antirapinaNotify) {
                                    AddNotifyToTel(I2CEVT_VERIFICA_PRES, I2CEVT_VER_PRES_NO_TRASP_B);
                                    antirapinaNotify = TRUE;
                                }
                            } else
                                cnt_deb_verifica_presenza = 0;
                        }
                        */
                    }

                    cmd_telefono = CMDW_TEL_ATT_VER_SLEEP;
                    val_id_ultimo_trovato = 0;
                }
            }

            // fine ricerca broadcast reenable Tx mng
            if(!flagRxCmdBroadI2c)
            {
                //comand oricerca broadcast I2c gi� gestito
                suspendTxMng = 0;
                tmrEnableTxMng = 0;
            }
            else
            {
                //retriggera in main loop per gestire ricerca broadcast da cmd I2c
                suspendTxMng = 1;
                tmrEnableTxMng = 10;
                cmd_telefono = CMDW_TEL_RICERCA_BROADCAST; 
                flagReceivingAnyTrasmitter = 0;
                //fase_centralina = FASE_940_RICERCA_BROADCAST;
                //stato_pic_low = stato_pic_low | STATO_PIC_SUPERV;
            }

            if(ricBroaDaI2cInCorso)
                ricBroaDaI2cInCorso = 0;

            fase_centralina = FASE_940_RUN;
            break;

        case FASE_940_PROSSIMITA_IN:
            // Prossimita IN

            // 1- Controlliamo che non sia scaduta la timeout generale
            if (cnt_timeout_prossimita) {
                // 2- Interroghiamo il trasponder
                // TX - trasmissione messaggio di ricerca
                GTMaxCount = MAX_TX_PACKET_200K_2S;
                GTTXcounter = 1;

                TxPacket[COUNTER_HIGH] = 0;
                TxPacket[COUNTER_LOW] = 1;
                TxPacket[MAX_COUNTER_HIGH] = MAX_TX_PACKET_200K_H_2S;
                TxPacket[MAX_COUNTER_LOW] = MAX_TX_PACKET_200K_L_2S;

                Status_Tx_cmd_1 = 0x0; // non usato
                Status_Tx_cmd_2 = TYP_GT940TRP | (id_prossimita & CONFIG_MASK_BIT_ID_TRASP); // invio tipo di centralina e indirizzo trasponder
                Status_Tx_cmd_3 = 0; // comando vuoto

                if (id_prossimita) {
                    // Prossimita normale su un ID singolo,eseguo la ricerca normale
                    Status_Tx_cmd_0 = 0x0; // comando di ricerca trasponder					
                    // Preparazione Messaggio di trasmissione e ricezione  
                    Prepare_TxRx_Packet();
                    // controllo se ricevuto un messaggio		
                    pointer_trasponder = ((id_prossimita - 1) * MAX_EEPR_DATA_TRASPONDER);
                    if (GTRXcounter != 0) {
                        // 3 - controlliamo che sia dentro la distanza
                        if (RSSIRegVal < distanza_prossimita)
                            cnt_debounce_prossimita++;
                        val_id_ultimo_trovato = val_id_ultimo_trovato_last = id_prossimita;
                        EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_VERIFICATO;
                        RSSI_trasponder = RSSIRegVal;
                        setTrasponderValBat(Status_Rx_cmd_3, id_prossimita, Status_Rx_cmd_0); // calcolo batteria
                        if (flgNotAnomalia) {
                            AddNotifyToTel(I2CEVT_ANOMALIA_TRASP, I2CEVT_ANOMALIA_TRASP_BATT);
                            flgNotAnomalia = FALSE;
                            ;
                        }
                    } else {
                        cnt_debounce_prossimita++;
                        EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_VERIFICATO;
                    }

                    // 4 - Verifico se � da gestire la terminazione della funzione
                    if (cnt_debounce_prossimita >= debounce_prossimita) {
                        cmd_telefono = CMDW_TEL_PROSS_IN_STOP;
                        AddNotifyToTel(I2CEVT_PROSS_IN, I2CEVT_PROSS_IN_DISABLE);
                        fase_centralina = FASE_940_RUN;
                    } else {
                        // Riattivo conteggio per la successiva interrogazione
                        // e metto in sleep il PIC
                        flg_reqProssIn = TRUE;
                        cnt_pollingProssIn = 0;
                        tau_pollingProssIn = tau_polling_prossimita;
                        fase_centralina = FASE_940_POWER_UP;
                    }
                } else {
                    BYTE i;

                    indice = 0x0001;
					reset_mskT(mskIn);
					reset_mskT(mskRSSIPros);

                    // Prossimita broadcast, eseguo una ricerca broadcast di tipo B
                    Status_Tx_cmd_0 = MASK_BIT_RICERCA_BROADCAST; // comando di ricerca trasponders broadcast

                    // Togliamo lo stato di trasponder verificato a tutti
                    for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++) {
                        pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);
                        EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_VERIFICATO;
                        EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_ATTIVO_GUIDA;
                    }

                    Prepare_TxRx_Packet_broadcast(0, 0);

                    // 1- Devo verificare i trsponder che hanno risposto, con quelli che hanno risposto
                    // la prima volta. Di questi trasponder devo verificare quali sono dentro o fuori raggio
                    for (loopIdx = 0; loopIdx < MAX_EEPR_NR_TRASPONDER; loopIdx++) {
                        SetLoopMsk();
                        if ((indice & mskPresProssimita[loopMsk]) && (indice & mskPres[loopMsk])
                                && (indice & mskRSSIPros[loopMsk])) {
                            // Il trasponder � presente e dentro la soglia RSSI
                            mskIn[loopMsk] |= indice;
                            counter_prossimita[loopIdx] = 0;
                        } else
                            counter_prossimita[loopIdx]++;

                        if (counter_prossimita[loopIdx] >= debounce_prossimita)
                            mskPresProssimita[loopMsk] &= ~indice; // cos� non viene pi� controllato in futuro

                        if (indice & 0x80000000)
                            indice = 0x0001; // si resetta l'indice per il cambio di maschera
                        else
                            indice *= 2;
                    }

                    // 2 - Verifichiamo se c'� qualche trasponder dentro il raggio, altrimenti incrementiamo
                    // il debounce
                    if (!mskIn[0] && !mskIn[1] && !mskIn[2] && !mskIn[3])
                        cnt_debounce_prossimita++;
                    else
                        cnt_debounce_prossimita = 0;

                    // 3 - Verifico se � da gestire la terminazione della funzione
                    if (cnt_debounce_prossimita >= debounce_prossimita) {
                        cmd_telefono = CMDW_TEL_PROSS_IN_STOP;
                        AddNotifyToTel(I2CEVT_PROSS_IN, I2CEVT_PROSS_IN_DISABLE);
                        fase_centralina = FASE_940_RUN;
                    } else {
                        // Riattivo conteggio per la successiva interrogazione
                        // e metto in sleep il PIC
                        flg_reqProssIn = TRUE;
                        cnt_pollingProssIn = 0;
                        tau_pollingProssIn = tau_polling_prossimita;
                        fase_centralina = FASE_940_POWER_UP;
                    }
                }
            } else {
                // Dobbiamo interrompere la gestione della funzione della prossimit� IN
                // e dare l'avviso che per tutto il timeout � rimasto dentro il raggio
                cmd_telefono = CMDW_TEL_PROSS_IN_STOP;
                if (id_prossimita)
                    AddNotifyToTel(I2CEVT_PROSS_IN, I2CEVT_PROSS_IN_KO);
                else {
					dWordMemCpy(msk_prossimita_out_multipla, (const DWORD*)mskPresProssimita, 4);
                    AddNotifyToTel(I2CEVT_PROSS_IN, I2CEVT_PROSS_IN_B_KO);
                }

                fase_centralina = FASE_940_RUN;
                stato_pic_low &= STATO_PIC_IN_PROSS_IN_NOT;
            }
            break;
        case FASE_940_PROSSIMITA_OUT:
            // VERIFICHIAMO SE GESTIRE PROSSIMITA SINGOLA O MULTIPLA
            if (id_prossimita) {
                // Prossimita OUT SINGOLA!!
                // 1- Controlliamo che non sia scaduta la timeout generale
                if (cnt_timeout_prossimita) {
                    // 2 - Interroghiamo il trasponder
                    // TX - trasmissione messaggio di ricerca
                    GTMaxCount = MAX_TX_PACKET_200K_2S;
                    GTTXcounter = 1;

                    TxPacket[COUNTER_HIGH] = 0;
                    TxPacket[COUNTER_LOW] = 1;
                    TxPacket[MAX_COUNTER_HIGH] = MAX_TX_PACKET_200K_H_2S;
                    TxPacket[MAX_COUNTER_LOW] = MAX_TX_PACKET_200K_L_2S;

                    // set status command
                    Status_Tx_cmd_0 = 0x0; // comando di ricerca trasponder
                    Status_Tx_cmd_1 = 0x0; // non usato
                    Status_Tx_cmd_2 = TYP_GT940TRP | (id_prossimita & CONFIG_MASK_BIT_ID_TRASP); // invio tipo di centralina e indirizzo trasponder
                    // comando non usato
                    Status_Tx_cmd_3 = 0; // comando vuoto

                    // Preparazione Messaggio di trasmissione e ricezione  
                    Prepare_TxRx_Packet();

                    pointer_trasponder = ((id_prossimita - 1) * MAX_EEPR_DATA_TRASPONDER);
                    // controllo se ricevuto un messaggio		
                    if (GTRXcounter != 0) {
                        // 3 - controlliamo che sia dentro la distanza
                        // e retriggeriamo il timeout
                        if (RSSIRegVal >= distanza_prossimita)
                            cnt_timeout_prossimita = timeout_prossimita;
                        val_id_ultimo_trovato = val_id_ultimo_trovato_last = id_prossimita;
                        EE_trasponder[pointer_trasponder + EE_STATO] |= TRASP_VERIFICATO;
                        RSSI_trasponder = RSSIRegVal;
                        setTrasponderValBat(Status_Rx_cmd_3, id_prossimita, Status_Rx_cmd_0); // calcolo batteria
                        if (flgNotAnomalia) {
                            AddNotifyToTel(I2CEVT_ANOMALIA_TRASP, I2CEVT_ANOMALIA_TRASP_BATT);
                            flgNotAnomalia = FALSE;
                            ;
                        }
                    } else {
                        EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_VERIFICATO;
                    }

                    // Riattivo conteggio per la successiva interrogazione
                    flg_reqProssOut = TRUE;
                    cnt_pollingProssOut = 0;
                    tau_pollingProssOut = tau_polling_prossimita;
                    fase_centralina = FASE_940_POWER_UP;
                } else {
                    // Dobbiamo interrompere la gestione della funzione della prossimit� OUT e
                    // Dare la segnalazione
                    cmd_telefono = CMDW_TEL_PROSS_OUT_STOP;
                    AddNotifyToTel(I2CEVT_PROSS_OUT, I2CEVT_PROSS_OUT_KO);
                    fase_centralina = FASE_940_RUN;
                    stato_pic_low &= STATO_PIC_IN_PROSS_OUT_NOT;
                }
            } else {
                BYTE i;

                indice = 0x0001;
				reset_mskT(mskIn);
				reset_mskT(mskOut);
				reset_mskT(mskRSSIPros);

                // Prossimita OUT MULTIPLA!!
                // 1 - Interroghiamo i trasponders con la ricerca B
                // TX - trasmissione messaggio di ricerca
                GTMaxCount = MAX_TX_PACKET_200K_2S;
                GTTXcounter = 1;

                TxPacket[COUNTER_HIGH] = 0;
                TxPacket[COUNTER_LOW] = 1;
                TxPacket[MAX_COUNTER_HIGH] = MAX_TX_PACKET_200K_H_2S;
                TxPacket[MAX_COUNTER_LOW] = MAX_TX_PACKET_200K_L_2S;

                // Prossimita broadcast, eseguo una ricerca broadcast di tipo B
                Status_Tx_cmd_0 = MASK_BIT_RICERCA_BROADCAST; // comando di ricerca trasponders broadcast
                Status_Tx_cmd_1 = 0x0; // non usato
                Status_Tx_cmd_2 = TYP_GT940TRP | (id_prossimita & CONFIG_MASK_BIT_ID_TRASP); // invio tipo di centralina e indirizzo trasponder
                Status_Tx_cmd_3 = 0;

                // Togliamo lo stato di trasponder verificato a tutti
                for (i = 0; i < MAX_EEPR_NR_TRASPONDER; i++) {
                    pointer_trasponder = (i * MAX_EEPR_DATA_TRASPONDER);
                    EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_VERIFICATO;
                    EE_trasponder[pointer_trasponder + EE_STATO] &= ~TRASP_ATTIVO_GUIDA;
                }

                Prepare_TxRx_Packet_broadcast(0, 0);

                // 2 - Devo verificare i trsponder che hanno risposto, con quelli che hanno risposto
                // la prima volta. Di questi trasponder devo verificare quali sono dentro o fuori raggio
                // dei primi segnalo la presenza nella mskIn dei secondi segnalo l'uscita in mskOut solo
                // nel caso in cui sia fuori il debounce
                for (loopIdx = 0; loopIdx < MAX_EEPR_NR_TRASPONDER; loopIdx++) {
                    SetLoopMsk();

                    if (indice & mskPresProssimita[loopMsk])
                    {
                        if ((indice & mskPres[loopMsk]) && (indice & mskRSSIPros[loopMsk])) {
                            // Il trasponder � presente e dentro il raggio
                            counter_prossimita[loopIdx] = 0;
                            mskIn[loopMsk] |= indice; // trasponder dentro il raggio
                        } else
                            counter_prossimita[loopIdx]++;

                        if (counter_prossimita[loopIdx] >= debounce_prossimita) {
                            mskOut[loopMsk] |= indice; // trasponder fuori raggio per n volte, fare segnalazione
                            mskPresProssimita[loopMsk] &= ~indice; // cos� non viene pi� controllato in futuro
                        }
                    }

                    if (indice & 0x80000000)
                        indice = 0x0001; // si resetta l'indice per il cambio di maschera
                    else
                        indice *= 2;
                }

                // 3 - verifico se c'� qualche trasponder che � uscito in modo definitivo
                // viene segnalato in mskOut
                if (mskOut[0] || mskOut[1] || mskOut[2] || mskOut[3]) {
					dWordMemCpy(msk_prossimita_out_multipla, (const DWORD*)mskOut, 4);
                    AddNotifyToTel(I2CEVT_PROSS_OUT, I2CEVT_PROSS_OUT_B_KO);
                }

                // 4 - Verifichiamo se non c'� nessun trasponder dentro il raggio 
                if (!mskIn[0] && !mskIn[1] && !mskIn[2] && !mskIn[3])
                    cnt_debounce_prossimita++;
                else
                    cnt_debounce_prossimita = 0;

                // 5 - Verifico se � da gestire la terminazione della funzione
                if (cnt_debounce_prossimita >= debounce_prossimita) {
                    cmd_telefono = CMDW_TEL_PROSS_OUT_STOP;
                    AddNotifyToTel(I2CEVT_PROSS_OUT, I2CEVT_PROSS_OUT_DISABLE);
                    //fase_centralina = FASE_940_RUN;
                    stato_pic_low &= STATO_PIC_IN_PROSS_OUT_NOT;
                } else {
                    // Riattivo conteggio per la successiva interrogazione
                    // e metto in sleep il PIC
                    flg_reqProssOut = TRUE;
                    cnt_pollingProssIn = 0;
                    tau_pollingProssIn = tau_polling_prossimita;
                    //fase_centralina = FASE_940_POWER_UP;
                }
                fase_centralina = FASE_940_RUN;
            }

            break;

    }
    return 0;
}

/*********************************************************************
 * Function:        void HighISR(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    Various flags and registers set.
 *
 * Overview:        This is the interrupt handler for the MRF89XA
 *                  
 ********************************************************************/
void TMR1_CallBack(void) {
    // incrementa contatore timer di sistema 
    ctr_timer_ext++;
    ctr_timer_1s++;

    if (ctr_timer_ext >= DEF_100MS_BS_2MS) {

		ClrWdt();

        // sono passati 100 ms
        timer_sistema++;
        ctr_timer_ext = 0;
        // verifica se timer in corso
        if (GTRXTimeoutExt_flag == TRUE) {
            // abilitazione timeout lungo
            // decremento contatore
            GTRXTimeoutExt--;

            if (GTRXTimeoutExt == 0) {
                // timeout trascorso
                GTRXTimeExt_expired = TRUE;
                GTRXTimeoutExt_flag = FALSE;
            }
        }

        if (TauSleepI2C)
            TauSleepI2C--;
    }

    // Timeout di un secondo
    if (ctr_timer_1s >= DEF_1000MS_BS_2MS) {
        ctr_timer_1s = 0;
        // Se il pezzo � in run e sta gestendo la funzione di prossimit�
        // va decrementato il timeout generale
        if (cnt_timeout_prossimita)
            cnt_timeout_prossimita--;

#ifdef AUTOAPPRENDIMENTO_SMART
		ctr_timer_10s++;
		if (ctr_timer_10s >= 10)
		{
			ctr_timer_10s = 0;

			if (fase_centralina == FASE_940_AUTOAPP_1 || fase_centralina == FASE_940_AUTOAPP_2)
				ctr_tx_autoapprendimento++;
		}//ogni 10 sec
#endif

        if( tmrEnableTxMng )
        {
            tmrEnableTxMng--;
            if( !tmrEnableTxMng )
            {
                suspendTxMng = 0;
            }
        }
    }

    // Timeout eseguito all'init del pezzo		
    if (GTTimeout2_flag) {
        GTTimeout2--;
        if (!GTTimeout2) {
            GTTime2_expired = TRUE;
            GTTimeout2_flag = FALSE;
        }
    }

    if (GTRXTimeout_flag == TRUE) {
        // abilitazione conteggio attesa ricezione
        GTRXTimeout--;

        if (GTRXTimeout == 0) {
            // timeout trascorso
            GTRXTime_expired = TRUE;
            GTRXTimeout_flag = FALSE;
        }
    }

    if (GTRXInibitTimeout_flag == TRUE) {
        // abilitazione conteggio attesa ricezione
        GTRXInibitTimeout--;

        if (GTRXInibitTimeout == 0) {
            // timeout trascorso
            GTRXInibitTime_expired = TRUE;
            GTRXInibitTimeout_flag = FALSE;
        }
    }

    // Gestione conteggio procedura KeepAlive I2C
    if (GTKATimeout_flag == TRUE) {
        GTKATimeout--;
        if (!GTKATimeout) {
            // timeout trascorso
            GTKATimeout_flag = FALSE;
            GTKASendNotify = TRUE;
        }
    }
}
/*
void EX_INT1_CallBack() {
    // gestione interrupt IRQ0
    if (RF_Mode == RF_RECEIVER)
        RSSIRegVal = (RegisterRead(REG_RSSIVALUE) >> 1);
    else
        IRQ0_Received = TRUE;
}

void EX_INT2_CallBack() {
    // gestione interrupt IRQ1
    if (RF_Mode == RF_RECEIVER)
        RSSIRegVal = (RegisterRead(REG_RSSIVALUE) >> 1);

    IRQ1_Received = TRUE;
}
 */
