/*
 * Getronic srl
 *
 * File:   GTI2C.h
 * Author: Stefano Isella
 *
 * Created on 23 luglio  2015
 * 
 */
#ifndef GTI2C_h
#define GTI2C_h

#include <stdbool.h>

extern void     I2CRoutine   (void);
extern void     I2CHandle(void);
extern void 	AddNotifyToTel(unsigned int nt,unsigned int cd);
extern int 		TelBuffIsEmpty(void);
extern bool salvataggio_soglie;

// Global Definition

#define __I2CMAXOUT     16
#define __I2CMAXIN      18
#define __I2CINBASE     0
#define __I2CSIZELIST	10
//#define __I2CSIZELIST	20		// in origine 20, ma per 99 trasponder dovuto diminurire per mancanza spazio

enum CODE_VERIFICA
{
	TAIL_EMPTY = 0,
	TAIL_BUSY,
	DATA_OK,
	
	NUM_STATE_CODE_VERIFICA
};

#endif // GTI2C_h


/*
EOF
 */

