/* 
 * File:   eeprom.h
 * Author: Marco Bosisio
 *
 * Created on February 14, 2019, 3:29 PM
 */

#ifndef EEPROM_H
#define	EEPROM_H

#ifdef	__cplusplus
extern "C" {
#endif

#define SLAVE_I2C_GENERIC_RETRY_MAX           100
#define SLAVE_I2C_GENERIC_DEVICE_TIMEOUT      50   // define slave timeout 
#define SLAVE_DEVICE_ADDRESS                  0x50   // define slave addres
#define EEPROM_PAGE_SIZE                64
#define EEPROM_PAGE_COUNT               2048

    uint8_t eeprom_read(uint16_t address);
    void eeprom_write(uint16_t address, uint8_t data);

#ifdef	__cplusplus
}
#endif

#endif	/* EEPROM_H */

