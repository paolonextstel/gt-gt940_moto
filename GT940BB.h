/*********************************************************************
 *
 *                  Radio Utility Driver specific defs.
 *
 *********************************************************************
 * FileName:        RadioDriverdefs.h
 * Dependencies:    None
 * Processor:       PIC16FL1829
 *
 ********************************************************************/
/********************************************************************
 * Versione 1 - 15/06/2016  --> Prima versione GT940BB (derivato da GT940TRP) con modifica
protocollo I2C per essere compatibile con la logica protocollo (filo rosa/nero)
dei dispositivi I2C della BB. 
Modifica gestione SLEEP. Va in SLEEP in modo automatico e viene
risvegliato solo da richieste I2C o da WDT interno se � attiva qualche
funzione.
Per il resto la parte di funzionalit� � rimasta identica al GT940TRP.
 ********************************************************************/
/********************************************************************
 * Versione 2 - 30/06/2016  --> Aggiunta gestione per ricerca broadcast
dei trasponder.
 ********************************************************************/
/********************************************************************
 * Versione 3 - 08/07/2016  --> Gestione della ricerca broadcast per tutte
le funzioni del 940
 ********************************************************************/
/********************************************************************
 * Versione 4 - 12/07/2016  --> Modifiche per gestire in modo corretto
la funzione verifica presenza nella varie possibili situazioni di 
impostazione
 ********************************************************************/
/********************************************************************
 * Versione 5 - 12/07/2016  --> Aggiunta la funzione antirapina nella gestione
verifica presenza. Aggiunti i comandi I2C per per leggere e scrivere
il site code della parte radio; e il comando di inserimento trasponder
senza passare dell'autoapprendimento
 ********************************************************************/
/********************************************************************
 * Versione 6 - 12/09/2016  --> Modificato tutto il firmware per poter
gestire 104 trasponders. Principale modifca � stata l'eliminiazione 
della memorizzazione del SN del trasponder e la modifica sulla gestione
della EEPROM.
 ********************************************************************/
/********************************************************************
 * Versione 7 - 27/09/2016  --> Modifica per evitare il problema della
mancata notifica dell'esito della ricerca verso il telefono.
 ********************************************************************/
/********************************************************************
 * Versione 8 - 12/10/2016  --> Modifiche per gestire correttamente anche
le funzioni di prossimit� multipla IN e OUT.
 ********************************************************************/
/********************************************************************
 * Versione 9 - 21/11/2016  --> Modifiche per gestire il test dei trasponders
e la notifica delle anomalie
 ********************************************************************/
/********************************************************************
 * Versione 10 - 30/11/2016  --> Modifica per problema riscontrato nella
funzione di verifica multipla
 ********************************************************************/
/********************************************************************
 * Versione 11/12 - 20/02/2017  --> Modifica per gestione ripristino batteria
trasponders
 ********************************************************************/
/********************************************************************
 * Versione 13 - 08/11/2017  --> Modifiche per gesitre problemi riguardanti
la funzione di verifica presenza, vedere mail del 17/10/2017
 ********************************************************************/
//940_MT
/********************************************************************
 * Versione 1 - 20/02/2019  --> Porting del 9400BB su 940_MT
 Nuova versioning, per ora non va in sleep
 ********************************************************************/
/********************************************************************
 * Versione 2 - 05/02/2019  --> Introdotto sleep e SQTP
 ********************************************************************/
/********************************************************************
 * Versione 3 - 07/02/2019  --> Corretta gestione comandi da I2C
 ********************************************************************/
/********************************************************************
 * Versione 4 - 11/03/2019  --> Corretta inizializzazione eeprom
 * e introdotto sleep condizionato a comando da telefono
 ********************************************************************/
/********************************************************************
 * author :         Paolo Brigatti
 * Versione 5 - 19/03/2019  --> Implementata procedura per azzeramento
 * eeprom all'avvio tramite jumper 
 ********************************************************************/
 /********************************************************************
 * author :         Paolo Brigatti
 * Versione 6.00 - 12/09/2019  --> porting AUTOAPRENDIMENTO_SMART from 940I vers 24.09
 ********************************************************************
 * author :         Paolo Brigatti
 * Versione 6.01 - 21/09/2019  -- > .porting AUTOAPRENDIMENTO_SMART from 940I vers 24.12
********************************************************************
 * author :         Paolo Brigatti
 * Versione 6.02 - 16/10/2019  -- > .sistemato ritorno info autista
 ********************************************************************/
/* author :         Paolo Brigatti
 * Versione 6.03 -  23/12/2019  -- >  Porting gestione trasmettitore from 940I v 24.40
 *
 *     _elenco modifiche 940I da vers 24.12 a 24.40
 *                  Vers. 24.13  .eliminata notifica ricerca KO per TXcom
 *					Vers. 24.14  .lev batt ricevuto in payload trasponder, sistemato ritorno info autista
 *					Vers. 24.15  .compattato codice
 *					Vers. 24.16  .sistemata uscita autoapprendimento, gestione sync code, no invio batteria da TX
 *					Vers. 24.17  .definite static variabili maschere trasponder
 *					Vers. 24.18  .ripristinato invio batteria da TX
 *					Vers. 24.19  .compattato codice
 *					Vers. 24.20  .define static idx in get_sirena()
 *                  Vers. 24.23  .gestione flagTx da comando Ricerca Broadcast
 *                               .corretto feeback maschera stato TX (loop cancellazione flag TrasponderTestato non deve avvenire se riceve TX)    
 *                  Vers. 24.24  .inserito comando I2C 0x91 configurazione 940 TX/trasponder da BB 
 *                  Vers. 24.25  .disabilitazione gestione Tx durante ricerca broadcast 
 *                  Vers. 24.26  .Gestione ricercaBroadcast anche in configurazione TX
 *                  Vers. 24.27  .messa configurazione 940I sotto #define CONFIGURA_940 
 *                  Vers. 24.28  .gestita ricezione TX durante verifica presenza attiva
 *                  Vers. 24.29  .eliminato loop notifica ricercaOK per ricerca con id
 *                  Vers. 24.30  .modificato condizione feedback ricerca OK
 *                  Vers. 24.31  .invia evento 52,5 riceve da TX (compatibile con BB.2065.109
 *                  Vers. 24.32  .fix feedback verifica presenza not C
 *                               .WIP verifiche singole 940enfun=0x000F
 *                  Vers. 24.33  .fix verifiche singole 940enfun=0x000F
 *                  Vers. 24.34  .invio driver al feedback verifica broadcast
 *                                (per verifica singola+multipla 940enfun=0x001F)
 *                  Vers. 24.35  .WIP fix aggiornamento id trasp attivo.
 *                  Vers. 24.36  .WIP fix aggiornamento id trasp attivo.
 *                  Vers. 24.37  .WIP flag no trasponder trovati
 *                  Vers. 24.38  .fix reintegro trasponder durante verifiche presenza da nessun trasponder presente.
 *                  Vers. 24.39  .verifica presenza: se assente il trasponder attivo viene cambiato con il tr con ID maggiore 
 *                  Vers. 24.40  .con 940ENFUN=001F set nuovo autista solo se DriverOn prenotato su trasponder
 ********************************************************************/
/* author :         Paolo Brigatti
 * Versione 6.04 -  02/01/2020  -- >  Porting   fix segnalazione antirapina ricerca C from 940I v 24.41
 ********************************************************************/ 
/* author :         Paolo Brigatti
 * Versione 6.05 -  10/01/2020  -- >  Porting from 940I v 24.44: .fix segnalazione antirapina ricerca C e
                                                                 .invio evento 100,2 una sola volta
 ********************************************************************/ 
/* author :         Paolo Brigatti
 * Versione 6.06 -  15/01/2020  -- >  Porting from 940I V. 24.45   .evento 100,2 alla prima verifica senza autista
 *                                                      V. 24.46   .DEBUG 
 *                                                      V. 24.47   .scartate Tx manuali durante verifica presenza 
 *                                                      V. 24.48   .conteggio antirapina in assenza di trasponder
 * Versione 6.07                                        V. 24.49   .fix antirapina singolo trasponder 
 ********************************************************************/ 
/* author :         Paolo Brigatti
 * Versione 6.10 -  17/01/2020  -- >  Sospensione gestione TX per comando I2c  Test come comando Ricerca broadcast
 ********************************************************************/ 
/* author :         Paolo Brigatti
 * Versione 6.11 -  20/01/2020  -- >  Fix ricerca broadcast in test
 ********************************************************************/
/* author :         Paolo Brigatti
 * vers     6.12-6.14                 DEBUG
 * Versione 6.15 -  21/01/2020  -- >  Fix ricezione messaggio ricerca broadcast
 * 
 ********************************************************************/
/* author :         Paolo Brigatti
 * Versione 6.16 -  10/02/2020  -- >  Porting from 940I v 24.52  fix attesa initbit radio, causava invio morboso di 52,2 durante periodo di inibit radio
 *                                                      v 24.53  fix salvataggio in eeprom
 ********************************************************************/  
/* author :         Paolo Brigatti
 * Versione 7.0 -  08/07/2020  -- >  rinominata vers 6.16 per produzione
 ********************************************************************/  



#define VERS_940MT		7
#define SUBVERS_940MT	0

#include "GenericTypeDefs.h"

// DEFINES FUNZIONALITA' 940
#define AUTOAPPRENDIMENTO_SMART  // define per nuova procedura di autoapprendimento che sfrutta la Tx manuale del trasponder alla prima pressione
//#define SLEEP_LIKE_940I

//IEEE EUI - globally unique number
#define EUI_7 0x00
#define EUI_6 0x01
#define EUI_5 0x02
#define EUI_4 0x03
#define EUI_3 0x04
#define EUI_2 0x05
#define EUI_1 0x06
#define EUI_0 0x07

/*
// definizione PIN di uscita per interrupt su PIC GPS				
#define	OUT_WAKEI2C_TRIS		TRISC2
#define	OUT_WAKEI2C				RC2

// definizione PIN Interrupt
#define PHY_IRQ_En 				INTCONbits.IOCIE
#define PHY_IRQ    				INTCONbits.IOCIF	

#define PHY_IRQ0_TRIS 			TRISA4
#define PHY_IRQ1_TRIS 			TRISA5
#define	PHY_IRQ2_TRIS			TRISA2	

// Rising edge
#define PHY_IRQ0_RLEDGE			IOCAPbits.IOCAP4
#define PHY_IRQ1_RLEDGE			IOCAPbits.IOCAP5
#define PHY_IRQ2_RLEDGE			IOCAPbits.IOCAP2

// Falling Edge
#define PHY_IRQ0_FLEDGE			IOCANbits.IOCAN4
#define PHY_IRQ1_FLEDGE			IOCANbits.IOCAN5
#define PHY_IRQ2_FLEDGE			IOCANbits.IOCAN2

// interrupt flag
#define PHY_IRQ0 				IOCAFbits.IOCAF4
#define PHY_IRQ1 				IOCAFbits.IOCAF5
#define PHY_IRQ2 				IOCAFbits.IOCAF2

// Configurazione PIN debug radio
//#define	OUT_DEBUG_RX_ACTIVE
#define	OUT_DEBUG_RX_TRIS		TRISA1
#define	OUT_DEBUG_RX			RA1

//  configurazione SPI SDI
#define SPI_SDI 				RB4
#define SPI_SDI_TRIS 			TRISB4

#define I2C_SDA_WP100			RB5			// I2C SDA con WMP100
#define I2C_SDA_WP100_TRIS		TRISB5		// I2C SDA con WMP100
#define I2C_SCL_WP100			RB7			// I2C SCL con WMP100
#define I2C_SCL_WP100_TRIS		TRISB7		// I2C SCL con WMP100

// configurazione SPI Clock 
#define SPI_CK 					RB6
#define SPI_CK_TRIS 			TRISB6

//  dati Chip Select MRF
#define Data_nCS 				RC0
#define Data_nCS_TRIS 			TRISC0

// configurazione Chip Select MRF
#define Config_nCS 				RC3
#define Config_nCS_TRIS 		TRISC3

//  configurazione SPI SDI
#define SPI_RESET 	   			RC6
#define SPI_RESET_TRIS 			TRISC6

//  configurazione SPI SDO 
#define SPI_SDO 				RC7
#define SPI_SDO_TRIS 			TRISC7

 */

//#define MAX_CICLI_ATTESA_START_AUTOAPP	10

#define PACKET_LEN 			64
#define PAYLOAD_LEN 		64
#define	ADD_GT_940TRP		1
#define	ADD_GT_TRP			2

// formato messaggio inviato 
//    - 4 byte di preambolo
//    - 4 byte di sync
//    - 1 byte lunghezza dati
//    - 1 byte address
//    - 2 byte contatore pacchetto messaggo
//    - 2 byte numeo pacchetti messaggio
//    - 4 byte stato comandi
//    - 4 byte GT rolling code
//    - 2 byte contatore TX trasponder
//    - 2 byte CRC 

#define MAX_TXRX_BUFF			14

#define COUNTER_HIGH			0
#define COUNTER_LOW				1
#define MAX_COUNTER_HIGH		2
#define MAX_COUNTER_LOW			3
#define STATUS_CMD_0			4
#define STATUS_CMD_1			5
#define STATUS_CMD_2			6
#define STATUS_CMD_3			7
#define ROLL_CODE_0				8
#define ROLL_CODE_1				9
#define ROLL_CODE_2				10
#define ROLL_CODE_3				11
#define TX_COUNTER_HIGH			12
#define TX_COUNTER_LOW			13


// define STATUS_CMD_0
#define STATUS_CMD_0_B0					0	// stato test : 0 = test non in corso, 1 = test in corso
#define MASK_BIT_START_TEST				0x01
#define MASK_RESET_BIT_START_TEST		0xFE

#define STATUS_CMD_0_B1			1	// batteria bassa : 0 = batteria normale, 1 = batteria bassa
#define MASK_BIT_BATT_STATUS		0x02	// valore di set bit di batteria bassa
#define MASK_RESET_BIT_BATT_STATUS	0xFD 	// valore di reset bit di batteria bassa
#define MASK_BIT_BAT_H				0x03	// Bit utilizzati nel comando per livello batteria parte alta
// batteria bassa : 0 = batteria normale, 1 = batteria bassa

#define STATUS_CMD_0_B2					2 		// stato inserimento
#define MASK_BIT_RICERCA_BROADCAST		0x04	// Set bit per ricerca broadcast
#define MASK_RESET_BIT_RIC_BROADCAST	0xFB 	// Valore di reset bit per ricerca broadcast

#define STATUS_CMD_0_B3					3	// start autoapprendimento
#define MASK_BIT_START_AUTOAPP			0x08
#define MASK_RESET_BIT_START_AUTOAPP	0xF7

#define STATUS_CMD_0_B4					4	// fine autoapprendimento
#define MASK_BIT_STOP_AUTOAPP			0x10
#define MASK_RESET_BIT_STOP_AUTOAPP		0xEF

#define STATUS_CMD_0_B5					5	// buffer pieno
#define MASK_BIT_BUFFER_FULL			0x20
#define MASK_RESET_BIT_BUFFER_FULL		0xDF
#define MASK_BIT_STATO_TAMPER			0x20
#define MASK_RESET_BIT_STATO_TAMPER		0xDF

// define STATUS_CMD_1
#define STATUS_CMD_1_B0					0	
#define MASK_BIT_START_INSER			0x01
#define MASK_RESET_BIT_START_INSER		0x0FE

#define STATUS_CMD_1_B1					1	
#define MASK_BIT_STROBO					0x02	// set accensione strobo	
#define MASK_RESET_BIT_STROBO			0xFD	


#define STATUS_CMD_1_B2					2 		// anomalia
#define MASK_BIT_ANOMALIA				0x04
#define MASK_RESET_BIT_ANOMALIA			0xFB

#define STATUS_CMD_1_B3					3	// non usato
#define MASK_CMD_INSER					0x8
#define MASK_RESET_CMD_INSER			0xF7

#define STATUS_CMD_1_B4					4	// non usato
#define MASK_CMD_DISINSER				0x10
#define MASK_RESET_CMD_DISINSER			0xEF

#define STATUS_CMD_1_B5					5	// non usato
#define MASK_CMD_AVV_ALLARME			0x20
#define MASK_RESET_CMD_AVV_ALLARME		0xDF

#define STATUS_CMD_1_B6					6	// non usato
#define MASK_CMD_SEGN_GENERICA			0x40
#define MASK_RESET_CMD__SEGN_GENERICA	0xBF

#define STATUS_CMD_1_B7			7	 
#define MASK_CMD_ALLARME				0x80
#define MASK_RESET_CMD_ALLARME			0x7F

// define STATUS_CMD_2
#define STATUS_CMD_2_B0						0	// 
#define STATUS_CMD_2_B1						1	// 
#define STATUS_CMD_2_B2						2	// 
#define STATUS_CMD_2_B3						3	// 

#define STATUS_CMD_2_B4						4	// 
#define STATUS_CMD_2_B5						5	// 
#define STATUS_CMD_2_B6						6	// 
#define STATUS_CMD_2_B7						7	// 

#define CONFIG_MASK_BIT_TIPO_CENTRALIN		0xF0
#define TYP_GT940TRP						0x80	//0x10
#define	GT985_DRIVER_ABIL					0x80	// stato dal 985 che indica che il trasponder � abilitato alla guida
#define CONFIG_MASK_BIT_ID_TRASP			0x7F	//x0F

// define STATUS_CMD_3
#define STATUS_CMD_3_B0						0	// 
#define STATUS_CMD_3_B1						1	// 
#define STATUS_CMD_3_B2						2	// 
#define STATUS_CMD_3_B3						3	// 
#define STATUS_CMD_3_B4						4	// 
#define STATUS_CMD_3_B5						5	// 
#define STATUS_CMD_3_B6						6	// 
#define STATUS_CMD_3_B7						7	// 


#define MAX_ACCENSIONI_RX_NORMALE		20		
#define MAX_ACCENSIONI_RX_AUTOAPP		10		// numero di cicli di accensione in attesa di autoapprendimento
#define MAX_SET_FUNZIONI				6		// numero massimo di funzioni di programmazione

// define tempi di timeout con base 100 ms
#define MAX_TO_START_AUTOAPPRENDIMENTO  80		// finestra start autoapprendimento con doppio giro chiave
// espressa in tick da 100 ms 
#define MAX_TO_START_INSERIMENTO		600		// finestra temporale per attivazione inserimento
#define MAX_TO_START_BLKM				3000	// finestra temporale per attivazione blocco motore
#define MAX_TO_START_CHECK_ANTIR		200		// finestra temporale per attesa check antirapina
#define MAX_TO_START_SEGNALA_ANTIR		300		// finestra temporale per segnalazione antirapina 
#define MAX_TO_START_CHECK_PROX			600		// finestra temporale per attesa check prossimit�												
#define MAX_TO_START_CHECK_SBLOCCO_EM	150		// finestra temporale per attesa start blocco di emergenza												
#define MAX_TO_START_CHECK_TIPO_EM		50		// finestra temporale per attesa riconoscimento tipo di sblocco EM
#ifdef AUTOAPPRENDIMENTO_SMART
#define MAX_CICLI_TX_AUTOAPP			6 //30		// numero di cicli di trasmissione prima di uscire dall'autoapprendimento
#else
#define MAX_CICLI_TX_AUTOAPP			5		// numero di cicli di trasmissione prima di uscire dall'autoapprendimento
#endif

#define MAX_CICLI_TX_RIC_TRASP			2		// numero di cicli di trasmissione prima di uscire dalla ricerca

#define MAX_CICLI_TX_CANCEL				2		// numero di cicli di trasmissione prima di uscire dalla cancellazione

#define MAX_CTR_DEB_CANC_TRASP			4		// numero di ritentativi

#define MAX_CICLI_TX_RIC_TRASP_ANTIRAP	4 		// numero di cicli di trasmissione ricerca trasponder antirapina

#define MAX_CICLI_TX_RIC_TRASP_REG_ANTIRAP	3	// numero di cicli di antirapina per ricerca trasponder registrato

#define MAX_CTR_STATO_ANTIRAP			4 		// numeri di cicli di segnalazione stato antirapina OK

#define MAX_CICLI_TX_RIC_TRASP_PROX		4		// numeri di cicli di trasmissione ricerca trasponder prossimit�
#define MAX_CICLI_TX_RIC_TRASP_REG_PROX	2		// numero di cicli di ricerca trasponder registrato in prossimit�

#define MAX_CICLI_TX_RIC_TRASP_DIS_ALL		2	// numero di cicli di ricerca trasponder per disinserimento in allarme
#define MAX_CICLI_TX_RIC_TRASP_REG_DIS_ALL	8	// numero di cicli di ricerca per disinseriemnto in allarme per  trasponder registrato

// define per timeout base tempi 100 ms

#define MAX_TO_COUNTER		7
#define TO_GIRI_CHIAVE		0
#define TO_ABIL_INSER		1
#define TO_ABIL_BLKM		2
#define TO_CHECK_ANTIRAP	3
#define TO_SEGNALA_ANTIRAP  4
#define TO_CHECK_PROX		5
#define TO_CHECK_SBLOCCO_EM	6

// define per velocit� di comunicazione e trasmissione
#define RATE200
#define MAX_TX_PACKET_100K	380 	// corrisponde a circa 1596 ms (4.2ms * 380)utilizzato a 100kbit
#define MAX_TX_PACKET_200K	513 	// corrisponde a circa 1540 ms (3 ms * 513) utilizzato a 200Kbit
#define MAX_TX_PACKET_200K_H	0x02 	// corrisponde alla parte alta del numero di pacchetti a 200Kbit
#define MAX_TX_PACKET_200K_L	0x01 	// corrisponde alla parte bassa del numero di pacchetti a 200Kbit

// valori validi per compilazione non ottimizzata
//#define MAX_TX_PACKET_200K_2S	667 	// corrisponde a circa 2001 ms (3 ms * 667) utilizzato a 200Kbit
//#define MAX_TX_PACKET_200K_H_2S	0x02 	// corrisponde alla parte alta del numero di pacchetti a 200Kbit
//#define MAX_TX_PACKET_200K_L_2S	0x9B 	// corrisponde alla parte bassa del numero di pacchetti a 200Kbit

//#define MAX_TX_PACKET_200K_2S	733 	// 	corrisponde a circa 2001 ms (2.7285 ms * 733) utilizzato a 200Kbit
//#define MAX_TX_PACKET_200K_H_2S	0x02 	// corrisponde alla parte alta del numero di pacchetti a 200Kbit
//#define MAX_TX_PACKET_200K_L_2S	0xDD 	// corrisponde alla parte bassa del numero di pacchetti a 200Kbit

#define MAX_TX_PACKET_200K_2S	780 	// 	corrisponde a circa 2148,9 ms (2.755 ms * 780) utilizzato a 200Kbit
#define MAX_TX_PACKET_200K_H_2S	0x03 	// corrisponde alla parte alta del numero di pacchetti a 200Kbit
#define MAX_TX_PACKET_200K_L_2S	0x0C 	// corrisponde alla parte bassa del numero di pacchetti a 200Kbit

#define MAX_TX_PACKET_EXPLORER	295	// define numero pacchetti riecvuti da scehda EXPLORER (solo in fase di ebug)
#define GT_MAX_PACKET_RX	10		// define per numero pacchetti inviati come risposta
#define GT_MS_TX_PACKET		3		// define tempo spedizione singolo pacchetto 2.8ms circa

// define per timeout con base tempi 100 msec
#define DEF_15SEC_BS_100MS		150	
#define DEF_10SEC_BS_100MS		100	
#define DEF_6SEC_BS_100MS		60	
#define DEF_3SEC_BS_100MS		30	
#define DEF_2SEC_BS_100MS		20	
#define DEF_1SEC_BS_100MS		10	

// define per timeout con base tempi 2 msec
#define DEF_4000MS_BS_2MS		2000
#define DEF_1000MS_BS_2MS		500
#define DEF_536MS_BS_2MS		268
#define DEF_500MS_BS_2MS		250
#define DEF_300MS_BS_2MS		150
#define DEF_262MS_BS_2MS		131
#define DEF_250MS_BS_2MS		125
#define DEF_200MS_BS_2MS		100
#define DEF_100MS_BS_2MS		50
#define DEF_120MS_BS_2MS		60
#define DEF_50MS_BS_2MS			25
#define DEF_30MS_BS_2MS			15
#define DEF_20MS_BS_2MS			10
#define DEF_10MS_BS_2MS			5
#define DEF_6MS_BS_2MS			3

#define SYNC_WORD_PASS_0		0x00	// passepourtout
#define SYNC_WORD_PASS_1		0x0A	// passepourtout
#define SYNC_WORD_PASS_2		0xF8	// passepourtout
#define SYNC_WORD_PASS_3		0x88	// passepourtout

/* Struttura eeprom */
/* 
 * 256 byte: 2 byte per ogni transponder, 1 per indice e 1 per stato 
 * 16 byte: maschere transponder 
 * 4 byte: site code 
 * 1 byte chiave validazione site code
 * 1 byte chiave validazione init eeprom
 * 1 byte: soglia baTt
 * 1 byte: soglia rssi  
 */
#define EE_ADD_TRASPONDER       	0
#define MAX_EEPR_DATA_TRASPONDER	0x2
#define MAX_EEPR_NR_TRASPONDER		128
#define MAX_EEPR_TRASPONDER			MAX_EEPR_DATA_TRASPONDER * MAX_EEPR_NR_TRASPONDER

#define EE_ADD_MSK_STATO        	MAX_EEPR_TRASPONDER
#define EE_BIT_SIZE_MSK_PRES		8
#define	EE_NUM_MSK_PRES				16

#define PM_SITE_CODE_ADD           	0x5600	
#define EE_SITE_CODE_ADD            EE_ADD_MSK_STATO + 	EE_NUM_MSK_PRES
#define EE_SITE_CODE_SIZE           4

#define EE_SITE_CODE_VALID_ADD      EE_SITE_CODE_ADD + EE_SITE_CODE_SIZE
#define EE_SITE_CODE_VALID_SIZE     1	
#define EE_SITE_CODE_VALID      	0xAA

#define EE_EEPROM_INIT_VALID_ADD    EE_SITE_CODE_VALID_ADD + EE_SITE_CODE_VALID_SIZE
#define EE_EEPROM_INIT_VALID_SIZE	1	
#define EE_EEPROM_INIT_VALID        0x55	

#define	EE_ADD_SOGLIA_BATT          EE_EEPROM_INIT_VALID_ADD + EE_EEPROM_INIT_VALID_SIZE	
#define EE_ADD_SOGLIA_RSSI          EE_ADD_SOGLIA_BATT + 1	
#ifdef CONFIGURA_940
  #define EE_940_CONFIG           EE_ADD_SOGLIA_RSSI +1
#endif

#define VAL_STATO_NULL              0x00
#define MSK_PRES_NULL               0x00

#define BATT_SOGLIA_940_DEF         55		// 806 - 751 = 55 --> 2600 + (55+4) = 2820 mV
#define RSSI_SOGLIA_940_DEF         50


#define MAX_STATO_TRASPONDER        MAX_EEPR_NR_TRASPONDER

#define EE_ID 					0	// Indica il numero di trasponder
#define	EE_STATO				1

#define EE_SITE_CODE_HH			0
#define EE_SITE_CODE_HL			1
#define EE_SITE_CODE_LH			2
#define EE_SITE_CODE_LL			3

#define	MAX_EEPR_SN_TRASP		4

// definizione stato PIC	- LOW
#define STATO_PIC_AUTOAPP			0x01
#define STATO_PIC_AUTOAPP_NOT		0xFE
#define STATO_PIC_TEST				0x02
#define STATO_PIC_TEST_NOT			0xFD
#define STATO_PIC_CANC				0x04
#define STATO_PIC_CANC_NOT			0xFB
#define STATO_PIC_SUPERV			0x08			// Sul TRP � la ricerca del trasponder		
#define STATO_PIC_SUPERV_NOT		0xF7		
#define STATO_PIC_SLEEP				0x10
#define STATO_PIC_SLEEP_NOT			0xEF
#define STATO_PIC_IN_VERIFICA		0x20
#define STATO_PIC_IN_VERIFICA_NOT	0xDF
#define STATO_PIC_IN_PROSS_IN		0x40
#define STATO_PIC_IN_PROSS_IN_NOT	0xBF
#define STATO_PIC_IN_PROSS_OUT		0x80
#define STATO_PIC_IN_PROSS_OUT_NOT	0x7F
// definizione stato PIC	- HIGH
#define	STATO_PIC_NEW_SITE_CODE		0x01
#define	STATO_PIC_NEW_SITE_CODE_NOT	0xFE
#define	STATO_PIC_ADD_DEVICE		0x02
#define	STATO_PIC_ADD_DEVICE_NOT	0xFD

#ifdef CONFIGURA_940
  #define	STATO_PIC_USE_TX    		0x04
  #define	STATO_PIC_USE_TX_NOT    	0xFB
#endif

#define		ST_940_SLEEP_PIC					0x0010
#define		ST_940_VERIFICA_ON					0x0020
#define 	ST_940_PROSS_IN						0x0040
#define 	ST_940_PROSS_OUT					0x0080

// Comando lettura diretta
#define CMDR_LETTURA_CODA_EVENTI	0x30

// comandi 
#define CMDW_PIC_KEEP_ALIVE_I2C		0x10
#define CMDW_PIC_AUTOAPP_ONOFF  	0x81
#define CMDW_PIC_TEST_ONOFF	    	0x82
#define CMDW_PIC_CANC_ONOFF			0x83
#define CMDW_PIC_RICERCA_TRAS		0x84		
#define CMDW_PIC_SLEEP				0x85
#define	CMDW_PIC_FERMA_VERIFICA		0x86
#define	CMDW_PIC_ATTIVA_VERIFICA	0x87
#define	CMDW_PIC_PROSSIMITA_IN		0x88
#define	CMDW_PIC_PROSSIMITA_OUT		0x89
#define CMDW_PIC_FERMA_RICERCA		0x8A
#define CMDW_PIC_RICERCA_BROADCAST	0x8B
#define	CMDW_PIC_FERMA_PROSS_IN		0x8C
#define	CMDW_PIC_FERMA_PROSS_OUT	0x8D
#define	CMDW_PIC_WRITE_SYNC_WORD	0x8E
#define	CMDW_PIC_ADD_DEVICE			0x8F
#define CMDW_PIC_SOGLIE				0x90
#ifdef CONFIGURA_940
  #define CMDW_PIC_940_CONFIG			0x91
#endif

#define	CMDW_PIC_BIT_RICERCA_C		0x01

// STATI CENTRALINA
#define FASE_940_POWER_UP			0x00
#define FASE_940_INIT				0x01
#define FASE_940_RUN				0x02
#define	FASE_940_AUTOAPP_1			0x03
#define FASE_940_AUTOAPP_2			0x04
#define FASE_940_FINE_AUTOAPP		0x05
#define FASE_940_TEST				0x06
#define FASE_940_CANCELLAZIONE		0x07
#define FASE_940_RICERCA			0x08
#define FASE_940_VERIFICA_PRES		0x09
#define FASE_940_PROSSIMITA_IN		0x0A
#define FASE_940_PROSSIMITA_OUT		0x0B
#define FASE_940_RICERCA_BROADCAST	0x0C
#define	FASE_940_WRITE_SYNC_CODE	0x0D		
#define	FASE_940_ADD_DEVICE			0x0E

// comandi gestione interna macchina a stati
#define CMDW_TEL_AUTOPP_OFF			1
#define CMDW_TEL_AUTOPP_ON			2
#define CMDW_TEL_TEST_OFF			3
#define CMDW_TEL_TEST_ON			4
#define CMDW_TEL_CANC_OFF			5
#define CMDW_TEL_CANC_ON			6
#define CMDW_TEL_INSER				7
#define CMDW_TEL_DISINS  			8
#define CMDW_TEL_SEGNALAZ			9
#define CMDW_TEL_SIRENA_ON			10
#define CMDW_TEL_SIRENA_OFF			11
#define CMDW_TEL_SUPERV_ON			12
#define CMDW_TEL_SUPERV_OFF			13
#define	CMDW_TEL_ATT_VERIFICA		14
#define CMDW_TEL_VERIFICA_ON		15
#define	CMDW_TEL_VERIFICA_OFF		16
#define	CMDW_TEL_PROSS_IN_START		17
#define	CMDW_TEL_PROSS_IN_ON		18
#define	CMDW_TEL_PROSS_IN_STOP		19
#define	CMDW_TEL_PROSS_OUT_START	20
#define	CMDW_TEL_PROSS_OUT_ON		21
#define	CMDW_TEL_PROSS_OUT_STOP		22
#define	CMDW_TEL_SLEEP_PIC			23
#define	CMDW_TEL_ATT_VER_SLEEP		24
#define CMDW_TEL_PIN_STOP_SLEEP		25
#define CMDW_TEL_GST_KEEP_ALIVE		26
#define CMDW_TEL_RICERCA_OFF		27
#define CMDW_TEL_RICERCA_BROADCAST	28
#define CMDW_TEL_WRITE_SYNC_CODE	29
#define CMDW_TEL_ADD_DEVICE			30


// gestione TEST
#define	TEST_MAX_GIRI			2		
// Comando cancella tutti
#define	CMD_DEL_ALL				0xF1

#define MASK_INSER				0x07
#define MASK_INS_BIT_0			0x1		// Beep inserimento
#define MASK_INS_BIT_1			0x2		// Strobo
#define MASK_INS_BIT_2			0x4		// Beep anomalia

#define MASK_DISINSER				0x0F
#define MASK_DISINS_BIT_0			0x1		// Beep disinserimento
#define MASK_DISINS_BIT_1			0x2		// Strobo
#define MASK_DISINS_BIT_2			0x4		// Beep anomalia
#define MASK_DISINS_BIT_3			0x8		// Beep avv. allarme

// Maschera STATI TRASPONDER
#define	TRAS_ATTIVO					0x01
#define	TRASP_ANOM_RSSI				0x02
#define	TRASP_ANOM_BAT				0x04
#define	TRASP_VERIFICATO			0x08
#define	TRASP_ATTIVO_GUIDA			0x10
#define	TRASP_ANOM_BATT_940			0x20
#define TRASP_TESTATO				0x80

//*************************************************//
//******************  NOTIFICHE	*******************//
//*************************************************//
#define	I2CEVT_NOTIFY 				1
#define	I2CEVT_AUTOAPP 				50
#define	I2CEVT_NUOVA_CONF			51
#define	I2CEVT_RICERCA				52
#define	I2CEVT_TEST					53
#define	I2CEVT_RICERCA_BROADCAST	54
#define	I2CEVT_WRITE_SYNC_WORD		55
#define	I2CEVT_ADD_DEVICE			56
#define	I2CEVT_ANOMALIA_TRASP		57
#define	I2CEVT_VERIFICA_PRES 		100
#define	I2CEVT_PROSS_IN				101
#define	I2CEVT_PROSS_OUT			102
#define	I2CEVT_ERRORE				500

//******************  DATO I2CEVT_NOTIFY 1	*******************//
#define	I2CEVT_NOT_DTO_ON 			1
#define I2CEVT_NOT_DTO_RESET		2
#define I2CEVT_NOT_DTO_OK 			100

//******************  DATO I2CEVT_AUTOAPP 50	*******************//
#define	I2CEVT_AUTOAPP_OK 			1
#define I2CEVT_AUTOAPP_KO			2
#define I2CEVT_AUTOAPP_BUFF_KO		3

//******************  DATO I2CEVT_NUOVA_CONF 51	*******************//
#define	I2CEVT_NUOVA_CONF_OK		1

//******************  DATO I2CEVT_RICERCA 52	*******************//
#define	I2CEVT_RICERCA_OK 			              1
#define I2CEVT_RICERCA_KO			              2
#define I2CEVT_CHANGE_ALARM_STATE_TX_COUNTER      3	
#define I2CEVT_CHANGE_ALARM_STATE_ID_TRASPONDER   4
#define I2CEVT_RICERCA_OK_BY_TX                   5


//******************  DATO I2CEVT_TEST 53	*******************//
#define	I2CEVT_TEST_OK 				1
#define I2CEVT_TEST_KO				2

//******************  DATO I2CEVT_WRITE_SYNC_WORD 55	*******************//
#define I2CEVT_WRITE_SYNC_WORD_OK	0
#define	I2CEVT_WRITE_SYNC_WORD_KO	1

//******************  DATO I2CEVT_ADD_DEVICE 56	*******************//
#define	I2CEVT_ADD_DEVICE_OK		0
#define I2CEVT_ADD_DEVICE_ID_KO		1
#define I2CEVT_ADD_DEVICE_TR_KO		2

//******************  DATO I2CEVT_ANOMALIA_TRASP 57	*******************//
#define	I2CEVT_ANOMALIA_TRASP_BATT	0

//******************  DATO I2CEVT_VERIFICA_PRES 100	*******************//
#define	I2CEVT_VER_PRES_CAMBIO 		1
#define I2CEVT_VER_PRES_NO_TRASP	2
#define I2CEVT_VER_PRES_RICERCA_B	3
#define I2CEVT_VER_PRES_NO_TRASP_B	4
#define I2CEVT_VER_PRES_NO_TRASP_ANTIRAPINA    0x0102

//******************  DATO I2CEVT_PROSS_IN 101	*******************//
#define	I2CEVT_PROSS_IN_DISABLE		1
#define I2CEVT_PROSS_IN_KO			2
#define I2CEVT_PROSS_IN_PRES		3	
#define I2CEVT_PROSS_IN_B_KO		4

//******************  DATO I2CEVT_PROSS_OUT 102	*******************//
#define	I2CEVT_PROSS_OUT_DISABLE	1
#define I2CEVT_PROSS_OUT_KO			2
#define I2CEVT_PROSS_OUT_PRES		3
#define I2CEVT_PROSS_OUT_B_KO		4

//******************  DATO I2CEVT_ERRORE 500	*******************//
#define	I2CEVT_ERRORE_GENERICO		1


// TIPI MASCHERE TRASPONDER  // NOTA_PB: valutare se compattare routines ricerca come per 940I
#define PRESENZA   0
#define TEST       1
#define ANOMALIA   2
#define BROADCAST  3
#define VALUE_TX_BY_USER_PRESS     0x01



void main_init(BOOL reinit);
int main_loop(void);








