/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system intialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.95-b-SNAPSHOT
        Device            :  PIC24FJ32GA002
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.36
        MPLAB 	          :  MPLAB X v5.10
 */

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
 */

/**
  Section: Included Files
 */
#include "mcc_generated_files/system.h"
#include "mcc_generated_files/pin_manager.h"
#include "GT940BB.h"
#include "GenericTypeDefs.h"
#include "mcc_generated_files/clock.h"


#define    FCY    _XTAL_FREQ    // Instruction cycle frequency, Hz - required for __delayXXX() to work
#include <libpic30.h>        // __delayXXX() functions macros defined here

/*
                         Main application
 */
int main(void) {
    // initialize the device
    SYSTEM_Initialize();
    RCONbits.SWDTEN = 0;
    //RCONbits.PMSLP = 1;
    //CLKDIVbits.DOZEN = 1;
    //CLKDIVbits.ROI = 1;
    __delay_ms(10);
    BOOL jumper_status = JUMPER_GetValue();
    JUMPER_SetDigitalOutput();
#if(DBG_JUMPER_INIT)
    JUMPER_SetHigh();
    __delay_ms(10);
    JUMPER_SetLow();
    __delay_ms(10);
    JUMPER_SetHigh();
    __delay_ms(10);
    JUMPER_SetLow();
#endif
    main_init(!jumper_status);
#if(DBG_JUMPER_INIT)
    JUMPER_SetHigh();
    __delay_ms(10);
    JUMPER_SetLow();
    __delay_ms(10);
    JUMPER_SetHigh();
#endif
    while (1) {
        // Add your application code
        main_loop();
        ClrWdt();
    }

    return 1;
}
/**
 End of File
 */

